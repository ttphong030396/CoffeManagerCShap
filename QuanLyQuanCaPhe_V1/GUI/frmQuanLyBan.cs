﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DTO;
using BUS;
namespace GUI
{
    public partial class frmQuanLyBan : Form
    {
        #region Thuoc tinh
        ErrorProvider err = new ErrorProvider();
        #endregion
        #region Phuong thuc
        public frmQuanLyBan()
        {
            try
            {
                InitializeComponent();
                CauHinh();
                LoadDSBan(int.Parse(nrudTrang.Value.ToString()), ckNhom.Checked);
                LoadKhuVuc();
                dgvBan.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
                dgvBan.MultiSelect = false;
                dgvBan.ReadOnly = true;
                cbbNhomKhuVuc.Enabled = false;
            }
            catch (Exception)
            {
            }
        }
        private void CauHinh()
        {
            try
            {
                cbbNhomKhuVuc.DataSource = BUS_Khu.Instance.LayDanhSach();
                cbbNhomKhuVuc.DisplayMember = "Ten";
                cbbNhomKhuVuc.ValueMember = "ID";
                cbbKhuVuc.DataSource = BUS_Khu.Instance.LayDanhSach();
                cbbKhuVuc.DisplayMember = "Ten";
                cbbKhuVuc.ValueMember = "ID";
                pnViTri.Visible = btnShow.Checked;

            }
            catch (Exception)
            {
            }
        }
        private void LoadKhuVuc()
        {
            try
            {
                dgvKhuVuc.DataSource = BUS_Khu.Instance.LayDanhSach();
                dgvKhuVuc.Columns["Ten"].HeaderText = "Tên Khu";
            }
            catch (Exception)
            {
            }
        }
        private int GetMaxPage(bool isFill)
        {
            try
            {
                if (isFill)
                {
                    int CountPage = BUS_Ban.Instance.GetCount(int.Parse(cbbNhomKhuVuc.SelectedValue.ToString()));
                    if (CountPage % 20 == 0)
                        return (int)(CountPage / 20);
                    return (int)(CountPage / 20) + 1;
                }
                else
                {
                    int CountPage = BUS_Ban.Instance.GetCount();
                    if (CountPage % 20 == 0)
                        return (int)(CountPage / 20);
                    return (int)(CountPage / 20) + 1;
                }
            }
            catch (Exception)
            {
                return -1;
            }
        }
        private void LoadDSBan(int Trang, bool isFill)
        {
            try
            {
                switch (isFill)
                {
                    case true:
                        {
                            // neu co chon chuc nang Loc theo khu vuc
                            dgvBan.DataSource = BUS_Ban.Instance.LayDanhSach_Trang_Khu(Trang, int.Parse(cbbNhomKhuVuc.SelectedValue.ToString().Trim())); ;
                            dgvBan.Columns[0].HeaderText = "ID Bàn";
                            dgvBan.Columns[1].HeaderText = "Tên bàn";
                            dgvBan.Columns[2].HeaderText = "Tình Trạng";
                            dgvBan.Columns[3].HeaderText = "ID Khu";
                            txtID.Clear();
                            txtTenBan.Clear();
                            break;
                        }
                    case false:
                        {
                            dgvBan.DataSource = BUS_Ban.Instance.LayDanhSach_Trang(Trang);
                            dgvBan.Columns[0].HeaderText = "ID Bàn";
                            dgvBan.Columns[1].HeaderText = "Tên bàn";
                            dgvBan.Columns[2].HeaderText = "Tình Trạng";
                            dgvBan.Columns[3].HeaderText = "ID Khu";
                            txtID.Clear();
                            txtTenBan.Clear();
                            break;
                        }
                }
                nrudTrang.Maximum = GetMaxPage(isFill);
            }
            catch (Exception)
            {
            }
        }
        #endregion
        #region Events
        private void ckNhom_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (ckNhom.Checked)
                {
                    cbbNhomKhuVuc.Enabled = true;
                    nrudTrang.Value = 1;
                    LoadDSBan(int.Parse(nrudTrang.Value.ToString().Trim()), ckNhom.Checked);
                }
                else
                {
                    cbbNhomKhuVuc.Enabled = false;
                    nrudTrang.Value = 1;
                    LoadDSBan(int.Parse(nrudTrang.Value.ToString().Trim()), ckNhom.Checked);
                }
            }
            catch (Exception)
            {
            }
        }
        private void nrudTrang_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (ckNhom.Checked)
                {
                    LoadDSBan(int.Parse(nrudTrang.Value.ToString()), ckNhom.Checked);
                }
                else
                {
                    LoadDSBan(int.Parse(nrudTrang.Value.ToString()), ckNhom.Checked);
                }
            }
            catch (Exception)
            {
            }
        }
        private void btnCapNhat_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtTenBan.Text.Trim() == "")
                {
                    err.SetError(txtTenBan, "TÊN BÀN KHÔNG ĐƯỢC BỎ TRỐNG");
                    txtTenBan.Focus();
                    return;
                }
                DTO_Ban ban = new DTO_Ban();
                ban.ID = int.Parse(dgvBan.CurrentRow.Cells[0].Value.ToString().Trim());
                ban.Ten = txtTenBan.Text.Trim();
                ban.IDKhu = int.Parse(cbbKhuVuc.SelectedValue.ToString().Trim());
                if (BUS_Ban.Instance.Update(ban))
                {
                    MessageBox.Show("Sửa thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    BUS_Database.Instance.GhiLichSu(frmDangNhap.Taikhoan.TaiKhoan, "Cập nhật tên bàn: " + dgvBan.CurrentRow.Cells[1].Value.ToString().Trim()+" Thành "+txtTenBan.Text, DateTime.Now);
                    frmMain.LoadLichSu();
                }
                else
                    MessageBox.Show("Lỗi", "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                LoadDSBan(int.Parse(nrudTrang.Value.ToString().Trim()), ckNhom.Checked);
            }
            catch (Exception)
            {
            }
        }
        private void btnXoa_Click(object sender, EventArgs e)
        {
            try
            {
                if (!(BUS_Ban.Instance.TrangThai(int.Parse(txtID.Text.Trim())).ToString().Trim().Equals("Trống")))
                {
                    MessageBox.Show("BÀN ĐANG CÓ NGƯỜI KHÔNG THỂ XÓA", "THÔNG BÁO", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                if (BUS_Ban.Instance.Delete(int.Parse(dgvBan.CurrentRow.Cells[0].Value.ToString().Trim())))
                {
                    BUS_Database.Instance.GhiLichSu(frmDangNhap.Taikhoan.TaiKhoan, "Xóa bàn " + dgvBan.CurrentRow.Cells[1].Value.ToString().Trim(), DateTime.Now);
                    frmMain.LoadLichSu();
                    MessageBox.Show("Bạn đã xóa bàn " + dgvBan.CurrentRow.Cells[1].Value.ToString().Trim(), "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                    
                LoadDSBan(int.Parse(nrudTrang.Value.ToString().Trim()), ckNhom.Checked);
            }
            catch (Exception)
            {
            }
        }
        private void btnThem_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtTenBan.Text.Trim() == "")
                {
                    err.SetError(txtTenBan, "TÊN BÀN KHÔNG ĐƯỢC BỎ TRỐNG");
                    txtTenBan.Focus();
                    return;
                }
                if (BUS_Ban.Instance.CountTen(txtTenBan.Text.Trim()) != 0)
                {
                    MessageBox.Show("Tên bàn đã tồn tại!\nThêm thất bại", "LỖI THÊM BÀN", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtTenBan.Text = "";
                    txtTenBan.Focus();
                }
                else
                {
                    DTO_Ban ban = new DTO_Ban();
                    ban.ID = int.Parse(txtID.Text.Trim());
                    ban.Ten = txtTenBan.Text;
                    ban.IDKhu = int.Parse(cbbKhuVuc.SelectedValue.ToString().Trim());
                    if (BUS_Ban.Instance.Insert(ban))
                    {
                        MessageBox.Show("Thêm thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        BUS_Database.Instance.GhiLichSu(frmDangNhap.Taikhoan.TaiKhoan, "Thêm Bàn "+ban.Ten, DateTime.Now);
                        frmMain.LoadLichSu();
                    }
                        
                    LoadDSBan(int.Parse(nrudTrang.Value.ToString().Trim()), ckNhom.Checked);
                }
            }
            catch (Exception)
            {
            }
        }
        private void btnDau_Click(object sender, EventArgs e)
        {
            try
            {
                nrudTrang.Value = nrudTrang.Minimum;
            }
            catch (Exception)
            {
            }
        }
        private void btnCuoi_Click(object sender, EventArgs e)
        {
            nrudTrang.Value = nrudTrang.Maximum;
        }
        private void btnSau_Click(object sender, EventArgs e)
        {
            try
            {
                int value = (int)nrudTrang.Value;
                if (value + 1 > nrudTrang.Maximum)
                    return;
                nrudTrang.Value++;
            }
            catch (Exception)
            {
            }
        }
        private void btnTruoc_Click(object sender, EventArgs e)
        {
            try
            {
                int value = (int)nrudTrang.Value;
                if (value - 1 < nrudTrang.Minimum)
                    return;
                nrudTrang.Value--;
            }
            catch (Exception)
            {
            }
        }
        private void cbbNhomKhuVuc_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                LoadDSBan(int.Parse(nrudTrang.Value.ToString().Trim()), ckNhom.Checked);
            }
            catch (Exception)
            {

            }
        }
        private void btnThemKhuVuc_Click(object sender, EventArgs e)
        {
            if (txtTenKhu.Text.Trim() == "")
            {
                err.SetError(txtTenKhu, "TÊN KHU KHÔNG ĐƯỢC BỎ TRỐNG!");
                return;
            }
            if (BUS_Khu.Instance.DemTenKhu(txtTenKhu.Text.Trim()) != 0)
            {
                MessageBox.Show("Tên khu đã tồn tại!\nVui lòng chọn tên khác!", "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            DTO_Khu khu = new DTO_Khu();
            khu.ID = int.Parse(txtIDKhu.Text.Trim());
            khu.Ten = txtTenKhu.Text.Trim();
            if (!BUS_Khu.Instance.Insert(khu))
            {
                MessageBox.Show("Lỗi khi thêm Khu", "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                MessageBox.Show("Thêm thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                BUS_Database.Instance.GhiLichSu(frmDangNhap.Taikhoan.TaiKhoan,"Thêm khu: "+txtTenKhu.Text, DateTime.Now);
                frmMain.LoadLichSu();
                cbbKhuVuc.DataSource = BUS_Khu.Instance.LayDanhSach();
                cbbKhuVuc.DisplayMember = "Ten";
                cbbKhuVuc.ValueMember = "ID";
                cbbNhomKhuVuc.DataSource = BUS_Khu.Instance.LayDanhSach();
                cbbNhomKhuVuc.DisplayMember = "Ten";
                cbbNhomKhuVuc.ValueMember = "ID";


                LoadKhuVuc();
            }
        }
        private void btnXoaKhuVuc_Click(object sender, EventArgs e)
        {
            if (!BUS_Khu.Instance.Delete(int.Parse(dgvKhuVuc.CurrentRow.Cells[0].Value.ToString().Trim())))
            {
                MessageBox.Show("Lỗi xóa", "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                MessageBox.Show("Đã xóa khu: " + dgvKhuVuc.CurrentRow.Cells[0].Value.ToString().Trim(), "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                BUS_Database.Instance.GhiLichSu(frmDangNhap.Taikhoan.TaiKhoan, "Xóa khu: " + dgvKhuVuc.CurrentRow.Cells[0].Value.ToString().Trim(), DateTime.Now);
                frmMain.LoadLichSu();
                LoadKhuVuc();
                cbbKhuVuc.DataSource = BUS_Khu.Instance.LayDanhSach();
                cbbKhuVuc.DisplayMember = "Ten";
                cbbKhuVuc.ValueMember = "ID";
                cbbNhomKhuVuc.DataSource = BUS_Khu.Instance.LayDanhSach();
                cbbNhomKhuVuc.DisplayMember = "Ten";
                cbbNhomKhuVuc.ValueMember = "ID";
            }
        }
        private void btnCapNhatKhuVuc_Click(object sender, EventArgs e)
        {
            if (txtTenKhu.Text.Trim() == "")
            {
                err.SetError(txtTenKhu, "VUI LÒNG NHẬP VÀO TÊN KHU");
                return;
            }
            if (BUS_Khu.Instance.DemTenKhu(txtTenKhu.Text.Trim()) != 0)
            {
                err.SetError(txtTenKhu, "KHU VỰC NÀY ĐÃ TỒN TẠI");
                return;
            }
            DTO_Khu khu = new DTO_Khu();
            khu.ID = int.Parse(dgvKhuVuc.CurrentRow.Cells[0].Value.ToString().Trim());
            khu.Ten = txtTenKhu.Text.Trim();
            if (!BUS_Khu.Instance.Update(khu))
            {
                MessageBox.Show("Lỗi cập nhật", "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                MessageBox.Show("Cập nhật thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                BUS_Database.Instance.GhiLichSu(frmDangNhap.Taikhoan.TaiKhoan, "Cập nhật khu: " + dgvKhuVuc.CurrentRow.Cells[0].Value.ToString().Trim()+" Thành "+txtTenKhu.Text, DateTime.Now);
                frmMain.LoadLichSu();
                LoadKhuVuc();
                cbbKhuVuc.DataSource = BUS_Khu.Instance.LayDanhSach();
                cbbKhuVuc.DisplayMember = "Ten";
                cbbKhuVuc.ValueMember = "ID";
                cbbNhomKhuVuc.DataSource = BUS_Khu.Instance.LayDanhSach();
                cbbNhomKhuVuc.DisplayMember = "Ten";
                cbbNhomKhuVuc.ValueMember = "ID";
            }
        }
        private void dgvBan_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                DataGridViewRow row = new DataGridViewRow();
                row = dgvBan.CurrentRow;
                txtID.Text = row.Cells[0].Value.ToString();
                txtTenBan.Text = row.Cells[1].Value.ToString();
                cbbKhuVuc.SelectedValue = row.Cells[3].Value;
            }
            catch (Exception)
            {
            }
        }
        private void dgvKhuVuc_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                DataGridViewRow row = dgvKhuVuc.CurrentRow;
                txtIDKhu.Text = row.Cells[0].Value.ToString().Trim();
                txtTenKhu.Text = row.Cells[1].Value.ToString().Trim();
            }
            catch (Exception)
            {
            }
        }
        private void txtID_TextChanged(object sender, EventArgs e)
        {
            err.SetError(txtID, "");
        }
        private void txtTenBan_TextChanged(object sender, EventArgs e)
        {
            err.SetError(txtTenBan, "");
        }
        private void txtIDKhu_TextChanged(object sender, EventArgs e)
        {
            err.SetError(txtIDKhu, "");
        }
        private void txtTenKhu_TextChanged(object sender, EventArgs e)
        {
            err.SetError(txtTenKhu, "");
        }
        #endregion

        private void btnShow_Click(object sender, EventArgs e)
        {
            btnShow.Checked = !btnShow.Checked;
            pnViTri.Visible = btnShow.Checked;
        }


    }
}
