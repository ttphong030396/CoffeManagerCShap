﻿namespace GUI
{
    partial class frmQuanLyBan
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            this.labelX4 = new DevComponents.DotNetBar.LabelX();
            this.nrudTrang = new System.Windows.Forms.NumericUpDown();
            this.btnDau = new DevComponents.DotNetBar.ButtonX();
            this.btnCuoi = new DevComponents.DotNetBar.ButtonX();
            this.btnSau = new DevComponents.DotNetBar.ButtonX();
            this.pnFooter = new DevComponents.DotNetBar.PanelEx();
            this.btnTruoc = new DevComponents.DotNetBar.ButtonX();
            this.ckNhom = new DevComponents.DotNetBar.Controls.CheckBoxX();
            this.cbbNhomKhuVuc = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.panelEx3 = new DevComponents.DotNetBar.PanelEx();
            this.panelEx1 = new DevComponents.DotNetBar.PanelEx();
            this.dgvBan = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.btnCapNhat = new DevComponents.DotNetBar.ButtonX();
            this.btnXoa = new DevComponents.DotNetBar.ButtonX();
            this.txtID = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.pnBackground = new DevComponents.DotNetBar.PanelEx();
            this.grThongTinViTri = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.panelEx4 = new DevComponents.DotNetBar.PanelEx();
            this.dgvKhuVuc = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.txtTenKhu = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.txtIDKhu = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.lb = new DevComponents.DotNetBar.LabelX();
            this.labelX6 = new DevComponents.DotNetBar.LabelX();
            this.btnCapNhatKhuVuc = new DevComponents.DotNetBar.ButtonX();
            this.btnXoaKhuVuc = new DevComponents.DotNetBar.ButtonX();
            this.btnThemKhuVuc = new DevComponents.DotNetBar.ButtonX();
            this.pnViTri = new DevComponents.DotNetBar.PanelEx();
            this.grThongTinBan = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.btnShow = new DevComponents.DotNetBar.ButtonX();
            this.btnThem = new DevComponents.DotNetBar.ButtonX();
            this.cbbKhuVuc = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.txtTenBan = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            ((System.ComponentModel.ISupportInitialize)(this.nrudTrang)).BeginInit();
            this.pnFooter.SuspendLayout();
            this.panelEx3.SuspendLayout();
            this.panelEx1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBan)).BeginInit();
            this.pnBackground.SuspendLayout();
            this.grThongTinViTri.SuspendLayout();
            this.panelEx4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvKhuVuc)).BeginInit();
            this.pnViTri.SuspendLayout();
            this.grThongTinBan.SuspendLayout();
            this.SuspendLayout();
            // 
            // labelX4
            // 
            // 
            // 
            // 
            this.labelX4.BackgroundStyle.Class = "";
            this.labelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX4.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX4.Location = new System.Drawing.Point(257, 6);
            this.labelX4.Name = "labelX4";
            this.labelX4.Size = new System.Drawing.Size(78, 35);
            this.labelX4.TabIndex = 5;
            this.labelX4.Text = "Trang";
            // 
            // nrudTrang
            // 
            this.nrudTrang.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nrudTrang.Location = new System.Drawing.Point(341, 6);
            this.nrudTrang.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nrudTrang.Name = "nrudTrang";
            this.nrudTrang.Size = new System.Drawing.Size(120, 38);
            this.nrudTrang.TabIndex = 4;
            this.nrudTrang.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.nrudTrang.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nrudTrang.ValueChanged += new System.EventHandler(this.nrudTrang_ValueChanged);
            // 
            // btnDau
            // 
            this.btnDau.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnDau.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnDau.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.btnDau.Location = new System.Drawing.Point(3, 6);
            this.btnDau.Name = "btnDau";
            this.btnDau.Size = new System.Drawing.Size(121, 38);
            this.btnDau.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnDau.TabIndex = 3;
            this.btnDau.Text = "Đầu";
            this.btnDau.Click += new System.EventHandler(this.btnDau_Click);
            // 
            // btnCuoi
            // 
            this.btnCuoi.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnCuoi.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnCuoi.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.btnCuoi.Location = new System.Drawing.Point(594, 6);
            this.btnCuoi.Name = "btnCuoi";
            this.btnCuoi.Size = new System.Drawing.Size(121, 38);
            this.btnCuoi.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnCuoi.TabIndex = 2;
            this.btnCuoi.Text = "Cuối";
            this.btnCuoi.Click += new System.EventHandler(this.btnCuoi_Click);
            // 
            // btnSau
            // 
            this.btnSau.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnSau.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnSau.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.btnSau.Location = new System.Drawing.Point(467, 6);
            this.btnSau.Name = "btnSau";
            this.btnSau.Size = new System.Drawing.Size(121, 38);
            this.btnSau.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnSau.TabIndex = 0;
            this.btnSau.Text = "Sau";
            this.btnSau.Click += new System.EventHandler(this.btnSau_Click);
            // 
            // pnFooter
            // 
            this.pnFooter.CanvasColor = System.Drawing.SystemColors.Control;
            this.pnFooter.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.pnFooter.Controls.Add(this.labelX4);
            this.pnFooter.Controls.Add(this.nrudTrang);
            this.pnFooter.Controls.Add(this.btnDau);
            this.pnFooter.Controls.Add(this.btnCuoi);
            this.pnFooter.Controls.Add(this.btnTruoc);
            this.pnFooter.Controls.Add(this.btnSau);
            this.pnFooter.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnFooter.Location = new System.Drawing.Point(0, 482);
            this.pnFooter.Name = "pnFooter";
            this.pnFooter.Size = new System.Drawing.Size(727, 50);
            this.pnFooter.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.pnFooter.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.pnFooter.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.pnFooter.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.pnFooter.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.pnFooter.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.pnFooter.Style.GradientAngle = 90;
            this.pnFooter.TabIndex = 0;
            // 
            // btnTruoc
            // 
            this.btnTruoc.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnTruoc.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnTruoc.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.btnTruoc.Location = new System.Drawing.Point(130, 6);
            this.btnTruoc.Name = "btnTruoc";
            this.btnTruoc.Size = new System.Drawing.Size(121, 38);
            this.btnTruoc.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnTruoc.TabIndex = 1;
            this.btnTruoc.Text = "Trước";
            this.btnTruoc.Click += new System.EventHandler(this.btnTruoc_Click);
            // 
            // ckNhom
            // 
            // 
            // 
            // 
            this.ckNhom.BackgroundStyle.Class = "";
            this.ckNhom.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ckNhom.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ckNhom.Location = new System.Drawing.Point(625, 3);
            this.ckNhom.Name = "ckNhom";
            this.ckNhom.Size = new System.Drawing.Size(90, 23);
            this.ckNhom.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.ckNhom.TabIndex = 1;
            this.ckNhom.Text = "Nhóm";
            this.ckNhom.CheckedChanged += new System.EventHandler(this.ckNhom_CheckedChanged);
            // 
            // cbbNhomKhuVuc
            // 
            this.cbbNhomKhuVuc.DisplayMember = "Text";
            this.cbbNhomKhuVuc.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cbbNhomKhuVuc.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbbNhomKhuVuc.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbNhomKhuVuc.FormattingEnabled = true;
            this.cbbNhomKhuVuc.ItemHeight = 20;
            this.cbbNhomKhuVuc.Location = new System.Drawing.Point(355, 3);
            this.cbbNhomKhuVuc.Name = "cbbNhomKhuVuc";
            this.cbbNhomKhuVuc.Size = new System.Drawing.Size(264, 26);
            this.cbbNhomKhuVuc.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cbbNhomKhuVuc.TabIndex = 0;
            this.cbbNhomKhuVuc.SelectedIndexChanged += new System.EventHandler(this.cbbNhomKhuVuc_SelectedIndexChanged);
            // 
            // panelEx3
            // 
            this.panelEx3.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelEx3.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.panelEx3.Controls.Add(this.ckNhom);
            this.panelEx3.Controls.Add(this.cbbNhomKhuVuc);
            this.panelEx3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelEx3.Location = new System.Drawing.Point(0, 0);
            this.panelEx3.Name = "panelEx3";
            this.panelEx3.Size = new System.Drawing.Size(727, 32);
            this.panelEx3.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelEx3.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelEx3.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.panelEx3.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelEx3.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelEx3.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelEx3.Style.GradientAngle = 90;
            this.panelEx3.TabIndex = 1;
            // 
            // panelEx1
            // 
            this.panelEx1.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelEx1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.panelEx1.Controls.Add(this.dgvBan);
            this.panelEx1.Controls.Add(this.panelEx3);
            this.panelEx1.Controls.Add(this.pnFooter);
            this.panelEx1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelEx1.Location = new System.Drawing.Point(0, 0);
            this.panelEx1.Name = "panelEx1";
            this.panelEx1.Size = new System.Drawing.Size(727, 532);
            this.panelEx1.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelEx1.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelEx1.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.panelEx1.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelEx1.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelEx1.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelEx1.Style.GradientAngle = 90;
            this.panelEx1.TabIndex = 0;
            this.panelEx1.Text = "panelEx1";
            // 
            // dgvBan
            // 
            this.dgvBan.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvBan.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvBan.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvBan.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvBan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvBan.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(170)))), ((int)(((byte)(170)))));
            this.dgvBan.Location = new System.Drawing.Point(0, 32);
            this.dgvBan.Name = "dgvBan";
            this.dgvBan.ReadOnly = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvBan.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvBan.Size = new System.Drawing.Size(727, 450);
            this.dgvBan.TabIndex = 2;
            this.dgvBan.SelectionChanged += new System.EventHandler(this.dgvBan_SelectionChanged);
            // 
            // btnCapNhat
            // 
            this.btnCapNhat.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnCapNhat.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnCapNhat.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCapNhat.Location = new System.Drawing.Point(521, 67);
            this.btnCapNhat.Name = "btnCapNhat";
            this.btnCapNhat.Size = new System.Drawing.Size(91, 40);
            this.btnCapNhat.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnCapNhat.TabIndex = 5;
            this.btnCapNhat.Text = "Cập nhật";
            this.btnCapNhat.Click += new System.EventHandler(this.btnCapNhat_Click);
            // 
            // btnXoa
            // 
            this.btnXoa.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnXoa.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnXoa.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXoa.Location = new System.Drawing.Point(521, 35);
            this.btnXoa.Name = "btnXoa";
            this.btnXoa.Size = new System.Drawing.Size(91, 26);
            this.btnXoa.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnXoa.TabIndex = 4;
            this.btnXoa.Text = "Xóa";
            this.btnXoa.Click += new System.EventHandler(this.btnXoa_Click);
            // 
            // txtID
            // 
            // 
            // 
            // 
            this.txtID.Border.Class = "TextBoxBorder";
            this.txtID.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtID.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtID.Location = new System.Drawing.Point(101, 12);
            this.txtID.Name = "txtID";
            this.txtID.ReadOnly = true;
            this.txtID.Size = new System.Drawing.Size(416, 26);
            this.txtID.TabIndex = 0;
            this.txtID.TextChanged += new System.EventHandler(this.txtID_TextChanged);
            // 
            // pnBackground
            // 
            this.pnBackground.CanvasColor = System.Drawing.SystemColors.Control;
            this.pnBackground.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.pnBackground.Controls.Add(this.grThongTinViTri);
            this.pnBackground.Controls.Add(this.grThongTinBan);
            this.pnBackground.Controls.Add(this.panelEx1);
            this.pnBackground.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnBackground.Location = new System.Drawing.Point(0, 0);
            this.pnBackground.Name = "pnBackground";
            this.pnBackground.Size = new System.Drawing.Size(1354, 532);
            this.pnBackground.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.pnBackground.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.pnBackground.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.pnBackground.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.pnBackground.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.pnBackground.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.pnBackground.Style.GradientAngle = 90;
            this.pnBackground.TabIndex = 1;
            // 
            // grThongTinViTri
            // 
            this.grThongTinViTri.CanvasColor = System.Drawing.SystemColors.Control;
            this.grThongTinViTri.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.grThongTinViTri.Controls.Add(this.panelEx4);
            this.grThongTinViTri.Controls.Add(this.pnViTri);
            this.grThongTinViTri.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grThongTinViTri.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grThongTinViTri.Location = new System.Drawing.Point(727, 152);
            this.grThongTinViTri.Name = "grThongTinViTri";
            this.grThongTinViTri.Size = new System.Drawing.Size(627, 380);
            // 
            // 
            // 
            this.grThongTinViTri.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.grThongTinViTri.Style.BackColorGradientAngle = 90;
            this.grThongTinViTri.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.grThongTinViTri.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grThongTinViTri.Style.BorderBottomWidth = 1;
            this.grThongTinViTri.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.grThongTinViTri.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grThongTinViTri.Style.BorderLeftWidth = 1;
            this.grThongTinViTri.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grThongTinViTri.Style.BorderRightWidth = 1;
            this.grThongTinViTri.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grThongTinViTri.Style.BorderTopWidth = 1;
            this.grThongTinViTri.Style.Class = "";
            this.grThongTinViTri.Style.CornerDiameter = 4;
            this.grThongTinViTri.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.grThongTinViTri.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.grThongTinViTri.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.grThongTinViTri.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.grThongTinViTri.StyleMouseDown.Class = "";
            this.grThongTinViTri.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.grThongTinViTri.StyleMouseOver.Class = "";
            this.grThongTinViTri.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.grThongTinViTri.TabIndex = 11;
            this.grThongTinViTri.Text = "THÔNG TIN KHU";
            // 
            // panelEx4
            // 
            this.panelEx4.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelEx4.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.panelEx4.Controls.Add(this.dgvKhuVuc);
            this.panelEx4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelEx4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panelEx4.Location = new System.Drawing.Point(0, 0);
            this.panelEx4.Name = "panelEx4";
            this.panelEx4.Size = new System.Drawing.Size(621, 294);
            this.panelEx4.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelEx4.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelEx4.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.panelEx4.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelEx4.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelEx4.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelEx4.Style.GradientAngle = 90;
            this.panelEx4.TabIndex = 2;
            this.panelEx4.Text = "panelEx4";
            // 
            // dgvKhuVuc
            // 
            this.dgvKhuVuc.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvKhuVuc.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvKhuVuc.DefaultCellStyle = dataGridViewCellStyle4;
            this.dgvKhuVuc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvKhuVuc.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(170)))), ((int)(((byte)(170)))));
            this.dgvKhuVuc.Location = new System.Drawing.Point(0, 0);
            this.dgvKhuVuc.MultiSelect = false;
            this.dgvKhuVuc.Name = "dgvKhuVuc";
            this.dgvKhuVuc.ReadOnly = true;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgvKhuVuc.RowsDefaultCellStyle = dataGridViewCellStyle5;
            this.dgvKhuVuc.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvKhuVuc.Size = new System.Drawing.Size(621, 294);
            this.dgvKhuVuc.TabIndex = 2;
            this.dgvKhuVuc.SelectionChanged += new System.EventHandler(this.dgvKhuVuc_SelectionChanged);
            // 
            // txtTenKhu
            // 
            // 
            // 
            // 
            this.txtTenKhu.Border.Class = "TextBoxBorder";
            this.txtTenKhu.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtTenKhu.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenKhu.Location = new System.Drawing.Point(193, 12);
            this.txtTenKhu.Name = "txtTenKhu";
            this.txtTenKhu.Size = new System.Drawing.Size(130, 26);
            this.txtTenKhu.TabIndex = 1;
            this.txtTenKhu.TextChanged += new System.EventHandler(this.txtTenKhu_TextChanged);
            // 
            // txtIDKhu
            // 
            // 
            // 
            // 
            this.txtIDKhu.Border.Class = "TextBoxBorder";
            this.txtIDKhu.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtIDKhu.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtIDKhu.Location = new System.Drawing.Point(33, 12);
            this.txtIDKhu.Name = "txtIDKhu";
            this.txtIDKhu.ReadOnly = true;
            this.txtIDKhu.Size = new System.Drawing.Size(124, 26);
            this.txtIDKhu.TabIndex = 0;
            this.txtIDKhu.TextChanged += new System.EventHandler(this.txtIDKhu_TextChanged);
            // 
            // lb
            // 
            // 
            // 
            // 
            this.lb.BackgroundStyle.Class = "";
            this.lb.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lb.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb.Location = new System.Drawing.Point(7, 12);
            this.lb.Name = "lb";
            this.lb.Size = new System.Drawing.Size(103, 23);
            this.lb.TabIndex = 15;
            this.lb.Text = "ID";
            // 
            // labelX6
            // 
            // 
            // 
            // 
            this.labelX6.BackgroundStyle.Class = "";
            this.labelX6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX6.Location = new System.Drawing.Point(160, 12);
            this.labelX6.Name = "labelX6";
            this.labelX6.Size = new System.Drawing.Size(103, 23);
            this.labelX6.TabIndex = 14;
            this.labelX6.Text = "Tên";
            // 
            // btnCapNhatKhuVuc
            // 
            this.btnCapNhatKhuVuc.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnCapNhatKhuVuc.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnCapNhatKhuVuc.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCapNhatKhuVuc.Location = new System.Drawing.Point(523, 6);
            this.btnCapNhatKhuVuc.Name = "btnCapNhatKhuVuc";
            this.btnCapNhatKhuVuc.Size = new System.Drawing.Size(91, 35);
            this.btnCapNhatKhuVuc.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnCapNhatKhuVuc.TabIndex = 4;
            this.btnCapNhatKhuVuc.Text = "Cập nhật";
            this.btnCapNhatKhuVuc.Click += new System.EventHandler(this.btnCapNhatKhuVuc_Click);
            // 
            // btnXoaKhuVuc
            // 
            this.btnXoaKhuVuc.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnXoaKhuVuc.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnXoaKhuVuc.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXoaKhuVuc.Location = new System.Drawing.Point(426, 6);
            this.btnXoaKhuVuc.Name = "btnXoaKhuVuc";
            this.btnXoaKhuVuc.Size = new System.Drawing.Size(91, 35);
            this.btnXoaKhuVuc.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnXoaKhuVuc.TabIndex = 3;
            this.btnXoaKhuVuc.Text = "Xóa";
            this.btnXoaKhuVuc.Click += new System.EventHandler(this.btnXoaKhuVuc_Click);
            // 
            // btnThemKhuVuc
            // 
            this.btnThemKhuVuc.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnThemKhuVuc.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnThemKhuVuc.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnThemKhuVuc.Location = new System.Drawing.Point(329, 6);
            this.btnThemKhuVuc.Name = "btnThemKhuVuc";
            this.btnThemKhuVuc.Size = new System.Drawing.Size(91, 35);
            this.btnThemKhuVuc.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnThemKhuVuc.TabIndex = 2;
            this.btnThemKhuVuc.Text = "Thêm";
            this.btnThemKhuVuc.Click += new System.EventHandler(this.btnThemKhuVuc_Click);
            // 
            // pnViTri
            // 
            this.pnViTri.CanvasColor = System.Drawing.SystemColors.Control;
            this.pnViTri.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.pnViTri.Controls.Add(this.btnCapNhatKhuVuc);
            this.pnViTri.Controls.Add(this.txtTenKhu);
            this.pnViTri.Controls.Add(this.btnXoaKhuVuc);
            this.pnViTri.Controls.Add(this.labelX6);
            this.pnViTri.Controls.Add(this.btnThemKhuVuc);
            this.pnViTri.Controls.Add(this.txtIDKhu);
            this.pnViTri.Controls.Add(this.lb);
            this.pnViTri.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnViTri.Location = new System.Drawing.Point(0, 294);
            this.pnViTri.Name = "pnViTri";
            this.pnViTri.Size = new System.Drawing.Size(621, 47);
            this.pnViTri.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.pnViTri.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.pnViTri.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.pnViTri.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.pnViTri.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.pnViTri.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.pnViTri.Style.GradientAngle = 90;
            this.pnViTri.TabIndex = 0;
            // 
            // grThongTinBan
            // 
            this.grThongTinBan.CanvasColor = System.Drawing.SystemColors.Control;
            this.grThongTinBan.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.grThongTinBan.Controls.Add(this.btnShow);
            this.grThongTinBan.Controls.Add(this.btnCapNhat);
            this.grThongTinBan.Controls.Add(this.btnXoa);
            this.grThongTinBan.Controls.Add(this.txtID);
            this.grThongTinBan.Controls.Add(this.btnThem);
            this.grThongTinBan.Controls.Add(this.cbbKhuVuc);
            this.grThongTinBan.Controls.Add(this.txtTenBan);
            this.grThongTinBan.Controls.Add(this.labelX3);
            this.grThongTinBan.Controls.Add(this.labelX1);
            this.grThongTinBan.Controls.Add(this.labelX2);
            this.grThongTinBan.Dock = System.Windows.Forms.DockStyle.Top;
            this.grThongTinBan.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grThongTinBan.Location = new System.Drawing.Point(727, 0);
            this.grThongTinBan.Name = "grThongTinBan";
            this.grThongTinBan.Size = new System.Drawing.Size(627, 152);
            // 
            // 
            // 
            this.grThongTinBan.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.grThongTinBan.Style.BackColorGradientAngle = 90;
            this.grThongTinBan.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.grThongTinBan.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grThongTinBan.Style.BorderBottomWidth = 1;
            this.grThongTinBan.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.grThongTinBan.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grThongTinBan.Style.BorderLeftWidth = 1;
            this.grThongTinBan.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grThongTinBan.Style.BorderRightWidth = 1;
            this.grThongTinBan.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grThongTinBan.Style.BorderTopWidth = 1;
            this.grThongTinBan.Style.Class = "";
            this.grThongTinBan.Style.CornerDiameter = 4;
            this.grThongTinBan.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.grThongTinBan.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.grThongTinBan.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.grThongTinBan.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.grThongTinBan.StyleMouseDown.Class = "";
            this.grThongTinBan.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.grThongTinBan.StyleMouseOver.Class = "";
            this.grThongTinBan.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.grThongTinBan.TabIndex = 10;
            this.grThongTinBan.Text = "THÔNG TIN BÀN";
            // 
            // btnShow
            // 
            this.btnShow.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnShow.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnShow.Location = new System.Drawing.Point(440, 76);
            this.btnShow.Name = "btnShow";
            this.btnShow.Size = new System.Drawing.Size(75, 26);
            this.btnShow.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnShow.TabIndex = 7;
            this.btnShow.Text = "=";
            this.btnShow.Click += new System.EventHandler(this.btnShow_Click);
            // 
            // btnThem
            // 
            this.btnThem.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnThem.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnThem.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnThem.Location = new System.Drawing.Point(521, 3);
            this.btnThem.Name = "btnThem";
            this.btnThem.Size = new System.Drawing.Size(91, 26);
            this.btnThem.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnThem.TabIndex = 3;
            this.btnThem.Text = "Thêm";
            this.btnThem.Click += new System.EventHandler(this.btnThem_Click);
            // 
            // cbbKhuVuc
            // 
            this.cbbKhuVuc.DisplayMember = "Text";
            this.cbbKhuVuc.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cbbKhuVuc.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbbKhuVuc.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbKhuVuc.FormattingEnabled = true;
            this.cbbKhuVuc.ItemHeight = 20;
            this.cbbKhuVuc.Location = new System.Drawing.Point(101, 76);
            this.cbbKhuVuc.Name = "cbbKhuVuc";
            this.cbbKhuVuc.Size = new System.Drawing.Size(332, 26);
            this.cbbKhuVuc.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cbbKhuVuc.TabIndex = 2;
            // 
            // txtTenBan
            // 
            // 
            // 
            // 
            this.txtTenBan.Border.Class = "TextBoxBorder";
            this.txtTenBan.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtTenBan.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenBan.Location = new System.Drawing.Point(101, 44);
            this.txtTenBan.Name = "txtTenBan";
            this.txtTenBan.Size = new System.Drawing.Size(416, 26);
            this.txtTenBan.TabIndex = 1;
            this.txtTenBan.TextChanged += new System.EventHandler(this.txtTenBan_TextChanged);
            // 
            // labelX3
            // 
            // 
            // 
            // 
            this.labelX3.BackgroundStyle.Class = "";
            this.labelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX3.Location = new System.Drawing.Point(14, 47);
            this.labelX3.Name = "labelX3";
            this.labelX3.Size = new System.Drawing.Size(75, 23);
            this.labelX3.TabIndex = 6;
            this.labelX3.Text = "Tên bàn";
            // 
            // labelX1
            // 
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.Class = "";
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX1.Location = new System.Drawing.Point(14, 15);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(75, 23);
            this.labelX1.TabIndex = 4;
            this.labelX1.Text = "ID";
            // 
            // labelX2
            // 
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.Class = "";
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX2.Location = new System.Drawing.Point(14, 79);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(75, 23);
            this.labelX2.TabIndex = 5;
            this.labelX2.Text = "Vị trí:";
            // 
            // frmQuanLyBan
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1354, 532);
            this.Controls.Add(this.pnBackground);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmQuanLyBan";
            this.Text = "frmQuanLyBan";
            ((System.ComponentModel.ISupportInitialize)(this.nrudTrang)).EndInit();
            this.pnFooter.ResumeLayout(false);
            this.panelEx3.ResumeLayout(false);
            this.panelEx1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvBan)).EndInit();
            this.pnBackground.ResumeLayout(false);
            this.grThongTinViTri.ResumeLayout(false);
            this.panelEx4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvKhuVuc)).EndInit();
            this.pnViTri.ResumeLayout(false);
            this.grThongTinBan.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.LabelX labelX4;
        private System.Windows.Forms.NumericUpDown nrudTrang;
        private DevComponents.DotNetBar.ButtonX btnDau;
        private DevComponents.DotNetBar.ButtonX btnCuoi;
        private DevComponents.DotNetBar.ButtonX btnSau;
        private DevComponents.DotNetBar.PanelEx pnFooter;
        private DevComponents.DotNetBar.ButtonX btnTruoc;
        private DevComponents.DotNetBar.Controls.CheckBoxX ckNhom;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cbbNhomKhuVuc;
        private DevComponents.DotNetBar.PanelEx panelEx3;
        private DevComponents.DotNetBar.PanelEx panelEx1;
        private DevComponents.DotNetBar.Controls.DataGridViewX dgvBan;
        private DevComponents.DotNetBar.ButtonX btnCapNhat;
        private DevComponents.DotNetBar.ButtonX btnXoa;
        private DevComponents.DotNetBar.Controls.TextBoxX txtID;
        private DevComponents.DotNetBar.PanelEx pnBackground;
        private DevComponents.DotNetBar.Controls.GroupPanel grThongTinViTri;
        private DevComponents.DotNetBar.Controls.DataGridViewX dgvKhuVuc;
        private DevComponents.DotNetBar.Controls.TextBoxX txtTenKhu;
        private DevComponents.DotNetBar.Controls.TextBoxX txtIDKhu;
        private DevComponents.DotNetBar.LabelX lb;
        private DevComponents.DotNetBar.LabelX labelX6;
        private DevComponents.DotNetBar.ButtonX btnCapNhatKhuVuc;
        private DevComponents.DotNetBar.ButtonX btnXoaKhuVuc;
        private DevComponents.DotNetBar.ButtonX btnThemKhuVuc;
        private DevComponents.DotNetBar.PanelEx pnViTri;
        private DevComponents.DotNetBar.Controls.GroupPanel grThongTinBan;
        private DevComponents.DotNetBar.ButtonX btnThem;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cbbKhuVuc;
        private DevComponents.DotNetBar.Controls.TextBoxX txtTenBan;
        private DevComponents.DotNetBar.LabelX labelX3;
        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.DotNetBar.LabelX labelX2;
        private DevComponents.DotNetBar.ButtonX btnShow;
        private DevComponents.DotNetBar.PanelEx panelEx4;
    }
}