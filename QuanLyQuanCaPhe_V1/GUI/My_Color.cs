﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GUI
{
    public class My_Color
    {
        private static My_Color instance;
        public static My_Color Instance
        {
            get { if (instance == null) instance = new My_Color(); return My_Color.instance; }
            set { }
        }
        private My_Color() { }

        public Color BanCoNguoi = Color.FromArgb(255, 0, 0);
        public Color BanTrong = Color.FromArgb(0, 255, 0);
        public Color ChuyenBan = Color.Blue;


    }
}
