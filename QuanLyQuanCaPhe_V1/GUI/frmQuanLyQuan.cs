﻿using System;
using DevComponents.DotNetBar;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DTO;
using BUS;
using DevComponents.DotNetBar.Rendering;
using DevComponents.DotNetBar.Controls;
using System.Drawing.Printing;
namespace GUI
{
    public partial class frmQuanLyQuan : Form
    {
        #region Properties
        private List<Button_Ban> lstBan = new List<Button_Ban>();
        private List<DTO_MonAn> lstMonAn = new List<DTO_MonAn>();
        private List<Button_LoaiMon> lstButtonLoaiMon = new List<Button_LoaiMon>();
        private int ViTriBanCanChuyen=-1;
        private int ViTriBanCanDen=-1;
        private int ViTriDangChon = -1;
        #endregion
        #region Method
        public frmQuanLyQuan()
        {
            InitializeComponent();
            HienThiDanhSachBan();
            CauHinh();
            HienThiDanhSachLoaiThucAn(Properties.Settings.Default.isModeDem);
            try
            {
                ButtonLoaiMon_Click(lstButtonLoaiMon[Properties.Settings.Default.ViTriLoaiMon], new EventArgs());
            }
            catch (Exception)
            {
                Properties.Settings.Default.ViTriLoaiMon = 0;
                Properties.Settings.Default.Save();
                ButtonLoaiMon_Click(lstButtonLoaiMon[Properties.Settings.Default.ViTriLoaiMon], new EventArgs());
            }
        }
        private void CauHinh()
        {
            btnChuyenBan.Checked = false;
            pnThongTin.Visible = false;
            btnInHoaDon.Enabled = false;
            btnChuyenBan.Enabled = false;
            btnGiamGia.Enabled = false;
            btnThanhToan.Enabled = false;
            pnChucNang.Visible = false;
            lbThanhTien.Text = "0 VNĐ";
        }
        private string FloatToString(object ob)
        {
            try
            {
                if (ob.ToString().Trim().Length == 0)
                    return " ";
                if (ob.ToString() == "0")
                    return "0";
                decimal thanhtien = Convert.ToDecimal(ob);
                string strthanhtien = string.Format("{0:#,#}", thanhtien);
                return strthanhtien;
            }
            catch (Exception)
            {
                throw;
            }
        }
        private void HienThiDanhSachBan()
        {
            List<DTO_Khu> lstKhu = BUS_Khu.Instance.LayDanhSach();
            int i = 0;
            foreach (DTO_Khu item in lstKhu)
            {
                List<DTO_Ban> lstBan = BUS_Ban.Instance.LayDanhSach_Khu(item.ID);
                FlowLayoutPanel pnKhu = new FlowLayoutPanel();
                foreach (DTO_Ban item_1 in lstBan)
                {
                    Button_Ban My_Button = new Button_Ban();
                    My_Button.ColorTable = eButtonColor.Orange;
                    My_Button.Size = new Size(50, 48);
                    My_Button.ForeColor = Color.Red;
                    //My_Button.Shape = new EllipticalShapeDescriptor();
                    //My_Button.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(20);
                    My_Button.Font = new Font("Arial", 12, FontStyle.Bold);
                    My_Button.Index = i;
                    My_Button.IdBan = item_1.ID;
                    My_Button.Text = item_1.Ten;
                    My_Button.TinhTrang = item_1.TinhTrang;
                    if (My_Button.TinhTrang.Equals("Có Người"))
                    {
                        DTO_HoaDon hd = new DTO_HoaDon();
                        hd = BUS_HoaDon.Instance.LayHoaDon_IDBan(My_Button.IdBan);
                        My_Button.BackColor = My_Color.Instance.BanCoNguoi;
                        My_Button.IdHoaDon = hd.ID;
                        My_Button.PhanTramGiamGia = hd.GiamGia;
                        My_Button.TienGiam = hd.TienGiam;
                        My_Button.TongTien = hd.TongTien;
                        My_Button.ThanhTienDaGiam = hd.TongTien - hd.TienGiam;

                    }
                    else
                    {
                        My_Button.BackColor = My_Color.Instance.BanTrong;
                        My_Button.IdHoaDon = 0;
                        My_Button.PhanTramGiamGia = 0;
                        My_Button.TienGiam = 0;
                        My_Button.ThanhTienDaGiam = 0;
                        My_Button.TongTien = 0;
                    }
                    My_Button.IdKhu = item.ID;
                    My_Button.TenKhu = item.Ten;
                    My_Button.Click += My_Button_Click;
                    this.lstBan.Add(My_Button);
                    pnKhu.Controls.Add(My_Button);
                    pnKhu.Dock = DockStyle.Top;
                    pnKhu.AutoSize = true;
                    pnHienThiBan.Controls.Add(pnKhu);
                    i++;
                }
            }
        }
        private float LamTron(float ob)
        {
            float ketqua = (float)((int)ob / 100);
            int donvi = (int)ketqua % 10;
            if (donvi == 0 || donvi == 5)
                return ketqua * 100;
            else
                if (donvi > 5)
                    return (ketqua + (10 - donvi)) * 100;
                else if (donvi < 5)
                    return (ketqua + (5 - donvi)) * 100;
            return ob;
        }
        private void SetTrangThaiButton(Button_Ban mbtn)
        {
            try
            {
                if (mbtn.TinhTrang.Equals("Trống"))
                {
                    btnThanhToan.Enabled = false;
                    btnChuyenBan.Enabled = false;
                    btnGiamGia.Enabled = false;
                    btnInHoaDon.Enabled = false;
                    pnChucNang.Visible = false;
                }
                else
                {
                    btnThanhToan.Enabled = true;
                    btnChuyenBan.Enabled = true;
                    btnGiamGia.Enabled = true;
                    btnInHoaDon.Enabled = true;
                    pnChucNang.Visible = true;
                }
                if (mbtn.PhanTramGiamGia != 0)
                {
                    btnGiamGia.Text = "Giảm Giá " + "(" + mbtn.PhanTramGiamGia.ToString() + "%)";
                }
                else btnGiamGia.Text = "Giảm Giá ";
                mbtn.TienGiam = LamTron(mbtn.TongTien * mbtn.PhanTramGiamGia / (float)100);
                mbtn.ThanhTienDaGiam = mbtn.TongTien - mbtn.TienGiam;
                lbThanhTien.Text = "Tổng Cộng: "+FloatToString(mbtn.ThanhTienDaGiam) + " VNĐ";
            }
            catch (Exception)
            {
            }
        }
        private void HienThiDanhSachLoaiThucAn(bool isModeDem)
        {
            try
            {
                List<DTO_LoaiMon> lstLoaiMon = new List<DTO_LoaiMon>();
                lstLoaiMon = BUS_LoaiMon.Instance.LayDanhSach();
                switch (isModeDem)
                {
                    case false:
                        {
                            btnCheDo.Checked = false;
                            btnCheDo.Text = "Chế độ NGÀY";
                            //Ban ngày
                            if (lstLoaiMon.Count != 0)
                            {
                                byte i = 0;
                                foreach (DTO_LoaiMon item in lstLoaiMon)
                                {
                                    List<DTO_MonAn> lstMon = new List<DTO_MonAn>();
                                    lstMon = BUS_MonAn.Instance.LayDanhSach_IDLoai(item.ID);
                                    if (lstMon.Count > 0)
                                    {
                                        Button_LoaiMon ButtonLoaiMon = new Button_LoaiMon();
                                        ButtonLoaiMon.IDLoaiMon = item.ID;
                                        ButtonLoaiMon.Mode = "Ngày";
                                        ButtonLoaiMon.Text = item.Ten;
                                        ButtonLoaiMon.ViTri = i++;
                                        ButtonLoaiMon.Font = new System.Drawing.Font("Microsoft Sans Serif", 12, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
                                        ButtonLoaiMon.Size = new Size(142, 30);
                                        ButtonLoaiMon.ColorTable = eButtonColor.OrangeWithBackground;
                                        ButtonLoaiMon.Click += ButtonLoaiMon_Click;
                                        pnButton_LoaiMon.Controls.Add(ButtonLoaiMon);
                                        lstButtonLoaiMon.Add(ButtonLoaiMon);
                                    }
                                }
                            }
                            break;
                        }
                    case true:
                        {
                            btnCheDo.Checked = true;
                            btnCheDo.Text = "Chế độ ĐÊM";
                            //Ban đêm
                            if (lstLoaiMon.Count != 0)
                            {
                                byte i = 0;
                                foreach (DTO_LoaiMon item in lstLoaiMon)
                                {
                                    List<DTO_MonAn> lstMon = new List<DTO_MonAn>();
                                    lstMon = BUS_MonAn.Instance.LayDanhSach_Mode_IDLoai("Đêm", item.ID);
                                    if (lstMon.Count > 0)
                                    {
                                        Button_LoaiMon ButtonLoaiMon = new Button_LoaiMon();
                                        ButtonLoaiMon.Mode = "Đêm";
                                        ButtonLoaiMon.IDLoaiMon = item.ID;
                                        ButtonLoaiMon.Text = item.Ten;
                                        ButtonLoaiMon.ViTri = i++;
                                        ButtonLoaiMon.Font = new System.Drawing.Font("Microsoft Sans Serif", 12, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
                                        ButtonLoaiMon.Size = new Size(142 , 30);
                                        ButtonLoaiMon.ColorTable = eButtonColor.OrangeWithBackground;
                                        ButtonLoaiMon.Click += ButtonLoaiMon_Click;
                                        pnButton_LoaiMon.Controls.Add(ButtonLoaiMon);
                                        lstButtonLoaiMon.Add(ButtonLoaiMon);
                                    }
                                }
                            }
                        }
                        break;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        private void SetColorMyButton(Button_Ban mbtn)
        {
            try
            {
                if (mbtn.TinhTrang.Equals("Trống"))
                    mbtn.BackColor = My_Color.Instance.BanTrong;
                else
                    mbtn.BackColor = My_Color.Instance.BanCoNguoi;
                mbtn.Checked = false;
            }
            catch (Exception)
            {
            }
        }
        private bool DaThanhToanHet()
        {
            foreach (Button_Ban item in lstBan)
                if (item.IdHoaDon != 0)
                    return false;
            return true;
        }
        private void CapNhatTrangThaiHoaDon(Button_Ban mbtn)
        {
            try
            {
                lstDanhSachHoaDonChiTiet.Clear();
                List<DTO_HoaDon_HoaDonChiTiet_MonAn> lsttmp = new List<DTO_HoaDon_HoaDonChiTiet_MonAn>();
                lsttmp = BUS_HoaDonChiTiet.Instance.LayDanhSach_IDBan(mbtn.IdBan);
                if(lsttmp.Count > 0)
                {
                    lstDanhSachHoaDonChiTiet.Columns.Add("Tên Món", 143);
                    lstDanhSachHoaDonChiTiet.Columns.Add("SL", 50);
                    lstDanhSachHoaDonChiTiet.Columns.Add("Đơn Giá", 80);
                    lstDanhSachHoaDonChiTiet.Columns.Add("Thành Tiền", 100);
                    float TongTien = 0;
                    foreach (DTO_HoaDon_HoaDonChiTiet_MonAn item in lsttmp)
                    {
                        ListViewItem lst = new ListViewItem(item.TenMonAn);

                        lst.SubItems.Add(item.SoLuongMonAn.ToString());
                        if (Properties.Settings.Default.isModeDem)
                        {
                            lst.SubItems.Add(item.GiaDem.ToString());
                            lst.SubItems.Add(item.TongDem.ToString());
                            TongTien += item.TongDem;
                        }
                        else
                        {
                            lst.SubItems.Add(item.GiaNgay.ToString());
                            lst.SubItems.Add(item.TongNgay.ToString());
                            TongTien += item.TongNgay;
                        }
                        lst.Tag = item.IDMonAn;
                        lstDanhSachHoaDonChiTiet.Items.Add(lst);
                    }
                    mbtn.TongTien = TongTien;
                }

            }
            catch (Exception)
            {
            }
        }
        private void ChuyenBan(Button_Ban BanCanChuyen, Button_Ban BanCanDen)
        {
            try
            {
                // chuyến bàn
                if (BanCanDen.IdHoaDon == 0)
                {
                    BUS_HoaDon.Instance.ChuyenBan(BanCanChuyen.IdHoaDon, BanCanDen.IdBan);
                    BanCanChuyen.TinhTrang = "Trống";
                    BUS_Ban.Instance.CapNhatTrangThaiBan(BanCanChuyen.IdBan, BanCanChuyen.TinhTrang);
                    BanCanDen.TinhTrang = "Có Người";
                    BUS_Ban.Instance.CapNhatTrangThaiBan(BanCanDen.IdBan, BanCanDen.TinhTrang);
                    BanCanDen.PhanTramGiamGia = BanCanChuyen.PhanTramGiamGia;
                    BanCanDen.IdHoaDon = BanCanChuyen.IdHoaDon;
                    BanCanChuyen.PhanTramGiamGia = 0;
                    SetTrangThaiButton(BanCanDen);
                    BanCanChuyen.IdHoaDon = 0;
                    BUS_Database.Instance.GhiLichSu(frmDangNhap.Taikhoan.TaiKhoan, "Chuyển Bàn " + BanCanChuyen.Text + " Sang bàn: " + BanCanDen.Text, DateTime.Now);
                    frmMain.LoadLichSu();
                }
                else
                {
                    // Gộp bàn
                    List<DTO_HoaDon_HoaDonChiTiet_MonAn> lstBillBanChuyen = new List<DTO_HoaDon_HoaDonChiTiet_MonAn>();
                    List<DTO_HoaDon_HoaDonChiTiet_MonAn> lstBillBanDen = new List<DTO_HoaDon_HoaDonChiTiet_MonAn>();
                    lstBillBanChuyen = BUS_HoaDonChiTiet.Instance.LayDanhSach_IDBan(BanCanChuyen.IdBan);
                    lstBillBanDen = BUS_HoaDonChiTiet.Instance.LayDanhSach_IDBan(BanCanDen.IdBan);
                    BUS_Database.Instance.GhiLichSu(frmDangNhap.Taikhoan.TaiKhoan, "Gộp Bàn " + BanCanChuyen.Text + " Vào bàn: " + BanCanDen.Text, DateTime.Now);
                    frmMain.LoadLichSu();
                    //
                    for (int i = 0; i < lstBillBanChuyen.Count; i++)
                    {
                        int idhoadonchitietXOA = (int)BUS_HoaDonChiTiet.Instance.LayIDHoaDonChiTiet_IDHoaDon_IDMonAn(lstBillBanChuyen[i].IDHoaDon, lstBillBanChuyen[i].IDMonAn);
                        for (int j = 0; j < lstBillBanDen.Count; j++)
                        {
                            // Kiểm tra món ăn
                            if (lstBillBanChuyen[i].IDMonAn == lstBillBanDen[j].IDMonAn)
                            {
                                int idhoadonchitiet = (int)BUS_HoaDonChiTiet.Instance.LayIDHoaDonChiTiet_IDHoaDon_IDMonAn(lstBillBanDen[j].IDHoaDon, lstBillBanDen[j].IDMonAn);
                                if (Properties.Settings.Default.isModeDem)
                                    BUS_HoaDonChiTiet.Instance.Update(idhoadonchitiet, lstBillBanDen[j].IDMonAn, lstBillBanChuyen[i].SoLuongMonAn, lstBillBanChuyen[i].GiaDem);
                                else
                                    BUS_HoaDonChiTiet.Instance.Update(idhoadonchitiet, lstBillBanDen[j].IDMonAn, lstBillBanChuyen[i].SoLuongMonAn, lstBillBanChuyen[i].GiaNgay);
                                BUS_HoaDonChiTiet.Instance.Delete(idhoadonchitietXOA);
                                break;
                            }
                            // nếu đến cuối danh sách mà vẫn chưa tìm thấy thì insertbill mới
                            if (j == lstBillBanDen.Count - 1)
                            {
                                if (Properties.Settings.Default.isModeDem)
                                    BUS_HoaDonChiTiet.Instance.Insert(lstBillBanDen[j].IDHoaDon, lstBillBanChuyen[i].IDMonAn, lstBillBanChuyen[i].SoLuongMonAn, lstBillBanChuyen[i].GiaDem);
                                else
                                    BUS_HoaDonChiTiet.Instance.Insert(lstBillBanDen[j].IDHoaDon, lstBillBanChuyen[i].IDMonAn, lstBillBanChuyen[i].SoLuongMonAn, lstBillBanChuyen[i].GiaNgay);
                                BUS_HoaDonChiTiet.Instance.Delete(idhoadonchitietXOA);
                            }
                        }
                    }
                    //Xoa hoa don BanCanChuyen
                    BUS_HoaDon.Instance.Delete(BanCanChuyen.IdHoaDon);
                    BanCanChuyen.IdHoaDon = 0;
                    BanCanChuyen.TinhTrang = "Trống";
                    BanCanChuyen.PhanTramGiamGia = 0;
                    BUS_Ban.Instance.CapNhatTrangThaiBan(BanCanChuyen.IdBan, BanCanChuyen.TinhTrang);
                    SetTrangThaiButton(BanCanChuyen);
                    SetTrangThaiButton(BanCanDen);

                }
            }
            catch (Exception)
            {
            }
        }
        private string DatetimeToString_dd_MM_yyyy(DateTime p)
        {
            string s = "";
            int ngay = p.Day;
            int thang = p.Month;
            int nam = p.Year;


            if (ngay < 10)
                s += "0" + ngay.ToString();
            else
                s += ngay.ToString();
            if (thang < 10)
                s += "/0" + thang.ToString();
            else
                s += "/" + thang.ToString();
            s += "/" + nam.ToString() + " ";
            return s;
        }
        private string DatetimeToString_hh_mm(DateTime p)
        {
            string s = "";
            int gio = p.Hour;
            int phut = p.Minute;
            if (gio < 10)
                s += "0" + gio.ToString() + "h";
            else
                s += gio.ToString() + "h";

            if (phut < 10)
                s += "0" + phut.ToString() + @"'";
            else
                s += phut.ToString() + @"'";
            return s;
        }
        #endregion
        #region Events
        private void My_Button_Click(object sender, EventArgs e)
        {
            try
            {
                Button_Ban mbtn = (Button_Ban)sender;

                if (btnChuyenBan.Checked)
                {
                    ViTriBanCanDen = mbtn.Index;
                    mbtn.Checked = false;
                    mbtn.BackColor = My_Color.Instance.ChuyenBan;
                    if (MessageBox.Show("Bạn có muốn chuyển từ bàn: " + lstBan[ViTriBanCanChuyen].Text + " sang bàn: " + lstBan[ViTriBanCanDen].Text, "THÔNG BÁO", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        
                        ChuyenBan(lstBan[ViTriBanCanChuyen], lstBan[ViTriBanCanDen]);
                        ViTriDangChon = ViTriBanCanDen;
                        lstBan[ViTriBanCanChuyen].BackColor = My_Color.Instance.BanTrong;
                        
                    }
                    lstBan[ViTriBanCanChuyen].Enabled = true;
                    btnChuyenBan.Checked = false;
                }
                //Khi chưa có button nào được Click thì indexPrevious = -1
                if (ViTriDangChon == -1)
                {
                    ViTriDangChon = mbtn.Index;
                    mbtn.Checked = true;
                }
                else
                {
                    SetColorMyButton(lstBan[ViTriDangChon]);
                    ViTriDangChon = mbtn.Index;
                    mbtn.Checked = true;
                }
                pnThongTin.Text = "Bàn: "+mbtn.Text;
                pnThongTin.Visible = true;
                CapNhatTrangThaiHoaDon(mbtn);
                SetTrangThaiButton(mbtn);
            }
            catch (Exception)
            {
            }
        }
        private void ButtonLoaiMon_Click(object sender, EventArgs e)
        {
            Button_LoaiMon btn = (Button_LoaiMon)sender;
            if (btn.Mode.Equals("Đêm"))
            {
                lstMonAn = BUS_MonAn.Instance.LayDanhSach_Mode_IDLoai(btn.Mode, btn.IDLoaiMon);
                dgvMonAn.DataSource = lstMonAn;
                dgvMonAn.Columns["ID"].Visible = false;
                dgvMonAn.Columns["IDLoai"].Visible = false;
                dgvMonAn.Columns["Mode"].Visible = false;
                dgvMonAn.Columns["GiaNgay"].Visible = false;

                dgvMonAn.Columns["Ten"].HeaderText = "Tên món ăn";
                dgvMonAn.Columns["Ten"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                dgvMonAn.Columns["GiaDem"].HeaderText = "Đơn giá";
                dgvMonAn.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            }
            else
            {
                lstMonAn = BUS_MonAn.Instance.LayDanhSach_IDLoai(btn.IDLoaiMon);
                dgvMonAn.DataSource = lstMonAn;
                dgvMonAn.Columns["ID"].Visible = false;
                dgvMonAn.Columns["IDLoai"].Visible = false;
                dgvMonAn.Columns["Mode"].Visible = false;
                dgvMonAn.Columns["GiaDem"].Visible = false;

                dgvMonAn.Columns["Ten"].HeaderText = "Tên món ăn";
                dgvMonAn.Columns["Ten"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                dgvMonAn.Columns["GiaNgay"].HeaderText = "Đơn giá";
                dgvMonAn.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            }
            foreach (Button_LoaiMon item in lstButtonLoaiMon)
                item.Checked = false;
            btn.Checked = true;
            Properties.Settings.Default.ViTriLoaiMon = btn.ViTri;
            Properties.Settings.Default.Save();
        }
        private void btnCheDo_Click(object sender, EventArgs e)
        {
            if (!DaThanhToanHet())
            {
                MessageBox.Show("Vui lòng thanh toán hết tất cả các bàn còn lại mới có thể chuyển sang chế độ khác!", "Lỗi !!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            Properties.Settings.Default.isModeDem = !Properties.Settings.Default.isModeDem;
            Properties.Settings.Default.Save();
            Properties.Settings.Default.ViTriLoaiMon = 0;
            Properties.Settings.Default.Save();
            pnButton_LoaiMon.Controls.Clear();
            lstButtonLoaiMon.Clear();
            dgvMonAn.DataSource = null;
            HienThiDanhSachLoaiThucAn(Properties.Settings.Default.isModeDem);
            ButtonLoaiMon_Click(lstButtonLoaiMon[Properties.Settings.Default.ViTriLoaiMon], new EventArgs());
        }
        private void btnChuyenBan_Click(object sender, EventArgs e)
        {
            try
            {
                if (btnChuyenBan.Checked)
                {
                    btnChuyenBan.Checked = false;
                    lstBan[ViTriBanCanChuyen].Enabled = true;
                    lstBan[ViTriBanCanChuyen].Checked = true;
                    btnChuyenBan.Text = "Chuyển Bàn";
                }
                else
                {
                    btnChuyenBan.Checked = true;
                    ViTriBanCanChuyen = ViTriDangChon;
                    lstBan[ViTriBanCanChuyen].Enabled = false;
                    lstBan[ViTriBanCanChuyen].Checked = false;
                    lstBan[ViTriBanCanChuyen].BackColor = My_Color.Instance.ChuyenBan;
                    btnChuyenBan.Text = "Chọn Bàn";

                }
            }
            catch (Exception)
            {
            }
        }
        private void btnThanhToan_Click(object sender, EventArgs e)
        {
            try
            {
                int IDBan = lstBan[ViTriDangChon].IdBan;
                int IDHoaDon = (int)BUS_HoaDon.Instance.LayIDHoaDonTheoIDBan(IDBan);
                List<DTO_HoaDon_HoaDonChiTiet_MonAn> lstHoaDonChiTiet = new List<DTO_HoaDon_HoaDonChiTiet_MonAn>();
                lstHoaDonChiTiet = BUS_HoaDonChiTiet.Instance.LayDanhSach_IDBan(IDBan);
                BUS_Database.Instance.GhiLichSu(frmDangNhap.Taikhoan.TaiKhoan, "Thanh toán hóa đơn: " + IDHoaDon.ToString() + " Thành tiền: " + lstBan[ViTriDangChon].TongTien.ToString(), DateTime.Now);
                frmMain.LoadLichSu();
                BUS_HoaDon.Instance.ThanhToan(IDHoaDon, lstBan[ViTriDangChon].TongTien, lstBan[ViTriDangChon].PhanTramGiamGia, lstBan[ViTriDangChon].TienGiam);
                lstBan[ViTriDangChon].TongTien = 0;
                lstBan[ViTriDangChon].PhanTramGiamGia = 0;
                lstBan[ViTriDangChon].TienGiam = 0;
                lstBan[ViTriDangChon].TinhTrang = "Trống";
                lstBan[ViTriDangChon].IdHoaDon = 0;
                BUS_Ban.Instance.CapNhatTrangThaiBan(lstBan[ViTriDangChon].IdBan, lstBan[ViTriDangChon].TinhTrang);
                SetTrangThaiButton(lstBan[ViTriDangChon]);
                CapNhatTrangThaiHoaDon(lstBan[ViTriDangChon]);
                
            }
            catch (Exception)
            {
            }
        }
        private void _CreateReceipt(object sender, PrintPageEventArgs e)
        {
            try
            {
                int IDBan = lstBan[ViTriDangChon].IdBan;
                int IDHoaDon = (int)BUS_HoaDon.Instance.LayIDHoaDonTheoIDBan(IDBan);
                List<DTO_HoaDon_HoaDonChiTiet_MonAn> lstHoaDonChiTiet = new List<DTO_HoaDon_HoaDonChiTiet_MonAn>();
                lstHoaDonChiTiet = BUS_HoaDonChiTiet.Instance.LayDanhSach_IDBan(IDBan);

                int pixelCharsize2 = 5;
                int padXK = 165;
                int padX = 5;
                int padY = 0;
                int size1 = 12; int size2 = 8;
                int offset2 = 15;
                int offset = 13;
                e.Graphics.DrawString("1985", new Font("Shortcut", 35, FontStyle.Bold), Brushes.Black, new Point(padX, padY));
                padY += offset2 + 15;
                e.Graphics.DrawString("Garden", new Font("Mr Lackboughs", 30, FontStyle.Italic), Brushes.Black, new Point(padX + 65, padY));
                padY += offset2 + 42;
                e.Graphics.DrawString("33/3 Trần Hưng Đạo P.Mỹ Xuyên ", new Font("Arial", size2, FontStyle.Italic), Brushes.Black, new Point(padX, padY));
                padY += offset2;
                e.Graphics.DrawString("Thành phố Long Xuyên, AG", new Font("Arial", size2, FontStyle.Italic), Brushes.Black, new Point(padX + 40, padY));
                padY += offset2;
                e.Graphics.DrawString("0763 84 1985 | 091 6783 001", new Font("Arial", size2, FontStyle.Italic), Brushes.Black, new Point(padX + 15, padY));
                padY += offset2;
                e.Graphics.DrawString("Facebook.com/cafe1985garden", new Font("Arial", size2, FontStyle.Italic), Brushes.Black, new Point(padX + 10, padY));
                padY += offset2;
                e.Graphics.DrawString("---------------------------------------------------------------------", new Font("Arial", size2, FontStyle.Bold), Brushes.Black, new Point(padX, padY));
                padY += offset2 + 5;
                e.Graphics.DrawString("PHIẾU THANH TOÁN", new Font("Arial", size1, FontStyle.Bold), Brushes.Black, new Point(padX, padY));
                padY += offset2 + 7;
                e.Graphics.DrawString(lstBan[ViTriDangChon].Text.Trim(), new Font("Arial", size2, FontStyle.Bold), Brushes.Black, new Point(padX, padY));
                padY += offset2 + 7;
                e.Graphics.DrawString("Tên món", new Font("Arial", size2, FontStyle.Italic), Brushes.Black, new Point(padX + 5, padY));
                e.Graphics.DrawString("SL", new Font("Arial", size2, FontStyle.Italic), Brushes.Black, new Point(padX + 120, padY));
                e.Graphics.DrawString("T.Tiền", new Font("Arial", size2, FontStyle.Italic), Brushes.Black, new Point(padX + 140 + 5, padY));
                padY += offset - 5;
                e.Graphics.DrawString("---------------------------------------------------------------------", new Font("Arial", size2, FontStyle.Bold), Brushes.Black, new Point(padX, padY));
                padY += offset2;
                foreach (DTO_HoaDon_HoaDonChiTiet_MonAn item in lstHoaDonChiTiet)
                {
                    e.Graphics.DrawString(item.TenMonAn, new Font("Arial", size2, FontStyle.Italic), Brushes.Black, new Point(padX, padY));
                    e.Graphics.DrawString(item.SoLuongMonAn.ToString(), new Font("Arial", size2, FontStyle.Italic), Brushes.Black, new Point(padX + 120, padY));
                    float K = 0;
                    if (Properties.Settings.Default.isModeDem)
                        K = (int.Parse(item.GiaDem.ToString()) * item.SoLuongMonAn) / 1000;
                    else
                        K = (int.Parse(item.GiaNgay.ToString()) * item.SoLuongMonAn) / 1000;
                    int lengthK = K.ToString().Length;
                    e.Graphics.DrawString(K.ToString() + "K", new Font("Arial", size2, FontStyle.Italic), Brushes.Black, new Point(padX + padXK - (lengthK * pixelCharsize2), padY));
                    padY += offset;
                }
                e.Graphics.DrawString("---------------------------------------------------------------------", new Font("Arial", size2, FontStyle.Bold), Brushes.Black, new Point(padX, padY));
                padY += offset2 + 7;
                if (lstBan[ViTriDangChon].PhanTramGiamGia != 0)
                {
                    e.Graphics.DrawString("Tổng cộng: " + FloatToString(lstBan[ViTriDangChon].TongTien) + " VNĐ", new Font("Arial", size2 - 1, FontStyle.Bold), Brushes.Black, new Point(padX, padY));
                    padY += offset2;
                    e.Graphics.DrawString("GIẢM: " + lstBan[ViTriDangChon].PhanTramGiamGia + "% (" + FloatToString(lstBan[ViTriDangChon].TienGiam) + "VNĐ)", new Font("Arial", size2 - 1, FontStyle.Bold), Brushes.Black, new Point(padX, padY));
                    padY += offset2 + 3;
                    e.Graphics.DrawString("Thành tiền: ", new Font("Arial", 8, FontStyle.Bold), Brushes.Black, new Point(padX, padY));
                    e.Graphics.DrawString(FloatToString(lstBan[ViTriDangChon].ThanhTienDaGiam), new Font("Arial", 12, FontStyle.Bold), Brushes.Black, new Point(padX + 95, padY - 5));
                    e.Graphics.DrawString("VNĐ", new Font("Arial", 7, FontStyle.Bold), Brushes.Black, new Point(padX + 160, padY));
                    padY += offset2;
                }
                else
                {
                    e.Graphics.DrawString("Thành tiền: ", new Font("Arial", 8, FontStyle.Bold), Brushes.Black, new Point(padX, padY));
                    e.Graphics.DrawString(FloatToString(lstBan[ViTriDangChon].ThanhTienDaGiam), new Font("Arial", 12, FontStyle.Bold), Brushes.Black, new Point(padX + 95, padY - 5));
                    e.Graphics.DrawString("VNĐ", new Font("Arial", 7, FontStyle.Bold), Brushes.Black, new Point(padX + 160, padY));
                    padY += offset2;
                }
                e.Graphics.DrawString("ĐVT: K = 1.000 VNĐ", new Font("Arial", size2 - 1, FontStyle.Italic), Brushes.Black, new Point(padX, padY));
                padY += offset2 + 10;
                e.Graphics.DrawString("Ngày: " + DatetimeToString_dd_MM_yyyy(DateTime.Now) + " - " + DatetimeToString_hh_mm(DateTime.Now), new Font("Arial", size2 - 1, FontStyle.Italic), Brushes.Black, new Point(padX + 20, padY));
                padY += offset2;
                e.Graphics.DrawString("Cảm ơn! Hẹn gặp lại quý khách!", new Font("Arial", size2, FontStyle.Italic), Brushes.Black, new Point(padX + 5, padY));
                padY += offset2 + 45;
                e.Graphics.DrawString(".", new Font("Arial", size2, FontStyle.Italic), Brushes.Black, new Point(padX + 15, padY));
                // Cập nhật sau khi thanh toán
                //DataProvider.Instance.ExecuteNonQuery("update HoaDon set TongTien = @tongtien , TinhTrang = 1 where ID = @id ", new object[] { TongTien, idbill });
                
            }
            catch (Exception)
            {
            }
        }
        private void btnInHoaDon_Click(object sender, EventArgs e)
        {
            try
            {
                if (Properties.Settings.Default.isXemTruocIn)
                {
                    PrintDocument _PrintDocument = new PrintDocument();
                    PrintDialog _PrintDialog = new PrintDialog();
                    _PrintDialog.Document = _PrintDocument; //add the document to the dialog box
                    PrintPreviewDialog ppd = new PrintPreviewDialog();
                    ppd.Document = _PrintDocument;
                    ppd.PrintPreviewControl.Zoom = 2.0;
                    ((Form)ppd).WindowState = FormWindowState.Maximized;
                    _PrintDocument.PrintPage += new PrintPageEventHandler(_CreateReceipt);
                    ppd.ShowDialog();
                    _PrintDocument.Print();
                }
                else
                {
                    PrintDocument _PrintDocument = new PrintDocument();
                    PrintDialog _PrintDialog = new PrintDialog();
                    _PrintDialog.Document = _PrintDocument; 
                    _PrintDocument.PrintPage += new PrintPageEventHandler(_CreateReceipt);
                    _PrintDocument.Print();
                }
            }
            catch (Exception)
            {
            }
        }
        private void dgvMonAn_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.RowIndex >= 0)
                {
                    if (ViTriDangChon == -1)
                    {
                        MessageBox.Show("Vui lòng chọn bàn trước khi gọi món!", "Lỗi gọi món", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    // nếu IDhoa đơn của bàn đang chọn = 0 =>> Bàn mới =>> Thêm hóa đơn
                    if (lstBan[ViTriDangChon].IdHoaDon == 0)
                    {
                        // Thêm hóa đơn cho bàn được chọn
                        if (!BUS_HoaDon.Instance.Insert(lstBan[ViTriDangChon].IdBan))
                            MessageBox.Show("Lỗi thêm hóa đơn!!", "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        else
                        {

                            //cập nhật IDHoaDon cho lstBan hiện tại
                            //if (BUS_HoaDon.Instance.LayIDHoaDonChuaThanhToanTheoIDBan(lstBan[ViTriDangChon].IdBan) != null)
                            lstBan[ViTriDangChon].IdHoaDon = int.Parse(BUS_HoaDon.Instance.LayIDHoaDonChuaThanhToan_IDBan(lstBan[ViTriDangChon].IdBan).ToString());
                            BUS_Database.Instance.GhiLichSu(frmDangNhap.Taikhoan.TaiKhoan, "Thêm Hóa đơn "+lstBan[ViTriDangChon].IdHoaDon+" cho bàn " + lstBan[ViTriDangChon].Text, DateTime.Now);
                            frmMain.LoadLichSu();
                        }
                    }
                    // Kiểm tra hóa đơn chi tiết
                    DataGridViewRow d = ((DataGridViewX)sender).Rows[e.RowIndex];
                    int IDFood = int.Parse(d.Cells[0].Value.ToString());
                    string TenMon = d.Cells[1].Value.ToString();
                    float DonGia = 0;
                    if (!Properties.Settings.Default.isModeDem)
                        DonGia = (float)Convert.ToDouble(d.Cells[4].Value.ToString());
                    else
                        DonGia = (float)Convert.ToDouble(d.Cells[5].Value.ToString());
                    if (BUS_HoaDonChiTiet.Instance.LayIDHoaDonChiTiet_IDHoaDon_IDMonAn(lstBan[ViTriDangChon].IdHoaDon, IDFood) == null)
                    {
                        // Nếu không tìm thấy hóa đơn chi tiết =>> Insert
                        if (!BUS_HoaDonChiTiet.Instance.Insert(lstBan[ViTriDangChon].IdHoaDon, IDFood, 1, DonGia))
                        {
                            MessageBox.Show("Lỗi thêm hóa đơn chi tiết", "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return;
                        }
                        else
                        {
                            BUS_Database.Instance.GhiLichSu(frmDangNhap.Taikhoan.TaiKhoan, "Thêm 1 "+BUS_MonAn.Instance.LayTen_ID(IDFood)+" cho bàn "+lstBan[ViTriDangChon].IdBan+" ID Hóa đơn: "+lstBan[ViTriDangChon].IdHoaDon, DateTime.Now);
                            frmMain.LoadLichSu();
                        }
                    }
                    else
                    {
                        // Nếu tìm thấy hóa đơn chi tiết =>> Update 
                        int IDHoaDonChiTiet = int.Parse(BUS_HoaDonChiTiet.Instance.LayIDHoaDonChiTiet_IDHoaDon_IDMonAn(lstBan[ViTriDangChon].IdHoaDon, IDFood).ToString());
                        if (!BUS_HoaDonChiTiet.Instance.Update(IDHoaDonChiTiet, IDFood, 1, DonGia))
                        {
                            MessageBox.Show("Lỗi cập nhật hóa đơn chi tiết", "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return;
                        }
                        else
                        {
                            BUS_Database.Instance.GhiLichSu(frmDangNhap.Taikhoan.TaiKhoan, "Thêm 1 " + BUS_MonAn.Instance.LayTen_ID(IDFood) + " cho bàn " + lstBan[ViTriDangChon].IdBan + " ID Hóa đơn: " + lstBan[ViTriDangChon].IdHoaDon, DateTime.Now);
                            frmMain.LoadLichSu();
                        }
                    }
                    //CapNhatTrangThaiBill
                    CapNhatTrangThaiHoaDon(lstBan[ViTriDangChon]);
                    lstBan[ViTriDangChon].TinhTrang = "Có Người";
                    BUS.BUS_Ban.Instance.CapNhatTrangThaiBan(lstBan[ViTriDangChon].IdBan, lstBan[ViTriDangChon].TinhTrang);
                    SetTrangThaiButton(lstBan[ViTriDangChon]);
                }
            }
            catch (Exception)
            {
                
                throw;
            }
        }
        private void lstDanhSachHoaDonChiTiet_MouseClick(object sender, MouseEventArgs e)
        {

            try
            {
                if(e.Button == System.Windows.Forms.MouseButtons.Left)//thêm
                {
                    int idfood = int.Parse(lstDanhSachHoaDonChiTiet.SelectedItems[0].Tag.ToString());
                    int DonGia = int.Parse(lstDanhSachHoaDonChiTiet.SelectedItems[0].SubItems[2].Text);
                    int idchitiet = (int)BUS_HoaDonChiTiet.Instance.LayIDHoaDonChiTiet_IDHoaDon_IDMonAn(lstBan[ViTriDangChon].IdHoaDon, idfood);
                    BUS_Database.Instance.GhiLichSu(frmDangNhap.Taikhoan.TaiKhoan, "Thêm 1 " + BUS_MonAn.Instance.LayTen_ID(idfood) + " cho bàn " + lstBan[ViTriDangChon].IdBan + " ID Hóa đơn: " + lstBan[ViTriDangChon].IdHoaDon, DateTime.Now);
                    frmMain.LoadLichSu();
                    BUS_HoaDonChiTiet.Instance.Update(idchitiet, idfood, 1, DonGia);
                    CapNhatTrangThaiHoaDon(lstBan[ViTriDangChon]);
                    SetTrangThaiButton(lstBan[ViTriDangChon]);
                }
                else // bớt
                {
                    int idfood = int.Parse(lstDanhSachHoaDonChiTiet.SelectedItems[0].Tag.ToString());
                    int DonGia = int.Parse(lstDanhSachHoaDonChiTiet.SelectedItems[0].SubItems[2].Text);
                    int idchitiet = (int)BUS_HoaDonChiTiet.Instance.LayIDHoaDonChiTiet_IDHoaDon_IDMonAn(lstBan[ViTriDangChon].IdHoaDon, idfood);
                    BUS_HoaDonChiTiet.Instance.Update(idchitiet, idfood, -1, DonGia);
                    BUS_Database.Instance.GhiLichSu(frmDangNhap.Taikhoan.TaiKhoan, "Bớt 1 " + BUS_MonAn.Instance.LayTen_ID(idfood) + " của bàn " + lstBan[ViTriDangChon].IdBan + " ID Hóa đơn: " + lstBan[ViTriDangChon].IdHoaDon, DateTime.Now);
                    frmMain.LoadLichSu();
                    if (BUS_HoaDonChiTiet.Instance.DemSoDong(lstBan[ViTriDangChon].IdHoaDon) == 0)
                    {
                        BUS_HoaDon.Instance.Delete(lstBan[ViTriDangChon].IdHoaDon);
                        BUS_Database.Instance.GhiLichSu(frmDangNhap.Taikhoan.TaiKhoan, "Xóa hóa đơn " + lstBan[ViTriDangChon].IdHoaDon + " của bàn " + lstBan[ViTriDangChon].Text, DateTime.Now);
                        frmMain.LoadLichSu();
                        lstBan[ViTriDangChon].TongTien = 0;
                        lstBan[ViTriDangChon].PhanTramGiamGia = 0;
                        lstBan[ViTriDangChon].TienGiam = 0;
                        lstBan[ViTriDangChon].TinhTrang = "Trống";
                        lstBan[ViTriDangChon].IdHoaDon = 0;
                        BUS_Ban.Instance.CapNhatTrangThaiBan(lstBan[ViTriDangChon].IdBan, lstBan[ViTriDangChon].TinhTrang);
                    }
                    CapNhatTrangThaiHoaDon(lstBan[ViTriDangChon]);
                    SetTrangThaiButton(lstBan[ViTriDangChon]);
                }
            }
            catch (Exception)
            {
                
            }

        }
        private void btnGiamGia_MouseDown(object sender, MouseEventArgs e)
        {
            try
            {
                if(e.Button == System.Windows.Forms.MouseButtons.Left)
                {
                    if (lstBan[ViTriDangChon].PhanTramGiamGia <=95 )
                    {
                        lstBan[ViTriDangChon].PhanTramGiamGia += 5;
                        BUS_HoaDon.Instance.CapNhatGiamGia(lstBan[ViTriDangChon].IdHoaDon, lstBan[ViTriDangChon].PhanTramGiamGia);
                        SetTrangThaiButton(lstBan[ViTriDangChon]);
                    }
                }
                else
                {
                    if (lstBan[ViTriDangChon].PhanTramGiamGia >= 5)
                    {
                        lstBan[ViTriDangChon].PhanTramGiamGia -= 5;
                        BUS_HoaDon.Instance.CapNhatGiamGia(lstBan[ViTriDangChon].IdHoaDon, lstBan[ViTriDangChon].PhanTramGiamGia);
                        SetTrangThaiButton(lstBan[ViTriDangChon]);
                    }
                }

            }
            catch (Exception)
            {
            }
        }
        #endregion
    }
}
