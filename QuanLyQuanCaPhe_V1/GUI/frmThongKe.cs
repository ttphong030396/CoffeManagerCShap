﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BUS;
using DTO;
using Microsoft.Reporting.WinForms;
using DevComponents.DotNetBar;
namespace GUI
{
    public partial class frmThongKe : Form
    {
        public frmThongKe()
        {
            InitializeComponent();
        }
        private void btnHomNay_Click(object sender, EventArgs e)
        {
            try
            {
                ((ButtonX)sender).Checked = true;
                btnThangNay.Checked = false;
                btnTuyChon.Checked = false;
                dtpNgayDau.Value = DateTime.Now;
                lbTuyChon.Visible = false;
                dtpTuyChon.Visible = false;
                pnChon.Visible = true;
            }
            catch (Exception)
            {

            }
        }
        private void dtpNgayDau_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (btnHomNay.Checked)
                {
                    DTO_ReportHoaDonBindingSource.DataSource = BUS_ReportHoaDon.Instance.Report(dtpNgayDau.Value.ToShortDateString());
                    DTO_ReportHoaDonChiTietBindingSource.DataSource = BUS_ReportHoaDonChiTiet.Instance.Report(dtpNgayDau.Value.ToShortDateString());

                    ReportParameter[] para = new ReportParameter[2];
                    para[0] = new ReportParameter("TieuDe");
                    para[0].Values.Add("Thống kê Ngày " + dtpNgayDau.Value.Day + " Tháng " + dtpNgayDau.Value.Month + " Năm " + dtpNgayDau.Value.Year);
                    para[1] = new ReportParameter("NgayLap");
                    string ngaylap = "Thời gian lập thống kê: " + DateTime.Now.Hour.ToString() + " Giờ " + DateTime.Now.Minute.ToString() + " Phút - Ngày " + DateTime.Now.Day.ToString() + " Tháng " + DateTime.Now.Month.ToString() + " Năm " + DateTime.Now.Year.ToString();
                    para[1].Values.Add(ngaylap);
                    rpHoaDon.LocalReport.SetParameters(para);
                }
                if (btnThangNay.Checked)
                {
                    DTO_ReportHoaDonBindingSource.DataSource = BUS_ReportHoaDon.Instance.ReportMonth(dtpNgayDau.Value.ToShortDateString());
                    DTO_ReportHoaDonChiTietBindingSource.DataSource = BUS_ReportHoaDonChiTiet.Instance.ReportMonth(dtpNgayDau.Value.ToShortDateString());

                    ReportParameter[] para = new ReportParameter[2];
                    para[0] = new ReportParameter("TieuDe");
                    para[0].Values.Add("Thống kê Tháng " + dtpNgayDau.Value.Month + " Năm " + dtpNgayDau.Value.Year);
                    para[1] = new ReportParameter("NgayLap");
                    string ngaylap = "Thời gian lập thống kê: " + DateTime.Now.Hour.ToString() + " Giờ " + DateTime.Now.Minute.ToString() + " Phút - Ngày " + DateTime.Now.Day.ToString() + " Tháng " + DateTime.Now.Month.ToString() + " Năm " + DateTime.Now.Year.ToString();
                    para[1].Values.Add(ngaylap);
                    rpHoaDon.LocalReport.SetParameters(para);
                }
                if (btnTuyChon.Checked)
                {
                    DTO_ReportHoaDonBindingSource.DataSource = BUS_ReportHoaDon.Instance.ReportCustom(dtpNgayDau.Value.ToShortDateString(), dtpTuyChon.Value.ToShortDateString());
                    DTO_ReportHoaDonChiTietBindingSource.DataSource = BUS_ReportHoaDonChiTiet.Instance.ReportCustom(dtpNgayDau.Value.ToShortDateString(), dtpTuyChon.Value.ToShortDateString());

                    ReportParameter[] para = new ReportParameter[2];
                    para[0] = new ReportParameter("TieuDe");
                    para[0].Values.Add("Thống kê từ " + dtpNgayDau.Value.Day + "/" + dtpNgayDau.Value.Month + "/" + dtpNgayDau.Value.Year + " Đến " + dtpTuyChon.Value.Day + "/" + dtpTuyChon.Value.Month + "/" + dtpTuyChon.Value.Year);
                    para[1] = new ReportParameter("NgayLap");
                    string ngaylap = "Thời gian lập thống kê: " + DateTime.Now.Hour.ToString() + " Giờ " + DateTime.Now.Minute.ToString() + " Phút - Ngày " + DateTime.Now.Day.ToString() + " Tháng " + DateTime.Now.Month.ToString() + " Năm " + DateTime.Now.Year.ToString();
                    para[1].Values.Add(ngaylap);
                    rpHoaDon.LocalReport.SetParameters(para);
                    
                }
                this.rpHoaDon.RefreshReport();
            }
            catch (Exception)
            {
            }
        }
        private void btnThangNay_Click(object sender, EventArgs e)
        {
            try
            {
                ((ButtonX)sender).Checked = true;
                dtpNgayDau.Value = DateTime.Now;
                btnHomNay.Checked = false;
                btnTuyChon.Checked = false;
                pnChon.Visible = false;
            }
            catch (Exception)
            {

            }
        }
        private void btnTuyChon_Click(object sender, EventArgs e)
        {
            try
            {
                ((ButtonX)sender).Checked = true;
                btnHomNay.Checked = false;
                btnThangNay.Checked = false;
                pnChon.Visible = true;
                dtpNgayDau.Value = DateTime.Now;
                dtpTuyChon.Value = DateTime.Now;
                lbTuyChon.Visible = true;
                dtpTuyChon.Visible = true;
            }
            catch (Exception)
            {

            }
        }
        private void dtpTuyChon_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (btnTuyChon.Checked)
                {
                    DTO_ReportHoaDonBindingSource.DataSource = BUS_ReportHoaDon.Instance.ReportCustom(dtpNgayDau.Value.ToShortDateString(), dtpTuyChon.Value.ToShortDateString());
                    DTO_ReportHoaDonChiTietBindingSource.DataSource = BUS_ReportHoaDonChiTiet.Instance.ReportCustom(dtpNgayDau.Value.ToShortDateString(), dtpTuyChon.Value.ToShortDateString());
                    ReportParameter[] para = new ReportParameter[2];
                    para[0] = new ReportParameter("TieuDe");
                    para[0].Values.Add("Thống kê từ " + dtpNgayDau.Value.Day + "/" + dtpNgayDau.Value.Month + "/" + dtpNgayDau.Value.Year + " Đến " + dtpTuyChon.Value.Day + "/" + dtpTuyChon.Value.Month + "/" + dtpTuyChon.Value.Year);
                    para[1] = new ReportParameter("NgayLap");
                    string ngaylap = "Thời gian lập thống kê: " + DateTime.Now.Hour.ToString() + " Giờ " + DateTime.Now.Minute.ToString() + " Phút - Ngày " + DateTime.Now.Day.ToString() + " Tháng " + DateTime.Now.Month.ToString() + " Năm " + DateTime.Now.Year.ToString();
                    para[1].Values.Add(ngaylap);
                    rpHoaDon.LocalReport.SetParameters(para);
                }
                this.rpHoaDon.RefreshReport();
            }
            catch (Exception)
            {
            }
        }

        private void frmThongKe_Load(object sender, EventArgs e)
        {
            btnHomNay_Click(btnHomNay, new EventArgs());
            this.rpHoaDon.RefreshReport();
        }

        
    }
}
