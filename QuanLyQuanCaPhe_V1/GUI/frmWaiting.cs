﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GUI
{
    public partial class frmWaiting : DevComponents.DotNetBar.Office2007RibbonForm
    {
        int i = 0;
        int loop = 0;
        bool isFlashForm;
        public frmWaiting(bool isFlashForm = false)
        {
            InitializeComponent();
            timer1.Enabled = true;
            pro.Value = i;
            this.isFlashForm = isFlashForm;
        }
        private void timer1_Tick(object sender, EventArgs e)
        {
            // không phải flashform
            if(!isFlashForm)
            {
                if (i < 100)
                    i += 10;
                else
                    i = 1;
                pro.Value = i;
            }
            else
            {
                if (i < 100)
                    i += 10;
                else
                    i = 1;
                pro.Value = i;
                loop++;
                if (loop == 45)
                    this.Close();
            }
        }

    }
}
