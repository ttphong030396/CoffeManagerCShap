﻿using BUS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GUI
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            //Properties.Settings.Default.StrConnection = "";
            //Properties.Settings.Default.Save();
            //Properties.Settings.Default.ViTriLoaiMon = 0;
            //Properties.Settings.Default.Save();
            if (Connection_BUS.Instance.TestConnection(Properties.Settings.Default.StrConnection))
                Application.Run(new frmDangNhap());
            else
                Application.Run(new frmCauHinh());
        }
    }
}
