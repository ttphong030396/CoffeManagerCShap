﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DTO;
using BUS;
using DevComponents.DotNetBar.Controls;
namespace GUI
{
    public partial class frmQuanLyTaiKhoan : DevComponents.DotNetBar.Office2007RibbonForm
    {
        ErrorProvider err = new ErrorProvider();
        public frmQuanLyTaiKhoan()
        {
            InitializeComponent();
            cbbLoaiTaiKhoan.DataSource = BUS_LoaiTaiKhoan.Instance.Get();
            cbbLoaiTaiKhoan.DisplayMember = "Ten";
            cbbLoaiTaiKhoan.ValueMember = "ID";
            dgvTaiKhoan.DataSource = BUS_TaiKhoan.Instance.GetAll();
            dgvTaiKhoan.Columns[0].HeaderText = "Tài Khoản";
            dgvTaiKhoan.Columns[1].HeaderText = "Mật Khẩu";
            dgvTaiKhoan.Columns[2].HeaderText = "Tên Hiển Thị";
            dgvTaiKhoan.Columns[3].HeaderText = "Loại";
        }
        

        private void dgvTaiKhoan_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                DataGridViewRow row  = ((DataGridViewX)sender).CurrentRow;
                txtTaiKhoan.Text = row.Cells[0].Value.ToString().Trim();
                txtMatKhau.Text = row.Cells[1].Value.ToString().Trim();
                txtTenHienThi.Text = row.Cells[2].Value.ToString().Trim();
                cbbLoaiTaiKhoan.SelectedValue = row.Cells[3].Value;
            }
            catch (Exception)
            {
                
                throw;
            }
        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            if(txtMatKhau.Text.Trim()=="")
            {
                err.SetError(txtTaiKhoan, "TÀI KHOẢN KHÔNG ĐƯỢC BỎ TRỐNG");
                txtTaiKhoan.Focus();
                return;
            }
            if(txtMatKhau.Text.Trim()=="")
            {
                err.SetError(txtMatKhau, "MẬT KHẨU KHÔNG ĐƯỢC BỎ TRỐNG");
                txtMatKhau.Focus();
                return;
            }
            DTO_TaiKhoan tk = new DTO_TaiKhoan();
            tk.TaiKhoan = txtTaiKhoan.Text.Trim();
            tk.MatKhau = txtMatKhau.Text.Trim();
            tk.TenHienThi = txtTenHienThi.Text.Trim();
            tk.Loai = int.Parse(cbbLoaiTaiKhoan.SelectedValue.ToString().Trim());
            if(!BUS_TaiKhoan.Instance.Insert(tk))
            {
                MessageBox.Show("Tài khoản đã tồn tại", "Lỗi thêm tài khoản", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                dgvTaiKhoan.DataSource = BUS_TaiKhoan.Instance.GetAll();
                BUS_Database.Instance.GhiLichSu(frmDangNhap.Taikhoan.TaiKhoan, "Thêm tài khoản " + tk.TaiKhoan + " Loai: " + BUS_LoaiTaiKhoan.Instance.LayTen_ID(tk.Loai), DateTime.Now);
                frmMain.LoadLichSu();
                dgvTaiKhoan.Columns[0].HeaderText = "Tài Khoản";
                dgvTaiKhoan.Columns[1].HeaderText = "Mật Khẩu";
                dgvTaiKhoan.Columns[2].HeaderText = "Tên Hiển Thị";
                dgvTaiKhoan.Columns[3].HeaderText = "Loại";
            }

        }
        private void btnXoa_Click(object sender, EventArgs e)
        {
            if(int.Parse(cbbLoaiTaiKhoan.SelectedValue.ToString())==1)
            {
                if(MessageBox.Show("Đây là tài khoản Administrator bạn có chắc chắn muốn xóa??\nHãy chắc rằng còn 1 tài khoản Administrator khác để có thể quản lý phần mềm","Thông báo",MessageBoxButtons.YesNo,MessageBoxIcon.Question)==DialogResult.Yes)
                {
                    if (BUS_TaiKhoan.Instance.Delete(dgvTaiKhoan.CurrentRow.Cells[0].Value.ToString().Trim()))
                    {
                        dgvTaiKhoan.DataSource = BUS_TaiKhoan.Instance.GetAll();
                        BUS_Database.Instance.GhiLichSu(frmDangNhap.Taikhoan.TaiKhoan, "Xóa tài khoản " + dgvTaiKhoan.CurrentRow.Cells[0].Value.ToString().Trim() + " Loai: " + BUS_LoaiTaiKhoan.Instance.LayTen_ID(int.Parse(dgvTaiKhoan.CurrentRow.Cells[2].Value.ToString().Trim())), DateTime.Now);
                        frmMain.LoadLichSu();
                        dgvTaiKhoan.Columns[0].HeaderText = "Tài Khoản";
                        dgvTaiKhoan.Columns[1].HeaderText = "Mật Khẩu";
                        dgvTaiKhoan.Columns[2].HeaderText = "Tên Hiển Thị";
                        dgvTaiKhoan.Columns[3].HeaderText = "Loại";
                    }
                    else
                        return;
                }
            }
            else
            {
                if (BUS_TaiKhoan.Instance.Delete(dgvTaiKhoan.CurrentRow.Cells[0].Value.ToString()))
                {
                    BUS_Database.Instance.GhiLichSu(frmDangNhap.Taikhoan.TaiKhoan, "Đã xóa tài khoản " + dgvTaiKhoan.CurrentRow.Cells[0].Value.ToString(), DateTime.Now);
                    frmMain.LoadLichSu();
                    dgvTaiKhoan.DataSource = BUS_TaiKhoan.Instance.GetAll();
                    dgvTaiKhoan.Columns[0].HeaderText = "Tài Khoản";
                    dgvTaiKhoan.Columns[1].HeaderText = "Mật Khẩu";
                    dgvTaiKhoan.Columns[2].HeaderText = "Tên Hiển Thị";
                    dgvTaiKhoan.Columns[3].HeaderText = "Loại";
                }
            }

        }
        private void btnCapNhat_Click(object sender, EventArgs e)
        {
            DTO_TaiKhoan tk = new DTO_TaiKhoan();
            tk.TaiKhoan = txtTaiKhoan.Text.Trim();
            tk.MatKhau = txtMatKhau.Text.Trim();
            tk.TenHienThi = txtTenHienThi.Text.Trim();
            tk.Loai = int.Parse(cbbLoaiTaiKhoan.SelectedValue.ToString().Trim());
            if(BUS_TaiKhoan.Instance.Update(tk))
            {
                string tmp = "Cập nhật Tài khoản: " + dgvTaiKhoan.CurrentRow.Cells[0].Value.ToString().Trim() + " Mật khẩu: " + dgvTaiKhoan.CurrentRow.Cells[0].Value.ToString().Trim() + " Tên hiển thị: " + dgvTaiKhoan.CurrentRow.Cells[2].Value.ToString().Trim() + " Loại: " + BUS_LoaiTaiKhoan.Instance.LayTen_ID((int)dgvTaiKhoan.CurrentRow.Cells[2].Value);
                string tmp1 = " Thành Tài khoản" + txtTaiKhoan.Text + " Mật khẩu: " + txtMatKhau + "Tên hiển thị: " + txtTenHienThi.Text + " Loại: " + cbbLoaiTaiKhoan.Text;
                BUS_Database.Instance.GhiLichSu(frmDangNhap.Taikhoan.TaiKhoan, tmp + tmp1, DateTime.Now);
                frmMain.LoadLichSu();
                dgvTaiKhoan.DataSource = BUS_TaiKhoan.Instance.GetAll();
                dgvTaiKhoan.Columns[0].HeaderText = "Tài Khoản";
                dgvTaiKhoan.Columns[1].HeaderText = "Mật Khẩu";
                dgvTaiKhoan.Columns[2].HeaderText = "Tên Hiển Thị";
                dgvTaiKhoan.Columns[3].HeaderText = "Loại";
            }
        }

        private void btnThoat_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
