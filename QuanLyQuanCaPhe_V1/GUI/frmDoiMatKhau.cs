﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DTO;
using BUS;
namespace GUI
{
    public partial class frmDoiMatKhau : DevComponents.DotNetBar.Office2007RibbonForm
    {
        ErrorProvider err = new ErrorProvider();
        public frmDoiMatKhau()
        {
            InitializeComponent();
        }
        private void btnDoiMatKhau_Click(object sender, EventArgs e)
        {
            if(txtMatKhuaCu.Text.Trim()=="")
            {
                err.SetError(txtMatKhuaCu, "CHƯA NHẬP MẬT KHẨU");
                txtMatKhuaCu.Focus();
                return;
            }
            if (txtMatKhauMoi.Text.Trim() == "")
            {
                err.SetError(txtMatKhauMoi, "CHƯA NHẬP MẬT KHẨU MỚI");
                txtMatKhauMoi.Focus();
                return;
            }
            if (txtNhapLaiMatKhau.Text.Trim() == "")
            {
                err.SetError(txtNhapLaiMatKhau, "CHƯA NHẬP LẠI MẬT KHẨU");
                txtNhapLaiMatKhau.Focus();
                return;
            }
            if (BUS_TaiKhoan.Instance.Get(frmDangNhap.Taikhoan.TaiKhoan, frmDangNhap.Taikhoan.MatKhau) == null)
            {
                err.SetError(txtMatKhuaCu, "MẬT KHẨU KHÔNG ĐÚNG");
                txtMatKhuaCu.Focus();
                return;
            }
            if (txtNhapLaiMatKhau.Text.Trim() != txtMatKhauMoi.Text.Trim())
            {
                err.SetError(txtNhapLaiMatKhau, "NHẬP LẠI MẬT KHẨU KHÔNG ĐÚNG");
                txtNhapLaiMatKhau.Focus();
                return;
            }
            
            frmDangNhap.Taikhoan.MatKhau = txtNhapLaiMatKhau.Text.Trim();
            if (BUS_TaiKhoan.Instance.Update(frmDangNhap.Taikhoan))
            {
                MessageBox.Show("Đổi mật khẩu thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                BUS_Database.Instance.GhiLichSu(frmDangNhap.Taikhoan.TaiKhoan, "Đổi mật khẩu", DateTime.Now);
                frmMain.LoadLichSu();
                this.Close();
            }
            else
            {
                MessageBox.Show("Không đổi được mật khẩu", "Lỗi đổi mật khẩu", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            }
        }

        private void btnThoat_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}          