﻿using DevComponents.DotNetBar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GUI
{
    public class Button_Ban : ButtonX
    {
        public int Index { get; set; }
        public int IdKhu { get; set; }
        public int IdBan { get; set; }
        public string TenKhu { get; set; }
        public float TongTien { get; set; }
        public int PhanTramGiamGia { get; set; }
        public float TienGiam { get; set; }
        public float ThanhTienDaGiam { get; set; }
        public int IdHoaDon { get; set; }
        public string TinhTrang { get; set; }
    }
}
