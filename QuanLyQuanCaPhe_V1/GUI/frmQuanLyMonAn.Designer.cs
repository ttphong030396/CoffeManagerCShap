﻿namespace GUI
{
    partial class frmQuanLyMonAn
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            this.pnLoai = new DevComponents.DotNetBar.PanelEx();
            this.grThongTinMonAn = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.btnShow = new DevComponents.DotNetBar.ButtonX();
            this.labelX5 = new DevComponents.DotNetBar.LabelX();
            this.txtGiaDem = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.txtGiaNgay = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.rdDem = new System.Windows.Forms.RadioButton();
            this.rdNgay = new System.Windows.Forms.RadioButton();
            this.labelX8 = new DevComponents.DotNetBar.LabelX();
            this.lbGiaDem = new DevComponents.DotNetBar.LabelX();
            this.btnCapNhatMon = new DevComponents.DotNetBar.ButtonX();
            this.btnXoaMon = new DevComponents.DotNetBar.ButtonX();
            this.txtIDMon = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.btnThemMon = new DevComponents.DotNetBar.ButtonX();
            this.cbbLoaiMon = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.txtTenMon = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.txtTenLoai = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.txtIDLoai = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.lb = new DevComponents.DotNetBar.LabelX();
            this.labelX6 = new DevComponents.DotNetBar.LabelX();
            this.btnCapNhatLoaiMon = new DevComponents.DotNetBar.ButtonX();
            this.btnXoaLoaiMon = new DevComponents.DotNetBar.ButtonX();
            this.btnThemLoaiMon = new DevComponents.DotNetBar.ButtonX();
            this.dgvMonAn = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.labelX4 = new DevComponents.DotNetBar.LabelX();
            this.nrudTrang = new System.Windows.Forms.NumericUpDown();
            this.btnDau = new DevComponents.DotNetBar.ButtonX();
            this.btnCuoi = new DevComponents.DotNetBar.ButtonX();
            this.btnSau = new DevComponents.DotNetBar.ButtonX();
            this.pnFooter = new DevComponents.DotNetBar.PanelEx();
            this.btnTruoc = new DevComponents.DotNetBar.ButtonX();
            this.ckNhom = new DevComponents.DotNetBar.Controls.CheckBoxX();
            this.cbbNhomLoaiMon = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.panelEx3 = new DevComponents.DotNetBar.PanelEx();
            this.grThongTinLoaiMon = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.panelEx4 = new DevComponents.DotNetBar.PanelEx();
            this.dgvLoaiMon = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.panelEx1 = new DevComponents.DotNetBar.PanelEx();
            this.pnBackground = new DevComponents.DotNetBar.PanelEx();
            this.pnLoai.SuspendLayout();
            this.grThongTinMonAn.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMonAn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nrudTrang)).BeginInit();
            this.pnFooter.SuspendLayout();
            this.panelEx3.SuspendLayout();
            this.grThongTinLoaiMon.SuspendLayout();
            this.panelEx4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvLoaiMon)).BeginInit();
            this.panelEx1.SuspendLayout();
            this.pnBackground.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnLoai
            // 
            this.pnLoai.CanvasColor = System.Drawing.SystemColors.Control;
            this.pnLoai.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.pnLoai.Controls.Add(this.btnCapNhatLoaiMon);
            this.pnLoai.Controls.Add(this.txtTenLoai);
            this.pnLoai.Controls.Add(this.btnXoaLoaiMon);
            this.pnLoai.Controls.Add(this.labelX6);
            this.pnLoai.Controls.Add(this.btnThemLoaiMon);
            this.pnLoai.Controls.Add(this.txtIDLoai);
            this.pnLoai.Controls.Add(this.lb);
            this.pnLoai.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnLoai.Location = new System.Drawing.Point(0, 296);
            this.pnLoai.Name = "pnLoai";
            this.pnLoai.Size = new System.Drawing.Size(621, 47);
            this.pnLoai.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.pnLoai.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.pnLoai.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.pnLoai.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.pnLoai.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.pnLoai.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.pnLoai.Style.GradientAngle = 90;
            this.pnLoai.TabIndex = 0;
            // 
            // grThongTinMonAn
            // 
            this.grThongTinMonAn.CanvasColor = System.Drawing.SystemColors.Control;
            this.grThongTinMonAn.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.grThongTinMonAn.Controls.Add(this.btnShow);
            this.grThongTinMonAn.Controls.Add(this.labelX5);
            this.grThongTinMonAn.Controls.Add(this.txtGiaDem);
            this.grThongTinMonAn.Controls.Add(this.txtGiaNgay);
            this.grThongTinMonAn.Controls.Add(this.rdDem);
            this.grThongTinMonAn.Controls.Add(this.rdNgay);
            this.grThongTinMonAn.Controls.Add(this.labelX8);
            this.grThongTinMonAn.Controls.Add(this.lbGiaDem);
            this.grThongTinMonAn.Controls.Add(this.btnCapNhatMon);
            this.grThongTinMonAn.Controls.Add(this.btnXoaMon);
            this.grThongTinMonAn.Controls.Add(this.txtIDMon);
            this.grThongTinMonAn.Controls.Add(this.btnThemMon);
            this.grThongTinMonAn.Controls.Add(this.cbbLoaiMon);
            this.grThongTinMonAn.Controls.Add(this.txtTenMon);
            this.grThongTinMonAn.Controls.Add(this.labelX3);
            this.grThongTinMonAn.Controls.Add(this.labelX1);
            this.grThongTinMonAn.Controls.Add(this.labelX2);
            this.grThongTinMonAn.Dock = System.Windows.Forms.DockStyle.Top;
            this.grThongTinMonAn.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grThongTinMonAn.Location = new System.Drawing.Point(727, 0);
            this.grThongTinMonAn.Name = "grThongTinMonAn";
            this.grThongTinMonAn.Size = new System.Drawing.Size(627, 150);
            // 
            // 
            // 
            this.grThongTinMonAn.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.grThongTinMonAn.Style.BackColorGradientAngle = 90;
            this.grThongTinMonAn.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.grThongTinMonAn.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grThongTinMonAn.Style.BorderBottomWidth = 1;
            this.grThongTinMonAn.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.grThongTinMonAn.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grThongTinMonAn.Style.BorderLeftWidth = 1;
            this.grThongTinMonAn.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grThongTinMonAn.Style.BorderRightWidth = 1;
            this.grThongTinMonAn.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grThongTinMonAn.Style.BorderTopWidth = 1;
            this.grThongTinMonAn.Style.Class = "";
            this.grThongTinMonAn.Style.CornerDiameter = 4;
            this.grThongTinMonAn.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.grThongTinMonAn.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.grThongTinMonAn.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.grThongTinMonAn.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.grThongTinMonAn.StyleMouseDown.Class = "";
            this.grThongTinMonAn.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.grThongTinMonAn.StyleMouseOver.Class = "";
            this.grThongTinMonAn.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.grThongTinMonAn.TabIndex = 10;
            this.grThongTinMonAn.Text = "THÔNG TIN MÓN";
            // 
            // btnShow
            // 
            this.btnShow.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnShow.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnShow.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.btnShow.Location = new System.Drawing.Point(227, 76);
            this.btnShow.Name = "btnShow";
            this.btnShow.Size = new System.Drawing.Size(46, 26);
            this.btnShow.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnShow.TabIndex = 19;
            this.btnShow.Text = "=";
            this.btnShow.Click += new System.EventHandler(this.btnShow_Click);
            // 
            // labelX5
            // 
            // 
            // 
            // 
            this.labelX5.BackgroundStyle.Class = "";
            this.labelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX5.Location = new System.Drawing.Point(279, 12);
            this.labelX5.Name = "labelX5";
            this.labelX5.Size = new System.Drawing.Size(60, 23);
            this.labelX5.TabIndex = 18;
            this.labelX5.Text = "Mode";
            // 
            // txtGiaDem
            // 
            // 
            // 
            // 
            this.txtGiaDem.Border.Class = "TextBoxBorder";
            this.txtGiaDem.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtGiaDem.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGiaDem.Location = new System.Drawing.Point(358, 79);
            this.txtGiaDem.Name = "txtGiaDem";
            this.txtGiaDem.Size = new System.Drawing.Size(144, 26);
            this.txtGiaDem.TabIndex = 6;
            this.txtGiaDem.TextChanged += new System.EventHandler(this.txtGiaDem_TextChanged);
            this.txtGiaDem.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtGiaDem_KeyPress);
            // 
            // txtGiaNgay
            // 
            // 
            // 
            // 
            this.txtGiaNgay.Border.Class = "TextBoxBorder";
            this.txtGiaNgay.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtGiaNgay.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGiaNgay.Location = new System.Drawing.Point(358, 44);
            this.txtGiaNgay.Name = "txtGiaNgay";
            this.txtGiaNgay.Size = new System.Drawing.Size(144, 26);
            this.txtGiaNgay.TabIndex = 5;
            this.txtGiaNgay.TextChanged += new System.EventHandler(this.txtGiaNgay_TextChanged);
            this.txtGiaNgay.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtGiaNgay_KeyPress);
            // 
            // rdDem
            // 
            this.rdDem.AutoSize = true;
            this.rdDem.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdDem.Location = new System.Drawing.Point(415, 12);
            this.rdDem.Name = "rdDem";
            this.rdDem.Size = new System.Drawing.Size(61, 24);
            this.rdDem.TabIndex = 4;
            this.rdDem.Text = "Đêm";
            this.rdDem.UseVisualStyleBackColor = true;
            this.rdDem.CheckedChanged += new System.EventHandler(this.rdDem_CheckedChanged);
            // 
            // rdNgay
            // 
            this.rdNgay.AutoSize = true;
            this.rdNgay.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdNgay.Location = new System.Drawing.Point(345, 12);
            this.rdNgay.Name = "rdNgay";
            this.rdNgay.Size = new System.Drawing.Size(63, 24);
            this.rdNgay.TabIndex = 3;
            this.rdNgay.Text = "Ngày";
            this.rdNgay.UseVisualStyleBackColor = true;
            this.rdNgay.CheckedChanged += new System.EventHandler(this.rdNgay_CheckedChanged);
            // 
            // labelX8
            // 
            // 
            // 
            // 
            this.labelX8.BackgroundStyle.Class = "";
            this.labelX8.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX8.Location = new System.Drawing.Point(279, 44);
            this.labelX8.Name = "labelX8";
            this.labelX8.Size = new System.Drawing.Size(77, 23);
            this.labelX8.TabIndex = 13;
            this.labelX8.Text = "Giá Ngày";
            // 
            // lbGiaDem
            // 
            // 
            // 
            // 
            this.lbGiaDem.BackgroundStyle.Class = "";
            this.lbGiaDem.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lbGiaDem.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbGiaDem.Location = new System.Drawing.Point(279, 79);
            this.lbGiaDem.Name = "lbGiaDem";
            this.lbGiaDem.Size = new System.Drawing.Size(65, 23);
            this.lbGiaDem.TabIndex = 12;
            this.lbGiaDem.Text = "Giá Đêm";
            // 
            // btnCapNhatMon
            // 
            this.btnCapNhatMon.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnCapNhatMon.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnCapNhatMon.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCapNhatMon.Location = new System.Drawing.Point(521, 76);
            this.btnCapNhatMon.Name = "btnCapNhatMon";
            this.btnCapNhatMon.Size = new System.Drawing.Size(91, 29);
            this.btnCapNhatMon.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnCapNhatMon.TabIndex = 9;
            this.btnCapNhatMon.Text = "Cập nhật";
            this.btnCapNhatMon.Click += new System.EventHandler(this.btnCapNhatMon_Click);
            // 
            // btnXoaMon
            // 
            this.btnXoaMon.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnXoaMon.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnXoaMon.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXoaMon.Location = new System.Drawing.Point(521, 44);
            this.btnXoaMon.Name = "btnXoaMon";
            this.btnXoaMon.Size = new System.Drawing.Size(91, 26);
            this.btnXoaMon.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnXoaMon.TabIndex = 8;
            this.btnXoaMon.Text = "Xóa";
            this.btnXoaMon.Click += new System.EventHandler(this.btnXoaMon_Click);
            // 
            // txtIDMon
            // 
            // 
            // 
            // 
            this.txtIDMon.Border.Class = "TextBoxBorder";
            this.txtIDMon.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtIDMon.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtIDMon.Location = new System.Drawing.Point(78, 12);
            this.txtIDMon.Name = "txtIDMon";
            this.txtIDMon.ReadOnly = true;
            this.txtIDMon.Size = new System.Drawing.Size(195, 26);
            this.txtIDMon.TabIndex = 0;
            this.txtIDMon.TextChanged += new System.EventHandler(this.txtIDMon_TextChanged);
            // 
            // btnThemMon
            // 
            this.btnThemMon.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnThemMon.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnThemMon.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnThemMon.Location = new System.Drawing.Point(521, 12);
            this.btnThemMon.Name = "btnThemMon";
            this.btnThemMon.Size = new System.Drawing.Size(91, 26);
            this.btnThemMon.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnThemMon.TabIndex = 7;
            this.btnThemMon.Text = "Thêm";
            this.btnThemMon.Click += new System.EventHandler(this.btnThemMon_Click);
            // 
            // cbbLoaiMon
            // 
            this.cbbLoaiMon.DisplayMember = "Text";
            this.cbbLoaiMon.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cbbLoaiMon.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbbLoaiMon.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbLoaiMon.FormattingEnabled = true;
            this.cbbLoaiMon.ItemHeight = 20;
            this.cbbLoaiMon.Location = new System.Drawing.Point(78, 76);
            this.cbbLoaiMon.Name = "cbbLoaiMon";
            this.cbbLoaiMon.Size = new System.Drawing.Size(143, 26);
            this.cbbLoaiMon.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cbbLoaiMon.TabIndex = 2;
            // 
            // txtTenMon
            // 
            // 
            // 
            // 
            this.txtTenMon.Border.Class = "TextBoxBorder";
            this.txtTenMon.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtTenMon.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenMon.Location = new System.Drawing.Point(78, 44);
            this.txtTenMon.Name = "txtTenMon";
            this.txtTenMon.Size = new System.Drawing.Size(195, 26);
            this.txtTenMon.TabIndex = 1;
            this.txtTenMon.TextChanged += new System.EventHandler(this.txtTenMon_TextChanged);
            // 
            // labelX3
            // 
            // 
            // 
            // 
            this.labelX3.BackgroundStyle.Class = "";
            this.labelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX3.Location = new System.Drawing.Point(9, 44);
            this.labelX3.Name = "labelX3";
            this.labelX3.Size = new System.Drawing.Size(75, 23);
            this.labelX3.TabIndex = 6;
            this.labelX3.Text = "Tên Món";
            // 
            // labelX1
            // 
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.Class = "";
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX1.Location = new System.Drawing.Point(6, 12);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(54, 23);
            this.labelX1.TabIndex = 4;
            this.labelX1.Text = "ID";
            // 
            // labelX2
            // 
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.Class = "";
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX2.Location = new System.Drawing.Point(9, 79);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(75, 23);
            this.labelX2.TabIndex = 5;
            this.labelX2.Text = "Loại";
            // 
            // txtTenLoai
            // 
            // 
            // 
            // 
            this.txtTenLoai.Border.Class = "TextBoxBorder";
            this.txtTenLoai.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtTenLoai.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenLoai.Location = new System.Drawing.Point(157, 9);
            this.txtTenLoai.Name = "txtTenLoai";
            this.txtTenLoai.Size = new System.Drawing.Size(158, 26);
            this.txtTenLoai.TabIndex = 1;
            this.txtTenLoai.TextChanged += new System.EventHandler(this.txtTenLoai_TextChanged);
            // 
            // txtIDLoai
            // 
            // 
            // 
            // 
            this.txtIDLoai.Border.Class = "TextBoxBorder";
            this.txtIDLoai.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtIDLoai.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtIDLoai.Location = new System.Drawing.Point(26, 9);
            this.txtIDLoai.Name = "txtIDLoai";
            this.txtIDLoai.ReadOnly = true;
            this.txtIDLoai.Size = new System.Drawing.Size(93, 26);
            this.txtIDLoai.TabIndex = 0;
            this.txtIDLoai.TextChanged += new System.EventHandler(this.txtIDLoai_TextChanged);
            // 
            // lb
            // 
            // 
            // 
            // 
            this.lb.BackgroundStyle.Class = "";
            this.lb.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lb.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb.Location = new System.Drawing.Point(4, 9);
            this.lb.Name = "lb";
            this.lb.Size = new System.Drawing.Size(27, 23);
            this.lb.TabIndex = 15;
            this.lb.Text = "ID";
            // 
            // labelX6
            // 
            // 
            // 
            // 
            this.labelX6.BackgroundStyle.Class = "";
            this.labelX6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX6.Location = new System.Drawing.Point(125, 9);
            this.labelX6.Name = "labelX6";
            this.labelX6.Size = new System.Drawing.Size(38, 23);
            this.labelX6.TabIndex = 14;
            this.labelX6.Text = "Tên";
            // 
            // btnCapNhatLoaiMon
            // 
            this.btnCapNhatLoaiMon.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnCapNhatLoaiMon.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnCapNhatLoaiMon.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCapNhatLoaiMon.Location = new System.Drawing.Point(515, 6);
            this.btnCapNhatLoaiMon.Name = "btnCapNhatLoaiMon";
            this.btnCapNhatLoaiMon.Size = new System.Drawing.Size(91, 35);
            this.btnCapNhatLoaiMon.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnCapNhatLoaiMon.TabIndex = 4;
            this.btnCapNhatLoaiMon.Text = "Cập nhật";
            this.btnCapNhatLoaiMon.Click += new System.EventHandler(this.btnCapNhatLoaiMon_Click);
            // 
            // btnXoaLoaiMon
            // 
            this.btnXoaLoaiMon.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnXoaLoaiMon.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnXoaLoaiMon.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXoaLoaiMon.Location = new System.Drawing.Point(418, 6);
            this.btnXoaLoaiMon.Name = "btnXoaLoaiMon";
            this.btnXoaLoaiMon.Size = new System.Drawing.Size(91, 35);
            this.btnXoaLoaiMon.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnXoaLoaiMon.TabIndex = 3;
            this.btnXoaLoaiMon.Text = "Xóa";
            this.btnXoaLoaiMon.Click += new System.EventHandler(this.btnXoaLoaiMon_Click);
            // 
            // btnThemLoaiMon
            // 
            this.btnThemLoaiMon.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnThemLoaiMon.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnThemLoaiMon.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnThemLoaiMon.Location = new System.Drawing.Point(321, 6);
            this.btnThemLoaiMon.Name = "btnThemLoaiMon";
            this.btnThemLoaiMon.Size = new System.Drawing.Size(91, 35);
            this.btnThemLoaiMon.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnThemLoaiMon.TabIndex = 2;
            this.btnThemLoaiMon.Text = "Thêm";
            this.btnThemLoaiMon.Click += new System.EventHandler(this.btnThemLoaiMon_Click);
            // 
            // dgvMonAn
            // 
            this.dgvMonAn.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvMonAn.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle9;
            this.dgvMonAn.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvMonAn.DefaultCellStyle = dataGridViewCellStyle10;
            this.dgvMonAn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvMonAn.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(170)))), ((int)(((byte)(170)))));
            this.dgvMonAn.Location = new System.Drawing.Point(0, 32);
            this.dgvMonAn.Name = "dgvMonAn";
            this.dgvMonAn.ReadOnly = true;
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvMonAn.RowHeadersDefaultCellStyle = dataGridViewCellStyle11;
            this.dgvMonAn.Size = new System.Drawing.Size(727, 450);
            this.dgvMonAn.TabIndex = 2;
            this.dgvMonAn.SelectionChanged += new System.EventHandler(this.dgvMonAn_SelectionChanged);
            // 
            // labelX4
            // 
            // 
            // 
            // 
            this.labelX4.BackgroundStyle.Class = "";
            this.labelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX4.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX4.Location = new System.Drawing.Point(257, 6);
            this.labelX4.Name = "labelX4";
            this.labelX4.Size = new System.Drawing.Size(78, 35);
            this.labelX4.TabIndex = 5;
            this.labelX4.Text = "Trang";
            // 
            // nrudTrang
            // 
            this.nrudTrang.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nrudTrang.Location = new System.Drawing.Point(341, 6);
            this.nrudTrang.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nrudTrang.Name = "nrudTrang";
            this.nrudTrang.Size = new System.Drawing.Size(120, 38);
            this.nrudTrang.TabIndex = 4;
            this.nrudTrang.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.nrudTrang.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nrudTrang.ValueChanged += new System.EventHandler(this.nrudTrang_ValueChanged);
            // 
            // btnDau
            // 
            this.btnDau.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnDau.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnDau.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.btnDau.Location = new System.Drawing.Point(3, 6);
            this.btnDau.Name = "btnDau";
            this.btnDau.Size = new System.Drawing.Size(121, 38);
            this.btnDau.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnDau.TabIndex = 3;
            this.btnDau.Text = "Đầu";
            this.btnDau.Click += new System.EventHandler(this.btnDau_Click);
            // 
            // btnCuoi
            // 
            this.btnCuoi.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnCuoi.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnCuoi.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.btnCuoi.Location = new System.Drawing.Point(594, 6);
            this.btnCuoi.Name = "btnCuoi";
            this.btnCuoi.Size = new System.Drawing.Size(121, 38);
            this.btnCuoi.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnCuoi.TabIndex = 2;
            this.btnCuoi.Text = "Cuối";
            this.btnCuoi.Click += new System.EventHandler(this.btnCuoi_Click);
            // 
            // btnSau
            // 
            this.btnSau.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnSau.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnSau.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.btnSau.Location = new System.Drawing.Point(467, 6);
            this.btnSau.Name = "btnSau";
            this.btnSau.Size = new System.Drawing.Size(121, 38);
            this.btnSau.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnSau.TabIndex = 0;
            this.btnSau.Text = "Sau";
            this.btnSau.Click += new System.EventHandler(this.btnSau_Click);
            // 
            // pnFooter
            // 
            this.pnFooter.CanvasColor = System.Drawing.SystemColors.Control;
            this.pnFooter.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.pnFooter.Controls.Add(this.labelX4);
            this.pnFooter.Controls.Add(this.nrudTrang);
            this.pnFooter.Controls.Add(this.btnDau);
            this.pnFooter.Controls.Add(this.btnCuoi);
            this.pnFooter.Controls.Add(this.btnTruoc);
            this.pnFooter.Controls.Add(this.btnSau);
            this.pnFooter.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnFooter.Location = new System.Drawing.Point(0, 482);
            this.pnFooter.Name = "pnFooter";
            this.pnFooter.Size = new System.Drawing.Size(727, 50);
            this.pnFooter.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.pnFooter.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.pnFooter.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.pnFooter.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.pnFooter.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.pnFooter.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.pnFooter.Style.GradientAngle = 90;
            this.pnFooter.TabIndex = 0;
            // 
            // btnTruoc
            // 
            this.btnTruoc.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnTruoc.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnTruoc.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.btnTruoc.Location = new System.Drawing.Point(130, 6);
            this.btnTruoc.Name = "btnTruoc";
            this.btnTruoc.Size = new System.Drawing.Size(121, 38);
            this.btnTruoc.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnTruoc.TabIndex = 1;
            this.btnTruoc.Text = "Trước";
            this.btnTruoc.Click += new System.EventHandler(this.btnTruoc_Click);
            // 
            // ckNhom
            // 
            // 
            // 
            // 
            this.ckNhom.BackgroundStyle.Class = "";
            this.ckNhom.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.ckNhom.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ckNhom.Location = new System.Drawing.Point(625, 3);
            this.ckNhom.Name = "ckNhom";
            this.ckNhom.Size = new System.Drawing.Size(90, 23);
            this.ckNhom.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.ckNhom.TabIndex = 1;
            this.ckNhom.Text = "Nhóm";
            this.ckNhom.CheckedChanged += new System.EventHandler(this.ckNhom_CheckedChanged);
            // 
            // cbbNhomLoaiMon
            // 
            this.cbbNhomLoaiMon.DisplayMember = "Text";
            this.cbbNhomLoaiMon.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cbbNhomLoaiMon.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbbNhomLoaiMon.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbNhomLoaiMon.FormattingEnabled = true;
            this.cbbNhomLoaiMon.ItemHeight = 20;
            this.cbbNhomLoaiMon.Location = new System.Drawing.Point(355, 3);
            this.cbbNhomLoaiMon.Name = "cbbNhomLoaiMon";
            this.cbbNhomLoaiMon.Size = new System.Drawing.Size(264, 26);
            this.cbbNhomLoaiMon.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cbbNhomLoaiMon.TabIndex = 0;
            this.cbbNhomLoaiMon.SelectedIndexChanged += new System.EventHandler(this.cbbNhomLoaiMon_SelectedIndexChanged);
            // 
            // panelEx3
            // 
            this.panelEx3.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelEx3.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.panelEx3.Controls.Add(this.ckNhom);
            this.panelEx3.Controls.Add(this.cbbNhomLoaiMon);
            this.panelEx3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelEx3.Location = new System.Drawing.Point(0, 0);
            this.panelEx3.Name = "panelEx3";
            this.panelEx3.Size = new System.Drawing.Size(727, 32);
            this.panelEx3.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelEx3.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelEx3.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.panelEx3.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelEx3.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelEx3.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelEx3.Style.GradientAngle = 90;
            this.panelEx3.TabIndex = 1;
            // 
            // grThongTinLoaiMon
            // 
            this.grThongTinLoaiMon.CanvasColor = System.Drawing.SystemColors.Control;
            this.grThongTinLoaiMon.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.grThongTinLoaiMon.Controls.Add(this.panelEx4);
            this.grThongTinLoaiMon.Controls.Add(this.pnLoai);
            this.grThongTinLoaiMon.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grThongTinLoaiMon.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grThongTinLoaiMon.Location = new System.Drawing.Point(727, 150);
            this.grThongTinLoaiMon.Name = "grThongTinLoaiMon";
            this.grThongTinLoaiMon.Size = new System.Drawing.Size(627, 382);
            // 
            // 
            // 
            this.grThongTinLoaiMon.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.grThongTinLoaiMon.Style.BackColorGradientAngle = 90;
            this.grThongTinLoaiMon.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.grThongTinLoaiMon.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grThongTinLoaiMon.Style.BorderBottomWidth = 1;
            this.grThongTinLoaiMon.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.grThongTinLoaiMon.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grThongTinLoaiMon.Style.BorderLeftWidth = 1;
            this.grThongTinLoaiMon.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grThongTinLoaiMon.Style.BorderRightWidth = 1;
            this.grThongTinLoaiMon.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.grThongTinLoaiMon.Style.BorderTopWidth = 1;
            this.grThongTinLoaiMon.Style.Class = "";
            this.grThongTinLoaiMon.Style.CornerDiameter = 4;
            this.grThongTinLoaiMon.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.grThongTinLoaiMon.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.grThongTinLoaiMon.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.grThongTinLoaiMon.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            // 
            // 
            // 
            this.grThongTinLoaiMon.StyleMouseDown.Class = "";
            this.grThongTinLoaiMon.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.grThongTinLoaiMon.StyleMouseOver.Class = "";
            this.grThongTinLoaiMon.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.grThongTinLoaiMon.TabIndex = 11;
            this.grThongTinLoaiMon.Text = "THÔNG TIN DANH MỤC MÓN";
            // 
            // panelEx4
            // 
            this.panelEx4.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelEx4.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.panelEx4.Controls.Add(this.dgvLoaiMon);
            this.panelEx4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelEx4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panelEx4.Location = new System.Drawing.Point(0, 0);
            this.panelEx4.Name = "panelEx4";
            this.panelEx4.Size = new System.Drawing.Size(621, 296);
            this.panelEx4.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelEx4.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelEx4.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.panelEx4.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelEx4.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelEx4.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelEx4.Style.GradientAngle = 90;
            this.panelEx4.TabIndex = 2;
            this.panelEx4.Text = "panelEx4";
            // 
            // dgvLoaiMon
            // 
            this.dgvLoaiMon.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvLoaiMon.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvLoaiMon.DefaultCellStyle = dataGridViewCellStyle12;
            this.dgvLoaiMon.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvLoaiMon.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(170)))), ((int)(((byte)(170)))));
            this.dgvLoaiMon.Location = new System.Drawing.Point(0, 0);
            this.dgvLoaiMon.MultiSelect = false;
            this.dgvLoaiMon.Name = "dgvLoaiMon";
            this.dgvLoaiMon.ReadOnly = true;
            this.dgvLoaiMon.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvLoaiMon.Size = new System.Drawing.Size(621, 296);
            this.dgvLoaiMon.TabIndex = 3;
            this.dgvLoaiMon.SelectionChanged += new System.EventHandler(this.dgvLoaiMon_SelectionChanged);
            // 
            // panelEx1
            // 
            this.panelEx1.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelEx1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.panelEx1.Controls.Add(this.dgvMonAn);
            this.panelEx1.Controls.Add(this.panelEx3);
            this.panelEx1.Controls.Add(this.pnFooter);
            this.panelEx1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelEx1.Location = new System.Drawing.Point(0, 0);
            this.panelEx1.Name = "panelEx1";
            this.panelEx1.Size = new System.Drawing.Size(727, 532);
            this.panelEx1.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelEx1.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelEx1.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.panelEx1.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelEx1.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelEx1.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelEx1.Style.GradientAngle = 90;
            this.panelEx1.TabIndex = 0;
            this.panelEx1.Text = "panelEx1";
            // 
            // pnBackground
            // 
            this.pnBackground.CanvasColor = System.Drawing.SystemColors.Control;
            this.pnBackground.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.pnBackground.Controls.Add(this.grThongTinLoaiMon);
            this.pnBackground.Controls.Add(this.grThongTinMonAn);
            this.pnBackground.Controls.Add(this.panelEx1);
            this.pnBackground.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnBackground.Location = new System.Drawing.Point(0, 0);
            this.pnBackground.Name = "pnBackground";
            this.pnBackground.Size = new System.Drawing.Size(1354, 532);
            this.pnBackground.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.pnBackground.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.pnBackground.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.pnBackground.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.pnBackground.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.pnBackground.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.pnBackground.Style.GradientAngle = 90;
            this.pnBackground.TabIndex = 2;
            // 
            // frmQuanLyMonAn
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1354, 532);
            this.Controls.Add(this.pnBackground);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmQuanLyMonAn";
            this.Text = "frmQuanLyMonAn";
            this.pnLoai.ResumeLayout(false);
            this.grThongTinMonAn.ResumeLayout(false);
            this.grThongTinMonAn.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMonAn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nrudTrang)).EndInit();
            this.pnFooter.ResumeLayout(false);
            this.panelEx3.ResumeLayout(false);
            this.grThongTinLoaiMon.ResumeLayout(false);
            this.panelEx4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvLoaiMon)).EndInit();
            this.panelEx1.ResumeLayout(false);
            this.pnBackground.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.PanelEx pnLoai;
        private DevComponents.DotNetBar.Controls.GroupPanel grThongTinMonAn;
        private DevComponents.DotNetBar.LabelX labelX5;
        private DevComponents.DotNetBar.Controls.TextBoxX txtGiaDem;
        private DevComponents.DotNetBar.Controls.TextBoxX txtGiaNgay;
        private System.Windows.Forms.RadioButton rdDem;
        private System.Windows.Forms.RadioButton rdNgay;
        private DevComponents.DotNetBar.LabelX labelX8;
        private DevComponents.DotNetBar.LabelX lbGiaDem;
        private DevComponents.DotNetBar.ButtonX btnCapNhatMon;
        private DevComponents.DotNetBar.ButtonX btnXoaMon;
        private DevComponents.DotNetBar.Controls.TextBoxX txtIDMon;
        private DevComponents.DotNetBar.ButtonX btnThemMon;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cbbLoaiMon;
        private DevComponents.DotNetBar.Controls.TextBoxX txtTenMon;
        private DevComponents.DotNetBar.LabelX labelX3;
        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.DotNetBar.LabelX labelX2;
        private DevComponents.DotNetBar.Controls.TextBoxX txtTenLoai;
        private DevComponents.DotNetBar.Controls.TextBoxX txtIDLoai;
        private DevComponents.DotNetBar.LabelX lb;
        private DevComponents.DotNetBar.LabelX labelX6;
        private DevComponents.DotNetBar.ButtonX btnCapNhatLoaiMon;
        private DevComponents.DotNetBar.ButtonX btnXoaLoaiMon;
        private DevComponents.DotNetBar.ButtonX btnThemLoaiMon;
        private DevComponents.DotNetBar.Controls.DataGridViewX dgvMonAn;
        private DevComponents.DotNetBar.LabelX labelX4;
        private System.Windows.Forms.NumericUpDown nrudTrang;
        private DevComponents.DotNetBar.ButtonX btnDau;
        private DevComponents.DotNetBar.ButtonX btnCuoi;
        private DevComponents.DotNetBar.ButtonX btnSau;
        private DevComponents.DotNetBar.PanelEx pnFooter;
        private DevComponents.DotNetBar.ButtonX btnTruoc;
        private DevComponents.DotNetBar.Controls.CheckBoxX ckNhom;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cbbNhomLoaiMon;
        private DevComponents.DotNetBar.PanelEx panelEx3;
        private DevComponents.DotNetBar.Controls.GroupPanel grThongTinLoaiMon;
        private DevComponents.DotNetBar.PanelEx panelEx1;
        private DevComponents.DotNetBar.PanelEx pnBackground;
        private DevComponents.DotNetBar.PanelEx panelEx4;
        private DevComponents.DotNetBar.Controls.DataGridViewX dgvLoaiMon;
        private DevComponents.DotNetBar.ButtonX btnShow;
    }
}