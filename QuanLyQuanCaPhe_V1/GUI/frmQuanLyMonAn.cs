﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BUS;
using DTO;
using DevComponents.DotNetBar.Controls;
namespace GUI
{
    public partial class frmQuanLyMonAn : Form
    {
        #region Thuoc tinh
        ErrorProvider err = new ErrorProvider();
        #endregion
        #region Phuong thuc
        public frmQuanLyMonAn()
        {
            InitializeComponent();
            LoadDSMonAn(int.Parse(nrudTrang.Value.ToString().Trim()), ckNhom.Checked);
            dgvMonAn.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dgvMonAn.MultiSelect = false;
            dgvMonAn.ReadOnly = true;
            rdNgay.Checked = true;
            cbbNhomLoaiMon.Enabled = false;
            cbbLoaiMon.DataSource = BUS_LoaiMon.Instance.LayDanhSach();
            cbbLoaiMon.DisplayMember = "Ten";
            cbbLoaiMon.ValueMember = "ID";
            cbbNhomLoaiMon.DataSource = BUS_LoaiMon.Instance.LayDanhSach();
            cbbNhomLoaiMon.DisplayMember = "Ten";
            cbbNhomLoaiMon.ValueMember = "ID";
            pnLoai.Visible = false;
            dgvLoaiMon.DataSource = BUS_LoaiMon.Instance.LayDanhSach();
            dgvLoaiMon.Columns["ID"].HeaderText = "ID Loại";
            dgvLoaiMon.Columns["Ten"].HeaderText = "Tên Loại";
        }
        private void LoadDSMonAn(int Trang, bool isFillLoaiMon)
        {
            try
            {
                switch (isFillLoaiMon)
                {
                    case false:
                        {

                            dgvMonAn.DataSource = BUS_MonAn.Instance.LayDanhSach_Trang(Trang);
                            dgvMonAn.Columns["ID"].HeaderText = "ID Món";
                            dgvMonAn.Columns["Ten"].HeaderText = "Tên Món";
                            dgvMonAn.Columns["IDLoai"].HeaderText = "Loại";
                            dgvMonAn.Columns["Mode"].HeaderText = "Chế Độ";
                            dgvMonAn.Columns["GiaNgay"].HeaderText = "Giá Này";
                            dgvMonAn.Columns["GiaDem"].HeaderText = "Giá Đêm";
                            break;
                        }
                    case true:
                        {
                            dgvMonAn.DataSource = BUS_MonAn.Instance.LayDanhSach_Trang_IDLoai(Trang, int.Parse(cbbNhomLoaiMon.SelectedValue.ToString().Trim()));
                            dgvMonAn.Columns["ID"].HeaderText = "ID Món";
                            dgvMonAn.Columns["Ten"].HeaderText = "Tên Món";
                            dgvMonAn.Columns["IDLoai"].HeaderText = "Loại";
                            dgvMonAn.Columns["Mode"].HeaderText = "Chế Độ";
                            dgvMonAn.Columns["GiaNgay"].HeaderText = "Giá Này";
                            dgvMonAn.Columns["GiaDem"].HeaderText = "Giá Đêm";
                            break;
                        }
                }
            }
            catch (Exception)
            {

            }
        }
        private int GetMaxPage(bool isFill)
        {
            try
            {
                if (isFill)
                {
                    int CountPage = BUS_MonAn.Instance.GetCount(int.Parse(cbbNhomLoaiMon.SelectedValue.ToString().Trim()));
                    if (CountPage % 20 == 0)
                        return (int)(CountPage / 20);
                    return (int)(CountPage / 20) + 1;
                }
                else
                {
                    int CountPage = BUS_MonAn.Instance.GetCount();
                    if (CountPage % 20 == 0)
                        return (int)(CountPage / 20);
                    return (int)(CountPage / 20) + 1;
                }
            }
            catch (Exception)
            {
                return -1;
            }
        }
        #endregion
        #region Events
        private void btnDau_Click(object sender, EventArgs e)
        {
            nrudTrang.Value = nrudTrang.Minimum;
        }
        private void btnTruoc_Click(object sender, EventArgs e)
        {
            int value = (int)nrudTrang.Value;
            if (value - 1 < nrudTrang.Minimum)
                return;
            nrudTrang.Value--;
        }
        private void btnSau_Click(object sender, EventArgs e)
        {
            int value = (int)nrudTrang.Value;
            if (value + 1 > nrudTrang.Maximum)
                return;
            nrudTrang.Value++;
        }
        private void btnCuoi_Click(object sender, EventArgs e)
        {
            nrudTrang.Value = nrudTrang.Maximum;
        }
        private void btnThemMon_Click(object sender, EventArgs e)
        {
            if (txtTenMon.Text.Trim() == "")
            {
                err.SetError(txtTenMon, "TÊN MÓN KHÔNG ĐƯỢC BỎ TRỐNG");
                return;
            }
            if (txtGiaNgay.Text.Trim() == "")
            {
                err.SetError(txtGiaNgay, "GIÁ NGÀY KHÔNG ĐƯỢC BỎ TRỐNG");
                return;
            }
            if (rdDem.Checked)
            {
                if (txtGiaDem.Text.Trim() == "")
                {
                    err.SetError(txtGiaDem, "GIÁ ĐÊM KHÔNG ĐƯỢC BỎ TRỐNG");
                    return;
                }
            }
            DTO_MonAn mon = new DTO_MonAn();
            mon.ID = int.Parse(txtIDMon.Text.Trim());
            mon.Ten = txtTenMon.Text.Trim();
            if (rdDem.Checked)
            {
                mon.Mode = "Đêm";
                mon.GiaDem = float.Parse(txtGiaDem.Text.Trim());
                mon.GiaNgay = float.Parse(txtGiaNgay.Text.Trim());
            }
            else
            {
                mon.Mode = "Ngày";
                mon.GiaDem = 0;
                mon.GiaNgay = float.Parse(txtGiaNgay.Text.Trim());
            }
            mon.IDLoai = int.Parse(cbbLoaiMon.SelectedValue.ToString().Trim());

            if (BUS_MonAn.Instance.Insert(mon))
            {
                MessageBox.Show("Thêm thành công!", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                BUS_Database.Instance.GhiLichSu(frmDangNhap.Taikhoan.TaiKhoan, "Thêm món: " + txtTenMon.Text, DateTime.Now);
                frmMain.LoadLichSu();
                LoadDSMonAn(int.Parse(nrudTrang.Value.ToString().Trim()), ckNhom.Checked);
                txtIDLoai.Clear(); txtTenMon.Clear(); txtGiaDem.Clear(); txtGiaNgay.Clear();
            }
            else
            {
                MessageBox.Show("Thêm thất bại!", "Lỗi thêm món", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void btnXoaMon_Click(object sender, EventArgs e)
        {

            if (BUS_MonAn.Instance.Delete(int.Parse(dgvMonAn.CurrentRow.Cells[0].Value.ToString().Trim())))
            {
                MessageBox.Show("Đã xóa " + txtTenMon.Text, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                BUS_Database.Instance.GhiLichSu(frmDangNhap.Taikhoan.TaiKhoan, "Xóa món: " + dgvMonAn.CurrentRow.Cells[1].Value.ToString().Trim(), DateTime.Now);
                frmMain.LoadLichSu();
                LoadDSMonAn(int.Parse(nrudTrang.Value.ToString().Trim()), ckNhom.Checked);
                txtIDLoai.Clear(); txtTenMon.Clear(); txtGiaDem.Clear(); txtGiaNgay.Clear();
            }
            else
            {
                MessageBox.Show("Xóa thất bại", "Lỗi xóa món ăn", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void btnCapNhatMon_Click(object sender, EventArgs e)
        {

            if (txtTenMon.Text.Trim() == "")
            {
                err.SetError(txtTenMon, "TÊN MÓN KHÔNG ĐƯỢC BỎ TRỐNG");
                return;
            }
            if (txtGiaNgay.Text.Trim() == "")
            {
                err.SetError(txtGiaNgay, "GIÁ NGÀY KHÔNG ĐƯỢC BỎ TRỐNG");
                return;
            }
            if (rdDem.Checked)
            {
                if (txtGiaDem.Text.Trim() == "")
                {
                    err.SetError(txtGiaDem, "GIÁ ĐÊM KHÔNG ĐƯỢC BỎ TRỐNG");
                    return;
                }
            }
            DTO_MonAn mon = new DTO_MonAn();
            mon.ID = int.Parse(dgvMonAn.CurrentRow.Cells[0].Value.ToString().Trim());
            mon.Ten = txtTenMon.Text.Trim();
            if (rdDem.Checked)
            {
                mon.Mode = "Đêm";
                mon.GiaDem = float.Parse(txtGiaDem.Text.Trim());
                mon.GiaNgay = float.Parse(txtGiaNgay.Text.Trim());
            }
            else
            {
                mon.Mode = "Ngày";
                mon.GiaDem = 0;
                mon.GiaNgay = float.Parse(txtGiaNgay.Text.Trim());
            }
            mon.IDLoai = int.Parse(cbbLoaiMon.SelectedValue.ToString().Trim());
            if (BUS_MonAn.Instance.Update(mon))
            {
                MessageBox.Show("Cập nhật thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                MessageBox.Show(BUS_LoaiMon.Instance.LayTen_ID((int)cbbLoaiMon.SelectedValue));

                string tmp = "Cập nhật: Tên món " + dgvMonAn.CurrentRow.Cells[1].Value.ToString().Trim() + " Danh mục: " + BUS_LoaiMon.Instance.LayTen_ID(int.Parse(cbbLoaiMon.SelectedValue.ToString())) + " Mode: " + dgvMonAn.CurrentRow.Cells[3].Value.ToString().Trim() + " Giá Ngày: " + dgvMonAn.CurrentRow.Cells[4].Value.ToString().Trim() + " Giá Đêm: " + dgvMonAn.CurrentRow.Cells[5].Value.ToString().Trim();
                string tmp1 = " Thành Tên món: "+mon.Ten+" Danh mục: "+BUS_LoaiMon.Instance.LayTen_ID(mon.IDLoai)+" Mode: "+mon.Mode+" Giá ngày: "+mon.GiaNgay+" Giá đêm: "+mon.GiaDem ;
                BUS_Database.Instance.GhiLichSu(frmDangNhap.Taikhoan.TaiKhoan,tmp+tmp1, DateTime.Now);
                frmMain.LoadLichSu();
                LoadDSMonAn(int.Parse(nrudTrang.Value.ToString().Trim()), ckNhom.Checked);
                txtIDLoai.Clear(); txtTenMon.Clear(); txtGiaDem.Clear(); txtGiaNgay.Clear();
            }
            else
                MessageBox.Show("Cập nhật thất bại", "Lỗi cập nhật", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
        private void rdNgay_CheckedChanged(object sender, EventArgs e)
        {
            if (!rdDem.Checked)
            {
                lbGiaDem.Visible = false;
                txtGiaDem.Visible = false;
            }
            else
            {
                lbGiaDem.Visible = true;
                txtGiaDem.Visible = true;
            }
        }
        private void rdDem_CheckedChanged(object sender, EventArgs e)
        {
            if (!rdDem.Checked)
            {
                lbGiaDem.Visible = false;
                txtGiaDem.Visible = false;
            }
            else
            {
                lbGiaDem.Visible = true;
                txtGiaDem.Visible = true;
            }
        }
        private void btnThemLoaiMon_Click(object sender, EventArgs e)
        {
            if (txtTenLoai.Text.Trim() == "")
            {
                err.SetError(txtTenLoai, "TÊN LOẠI KHÔNG ĐƯỢC BỎ TRỐNG");
                return;
            }
            DTO_LoaiMon loai = new DTO_LoaiMon();
            loai.Ten = txtTenLoai.Text.Trim();
            if (BUS_LoaiMon.Instance.Insert(loai))
            {
                MessageBox.Show("Thêm thành công!", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                BUS_Database.Instance.GhiLichSu(frmDangNhap.Taikhoan.TaiKhoan, "Thêm Danh mục: " + txtTenLoai.Text, DateTime.Now);
                frmMain.LoadLichSu();
                dgvLoaiMon.DataSource = BUS_LoaiMon.Instance.LayDanhSach();
                cbbLoaiMon.DataSource = BUS_LoaiMon.Instance.LayDanhSach();
                cbbLoaiMon.DisplayMember = "Ten";
                cbbLoaiMon.ValueMember = "ID";

                cbbNhomLoaiMon.DataSource = BUS_LoaiMon.Instance.LayDanhSach();
                cbbNhomLoaiMon.DisplayMember = "Ten";
                cbbNhomLoaiMon.ValueMember = "ID";


                txtTenLoai.Clear(); txtIDLoai.Clear();
            }
        }
        private void btnXoaLoaiMon_Click(object sender, EventArgs e)
        {

            if (BUS_LoaiMon.Instance.Delete(int.Parse(dgvLoaiMon.CurrentRow.Cells[0].Value.ToString().Trim())))
            {
                MessageBox.Show("Đã xóa " + dgvLoaiMon.CurrentRow.Cells[1].Value.ToString().Trim(), "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                BUS_Database.Instance.GhiLichSu(frmDangNhap.Taikhoan.TaiKhoan, "Xóa danh mục: " + dgvLoaiMon.CurrentRow.Cells[1].Value.ToString().Trim(), DateTime.Now);
                frmMain.LoadLichSu();
                dgvLoaiMon.DataSource = BUS_LoaiMon.Instance.LayDanhSach();
                cbbLoaiMon.DataSource = BUS_LoaiMon.Instance.LayDanhSach();
                cbbLoaiMon.DisplayMember = "Ten";
                cbbLoaiMon.ValueMember = "ID";

                cbbNhomLoaiMon.DataSource = BUS_LoaiMon.Instance.LayDanhSach();
                cbbNhomLoaiMon.DisplayMember = "Ten";
                cbbNhomLoaiMon.ValueMember = "ID";
                txtTenLoai.Clear(); txtIDLoai.Clear();
            }
            else
                MessageBox.Show("Xóa thất bại", "Lỗi xóa loại món", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
        private void btnCapNhatLoaiMon_Click(object sender, EventArgs e)
        {
            if (txtTenLoai.Text.Trim() == "")
            {
                err.SetError(txtTenLoai, "TÊN LOẠI KHÔNG ĐƯỢC BỎ TRỐNG");
                return;
            }
            DTO_LoaiMon loai = new DTO_LoaiMon();
            loai.ID = int.Parse(dgvLoaiMon.CurrentRow.Cells[0].Value.ToString().Trim());
            loai.Ten = txtTenLoai.Text.Trim();
            if (BUS_LoaiMon.Instance.Update(loai))
            {
                MessageBox.Show("Cập nhật thành công ", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                BUS_Database.Instance.GhiLichSu(frmDangNhap.Taikhoan.TaiKhoan, "Cập nhật danh mục: " + dgvLoaiMon.CurrentRow.Cells[1].Value.ToString().Trim()+" Thành: "+txtTenLoai.Text, DateTime.Now);
                frmMain.LoadLichSu();
                dgvLoaiMon.DataSource = BUS_LoaiMon.Instance.LayDanhSach();
                cbbLoaiMon.DataSource = BUS_LoaiMon.Instance.LayDanhSach();
                cbbLoaiMon.DisplayMember = "Ten";
                cbbLoaiMon.ValueMember = "ID";

                cbbNhomLoaiMon.DataSource = BUS_LoaiMon.Instance.LayDanhSach();
                cbbNhomLoaiMon.DisplayMember = "Ten";
                cbbNhomLoaiMon.ValueMember = "ID";
                txtTenLoai.Clear(); txtIDLoai.Clear();
            }
            else
                MessageBox.Show("Cập nhật thất bại", "Lỗi cập nhật loại món", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
        private void cbbNhomLoaiMon_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadDSMonAn(int.Parse(nrudTrang.Value.ToString().Trim()), ckNhom.Checked);
        }
        private void ckNhom_CheckedChanged(object sender, EventArgs e)
        {
            nrudTrang.Maximum = GetMaxPage(ckNhom.Checked);
            try
            {
                if (ckNhom.Checked)
                {
                    cbbNhomLoaiMon.Enabled = true;
                    nrudTrang.Value = 1;
                    LoadDSMonAn(int.Parse(nrudTrang.Value.ToString().Trim()), ckNhom.Checked);
                }
                else
                {
                    cbbNhomLoaiMon.Enabled = false;
                    nrudTrang.Value = 1;
                    LoadDSMonAn(int.Parse(nrudTrang.Value.ToString().Trim()), ckNhom.Checked);
                }
            }
            catch (Exception)
            { }
        }
        private void nrudTrang_ValueChanged(object sender, EventArgs e)
        {
            nrudTrang.Maximum = GetMaxPage(ckNhom.Checked);
            if (ckNhom.Checked)
            {
                LoadDSMonAn(int.Parse(nrudTrang.Value.ToString()), ckNhom.Checked);
            }
            else
            {
                LoadDSMonAn(int.Parse(nrudTrang.Value.ToString()), ckNhom.Checked);
            }
        }
        private void txtGiaDem_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && !Char.IsControl(e.KeyChar))
            {
                e.Handled = true;
            }
        }
        private void txtGiaNgay_TextChanged(object sender, EventArgs e)
        {
            err.SetError(txtGiaNgay, "");
        }
        private void txtGiaNgay_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && !Char.IsControl(e.KeyChar))
            {
                e.Handled = true;
            }
        }
        private void txtGiaDem_TextChanged(object sender, EventArgs e)
        {
            err.SetError(txtGiaDem, "");
        }
        private void dgvMonAn_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                DataGridViewRow row = ((DataGridViewX)sender).CurrentRow;
                txtIDMon.Text = row.Cells[0].Value.ToString().Trim();
                txtTenMon.Text = row.Cells[1].Value.ToString().Trim();
                cbbLoaiMon.SelectedValue = row.Cells[2].Value;
                if (row.Cells[3].Value.ToString().Trim().Equals("Đêm"))
                    rdDem.Checked = true;
                else
                    rdNgay.Checked = true;
                txtGiaNgay.Text = row.Cells[4].Value.ToString().Trim();
                txtGiaDem.Text = row.Cells[5].Value.ToString().Trim();
            }
            catch (Exception)
            {

            }

        }
        private void txtTenMon_TextChanged(object sender, EventArgs e)
        {
            err.SetError(txtTenMon, "");
        }
        private void txtIDMon_TextChanged(object sender, EventArgs e)
        {
            err.SetError(txtIDMon, "");
        }
        private void txtIDLoai_TextChanged(object sender, EventArgs e)
        {
            err.SetError(txtIDLoai, "");
        }
        private void txtTenLoai_TextChanged(object sender, EventArgs e)
        {
            err.SetError(txtTenLoai, "");
        }
        private void dgvLoaiMon_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                DataGridViewRow row = ((DataGridViewX)sender).CurrentRow;
                txtIDLoai.Text = row.Cells[0].Value.ToString().Trim();
                txtTenLoai.Text = row.Cells[1].Value.ToString().Trim();
            }
            catch (Exception)
            {
            }
        }
        private void btnShow_Click(object sender, EventArgs e)
        {
            btnShow.Checked = !btnShow.Checked;
            pnLoai.Visible = btnShow.Checked;
        }
        #endregion


    }
}
