﻿namespace GUI
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.styleManager1 = new DevComponents.DotNetBar.StyleManager(this.components);
            this.ribTenNguoiDung = new DevComponents.DotNetBar.RibbonTabItem();
            this.pnMenu = new DevComponents.DotNetBar.RibbonPanel();
            this.rib_QuanLy = new DevComponents.DotNetBar.RibbonBar();
            this.btnQuanLyBan = new DevComponents.DotNetBar.ButtonItem();
            this.btnQuanLyMonAn = new DevComponents.DotNetBar.ButtonItem();
            this.rib_BanHang = new DevComponents.DotNetBar.RibbonBar();
            this.btnQuanLyQuan = new DevComponents.DotNetBar.ButtonItem();
            this.qatCustomizeItem1 = new DevComponents.DotNetBar.QatCustomizeItem();
            this.frm_ribbon = new DevComponents.DotNetBar.RibbonControl();
            this.office2007StartButton1 = new DevComponents.DotNetBar.Office2007StartButton();
            this.itemMenu = new DevComponents.DotNetBar.ItemContainer();
            this.btnQuanLyTaiKhoan = new DevComponents.DotNetBar.ButtonItem();
            this.btnDoiMatKhau = new DevComponents.DotNetBar.ButtonItem();
            this.btnThongKe = new DevComponents.DotNetBar.ButtonItem();
            this.btnDoiAnhNen = new DevComponents.DotNetBar.ButtonItem();
            this.btnTrangIn = new DevComponents.DotNetBar.ButtonItem();
            this.ckCo = new DevComponents.DotNetBar.CheckBoxItem();
            this.ckKhong = new DevComponents.DotNetBar.CheckBoxItem();
            this.btnDatabase = new DevComponents.DotNetBar.ButtonItem();
            this.btnBackup = new DevComponents.DotNetBar.ButtonItem();
            this.btnRetore = new DevComponents.DotNetBar.ButtonItem();
            this.btnLichSu = new DevComponents.DotNetBar.ButtonItem();
            this.btnHelp = new DevComponents.DotNetBar.ButtonItem();
            this.btnThoat = new DevComponents.DotNetBar.ButtonItem();
            this.picBackground = new System.Windows.Forms.PictureBox();
            this.panelEx1 = new DevComponents.DotNetBar.PanelEx();
            this.lbDangNhap = new DevComponents.DotNetBar.LabelX();
            this.pnThaoTac = new DevComponents.DotNetBar.PanelEx();
            this.pnMenu.SuspendLayout();
            this.frm_ribbon.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBackground)).BeginInit();
            this.panelEx1.SuspendLayout();
            this.SuspendLayout();
            // 
            // styleManager1
            // 
            this.styleManager1.ManagerStyle = DevComponents.DotNetBar.eStyle.Office2010Blue;
            this.styleManager1.MetroColorParameters = new DevComponents.DotNetBar.Metro.ColorTables.MetroColorGeneratorParameters(System.Drawing.Color.White, System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(163)))), ((int)(((byte)(26))))));
            // 
            // ribTenNguoiDung
            // 
            this.ribTenNguoiDung.Checked = true;
            this.ribTenNguoiDung.Name = "ribTenNguoiDung";
            this.ribTenNguoiDung.Panel = this.pnMenu;
            this.ribTenNguoiDung.Text = "Nghiệp vụ";
            // 
            // pnMenu
            // 
            this.pnMenu.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.pnMenu.Controls.Add(this.rib_QuanLy);
            this.pnMenu.Controls.Add(this.rib_BanHang);
            this.pnMenu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnMenu.Location = new System.Drawing.Point(0, 0);
            this.pnMenu.Name = "pnMenu";
            this.pnMenu.Padding = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.pnMenu.Size = new System.Drawing.Size(1338, 208);
            // 
            // 
            // 
            this.pnMenu.Style.Class = "";
            this.pnMenu.Style.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.pnMenu.StyleMouseDown.Class = "";
            this.pnMenu.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.pnMenu.StyleMouseOver.Class = "";
            this.pnMenu.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.pnMenu.TabIndex = 1;
            // 
            // rib_QuanLy
            // 
            this.rib_QuanLy.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.rib_QuanLy.BackgroundMouseOverStyle.Class = "";
            this.rib_QuanLy.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.rib_QuanLy.BackgroundStyle.Class = "";
            this.rib_QuanLy.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.rib_QuanLy.ContainerControlProcessDialogKey = true;
            this.rib_QuanLy.Dock = System.Windows.Forms.DockStyle.Left;
            this.rib_QuanLy.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnQuanLyBan,
            this.btnQuanLyMonAn});
            this.rib_QuanLy.Location = new System.Drawing.Point(139, 0);
            this.rib_QuanLy.Name = "rib_QuanLy";
            this.rib_QuanLy.Size = new System.Drawing.Size(253, 205);
            this.rib_QuanLy.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.rib_QuanLy.TabIndex = 1;
            this.rib_QuanLy.Text = "Quản lý";
            // 
            // 
            // 
            this.rib_QuanLy.TitleStyle.Class = "";
            this.rib_QuanLy.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.rib_QuanLy.TitleStyleMouseOver.Class = "";
            this.rib_QuanLy.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // btnQuanLyBan
            // 
            this.btnQuanLyBan.BeginGroup = true;
            this.btnQuanLyBan.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnQuanLyBan.FixedSize = new System.Drawing.Size(120, 120);
            this.btnQuanLyBan.Name = "btnQuanLyBan";
            this.btnQuanLyBan.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(20);
            this.btnQuanLyBan.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.F2);
            this.btnQuanLyBan.SubItemsExpandWidth = 14;
            this.btnQuanLyBan.Text = "Quan ly ban";
            this.btnQuanLyBan.Click += new System.EventHandler(this.btnQuanLyBan_Click);
            // 
            // btnQuanLyMonAn
            // 
            this.btnQuanLyMonAn.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnQuanLyMonAn.FixedSize = new System.Drawing.Size(120, 120);
            this.btnQuanLyMonAn.Name = "btnQuanLyMonAn";
            this.btnQuanLyMonAn.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(20);
            this.btnQuanLyMonAn.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.F3);
            this.btnQuanLyMonAn.SubItemsExpandWidth = 14;
            this.btnQuanLyMonAn.Text = "Thuc don";
            this.btnQuanLyMonAn.Click += new System.EventHandler(this.btnQuanLyMonAn_Click);
            // 
            // rib_BanHang
            // 
            this.rib_BanHang.AutoOverflowEnabled = true;
            // 
            // 
            // 
            this.rib_BanHang.BackgroundMouseOverStyle.Class = "";
            this.rib_BanHang.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.rib_BanHang.BackgroundStyle.Class = "";
            this.rib_BanHang.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.rib_BanHang.ContainerControlProcessDialogKey = true;
            this.rib_BanHang.Dock = System.Windows.Forms.DockStyle.Left;
            this.rib_BanHang.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnQuanLyQuan});
            this.rib_BanHang.Location = new System.Drawing.Point(3, 0);
            this.rib_BanHang.Name = "rib_BanHang";
            this.rib_BanHang.Size = new System.Drawing.Size(136, 205);
            this.rib_BanHang.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.rib_BanHang.TabIndex = 0;
            this.rib_BanHang.Text = "BÁN HÀNG";
            // 
            // 
            // 
            this.rib_BanHang.TitleStyle.Class = "";
            this.rib_BanHang.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // 
            // 
            this.rib_BanHang.TitleStyleMouseOver.Class = "";
            this.rib_BanHang.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            // 
            // btnQuanLyQuan
            // 
            this.btnQuanLyQuan.BeginGroup = true;
            this.btnQuanLyQuan.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnQuanLyQuan.FixedSize = new System.Drawing.Size(120, 120);
            this.btnQuanLyQuan.FontBold = true;
            this.btnQuanLyQuan.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top;
            this.btnQuanLyQuan.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            this.btnQuanLyQuan.Name = "btnQuanLyQuan";
            this.btnQuanLyQuan.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(20);
            this.btnQuanLyQuan.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.F1);
            this.btnQuanLyQuan.SubItemsExpandWidth = 14;
            this.btnQuanLyQuan.Text = "Quản lý quán";
            this.btnQuanLyQuan.Click += new System.EventHandler(this.btnQuanLyQuan_Click);
            // 
            // qatCustomizeItem1
            // 
            this.qatCustomizeItem1.Name = "qatCustomizeItem1";
            // 
            // frm_ribbon
            // 
            // 
            // 
            // 
            this.frm_ribbon.BackgroundStyle.Class = "";
            this.frm_ribbon.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.frm_ribbon.CaptionVisible = true;
            this.frm_ribbon.Controls.Add(this.pnMenu);
            this.frm_ribbon.Dock = System.Windows.Forms.DockStyle.Top;
            this.frm_ribbon.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.office2007StartButton1,
            this.ribTenNguoiDung});
            this.frm_ribbon.KeyTipsFont = new System.Drawing.Font("Tahoma", 7F);
            this.frm_ribbon.Location = new System.Drawing.Point(5, 1);
            this.frm_ribbon.Name = "frm_ribbon";
            this.frm_ribbon.Padding = new System.Windows.Forms.Padding(0, 0, 0, 2);
            this.frm_ribbon.QuickToolbarItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.qatCustomizeItem1});
            this.frm_ribbon.Size = new System.Drawing.Size(1338, 210);
            this.frm_ribbon.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.frm_ribbon.SystemText.MaximizeRibbonText = "&Maximize the Ribbon";
            this.frm_ribbon.SystemText.MinimizeRibbonText = "Mi&nimize the Ribbon";
            this.frm_ribbon.SystemText.QatAddItemText = "&Add to Quick Access Toolbar";
            this.frm_ribbon.SystemText.QatCustomizeMenuLabel = "<b>Customize Quick Access Toolbar</b>";
            this.frm_ribbon.SystemText.QatCustomizeText = "&Customize Quick Access Toolbar...";
            this.frm_ribbon.SystemText.QatDialogAddButton = "&Add >>";
            this.frm_ribbon.SystemText.QatDialogCancelButton = "Cancel";
            this.frm_ribbon.SystemText.QatDialogCaption = "Customize Quick Access Toolbar";
            this.frm_ribbon.SystemText.QatDialogCategoriesLabel = "&Choose commands from:";
            this.frm_ribbon.SystemText.QatDialogOkButton = "OK";
            this.frm_ribbon.SystemText.QatDialogPlacementCheckbox = "&Place Quick Access Toolbar below the Ribbon";
            this.frm_ribbon.SystemText.QatDialogRemoveButton = "&Remove";
            this.frm_ribbon.SystemText.QatPlaceAboveRibbonText = "&Place Quick Access Toolbar above the Ribbon";
            this.frm_ribbon.SystemText.QatPlaceBelowRibbonText = "&Place Quick Access Toolbar below the Ribbon";
            this.frm_ribbon.SystemText.QatRemoveItemText = "&Remove from Quick Access Toolbar";
            this.frm_ribbon.TabGroupHeight = 14;
            this.frm_ribbon.TabIndex = 1;
            this.frm_ribbon.Text = "ribbonControl1";
            // 
            // office2007StartButton1
            // 
            this.office2007StartButton1.AutoExpandOnClick = true;
            this.office2007StartButton1.CanCustomize = false;
            this.office2007StartButton1.HotTrackingStyle = DevComponents.DotNetBar.eHotTrackingStyle.Image;
            this.office2007StartButton1.Image = ((System.Drawing.Image)(resources.GetObject("office2007StartButton1.Image")));
            this.office2007StartButton1.ImageFixedSize = new System.Drawing.Size(16, 16);
            this.office2007StartButton1.ImagePaddingHorizontal = 0;
            this.office2007StartButton1.ImagePaddingVertical = 0;
            this.office2007StartButton1.Name = "office2007StartButton1";
            this.office2007StartButton1.ShowSubItems = false;
            this.office2007StartButton1.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.itemMenu});
            this.office2007StartButton1.Text = "&File";
            // 
            // itemMenu
            // 
            // 
            // 
            // 
            this.itemMenu.BackgroundStyle.Class = "RibbonFileMenuContainer";
            this.itemMenu.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.itemMenu.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical;
            this.itemMenu.Name = "itemMenu";
            this.itemMenu.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnQuanLyTaiKhoan,
            this.btnDoiMatKhau,
            this.btnThongKe,
            this.btnDoiAnhNen,
            this.btnTrangIn,
            this.btnDatabase,
            this.btnLichSu,
            this.btnHelp,
            this.btnThoat});
            // 
            // btnQuanLyTaiKhoan
            // 
            this.btnQuanLyTaiKhoan.BeginGroup = true;
            this.btnQuanLyTaiKhoan.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnQuanLyTaiKhoan.Image = ((System.Drawing.Image)(resources.GetObject("btnQuanLyTaiKhoan.Image")));
            this.btnQuanLyTaiKhoan.Name = "btnQuanLyTaiKhoan";
            this.btnQuanLyTaiKhoan.SubItemsExpandWidth = 24;
            this.btnQuanLyTaiKhoan.Text = "Tài Khoản";
            this.btnQuanLyTaiKhoan.Click += new System.EventHandler(this.btnQuanLyTaiKhoan_Click);
            // 
            // btnDoiMatKhau
            // 
            this.btnDoiMatKhau.BeginGroup = true;
            this.btnDoiMatKhau.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnDoiMatKhau.Image = ((System.Drawing.Image)(resources.GetObject("btnDoiMatKhau.Image")));
            this.btnDoiMatKhau.Name = "btnDoiMatKhau";
            this.btnDoiMatKhau.SubItemsExpandWidth = 24;
            this.btnDoiMatKhau.Text = "Đổi Mật Khẩu";
            this.btnDoiMatKhau.Click += new System.EventHandler(this.btnDoiMatKhau_Click);
            // 
            // btnThongKe
            // 
            this.btnThongKe.BeginGroup = true;
            this.btnThongKe.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnThongKe.Image = ((System.Drawing.Image)(resources.GetObject("btnThongKe.Image")));
            this.btnThongKe.Name = "btnThongKe";
            this.btnThongKe.SubItemsExpandWidth = 24;
            this.btnThongKe.Text = "Thống Kê";
            this.btnThongKe.Click += new System.EventHandler(this.btnThongKe_Click);
            // 
            // btnDoiAnhNen
            // 
            this.btnDoiAnhNen.BeginGroup = true;
            this.btnDoiAnhNen.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnDoiAnhNen.Image = ((System.Drawing.Image)(resources.GetObject("btnDoiAnhNen.Image")));
            this.btnDoiAnhNen.Name = "btnDoiAnhNen";
            this.btnDoiAnhNen.SubItemsExpandWidth = 24;
            this.btnDoiAnhNen.Text = "Đổi Ảnh Nền";
            this.btnDoiAnhNen.Click += new System.EventHandler(this.btnDoiAnhNen_Click);
            // 
            // btnTrangIn
            // 
            this.btnTrangIn.BeginGroup = true;
            this.btnTrangIn.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnTrangIn.Image = ((System.Drawing.Image)(resources.GetObject("btnTrangIn.Image")));
            this.btnTrangIn.Name = "btnTrangIn";
            this.btnTrangIn.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.ckCo,
            this.ckKhong});
            this.btnTrangIn.SubItemsExpandWidth = 24;
            this.btnTrangIn.Text = "Xem Trước Khi In";
            // 
            // ckCo
            // 
            this.ckCo.Name = "ckCo";
            this.ckCo.Text = "Có";
            this.ckCo.CheckedChanged += new DevComponents.DotNetBar.CheckBoxChangeEventHandler(this.ckCo_CheckedChanged);
            // 
            // ckKhong
            // 
            this.ckKhong.Name = "ckKhong";
            this.ckKhong.Text = "Không";
            this.ckKhong.CheckedChanged += new DevComponents.DotNetBar.CheckBoxChangeEventHandler(this.ckKhong_CheckedChanged);
            // 
            // btnDatabase
            // 
            this.btnDatabase.BeginGroup = true;
            this.btnDatabase.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnDatabase.Image = ((System.Drawing.Image)(resources.GetObject("btnDatabase.Image")));
            this.btnDatabase.Name = "btnDatabase";
            this.btnDatabase.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.btnBackup,
            this.btnRetore});
            this.btnDatabase.SubItemsExpandWidth = 24;
            this.btnDatabase.Text = "Cơ Sở Dữ Liệu";
            // 
            // btnBackup
            // 
            this.btnBackup.Image = ((System.Drawing.Image)(resources.GetObject("btnBackup.Image")));
            this.btnBackup.Name = "btnBackup";
            this.btnBackup.Text = "Sao lưu dữ liệu";
            this.btnBackup.Click += new System.EventHandler(this.btnBackup_Click);
            // 
            // btnRetore
            // 
            this.btnRetore.Image = ((System.Drawing.Image)(resources.GetObject("btnRetore.Image")));
            this.btnRetore.Name = "btnRetore";
            this.btnRetore.Text = "Phục hồi dữ liệu";
            this.btnRetore.Click += new System.EventHandler(this.btnRetore_Click);
            // 
            // btnLichSu
            // 
            this.btnLichSu.BeginGroup = true;
            this.btnLichSu.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnLichSu.Image = ((System.Drawing.Image)(resources.GetObject("btnLichSu.Image")));
            this.btnLichSu.Name = "btnLichSu";
            this.btnLichSu.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(2);
            this.btnLichSu.SubItemsExpandWidth = 24;
            this.btnLichSu.Text = "Lịch Sử";
            this.btnLichSu.Click += new System.EventHandler(this.btnLichSu_Click);
            // 
            // btnHelp
            // 
            this.btnHelp.BeginGroup = true;
            this.btnHelp.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnHelp.Image = ((System.Drawing.Image)(resources.GetObject("btnHelp.Image")));
            this.btnHelp.Name = "btnHelp";
            this.btnHelp.Shape = new DevComponents.DotNetBar.RoundRectangleShapeDescriptor(2);
            this.btnHelp.SubItemsExpandWidth = 24;
            this.btnHelp.Text = "Hướng dẫn sử dụng";
            this.btnHelp.Click += new System.EventHandler(this.btnHelp_Click);
            // 
            // btnThoat
            // 
            this.btnThoat.BeginGroup = true;
            this.btnThoat.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText;
            this.btnThoat.Icon = ((System.Drawing.Icon)(resources.GetObject("btnThoat.Icon")));
            this.btnThoat.Image = ((System.Drawing.Image)(resources.GetObject("btnThoat.Image")));
            this.btnThoat.Name = "btnThoat";
            this.btnThoat.SubItemsExpandWidth = 24;
            this.btnThoat.Text = "Thoát";
            this.btnThoat.Click += new System.EventHandler(this.btnThoat_Click);
            // 
            // picBackground
            // 
            this.picBackground.Dock = System.Windows.Forms.DockStyle.Fill;
            this.picBackground.Location = new System.Drawing.Point(5, 211);
            this.picBackground.Name = "picBackground";
            this.picBackground.Size = new System.Drawing.Size(1338, 477);
            this.picBackground.TabIndex = 3;
            this.picBackground.TabStop = false;
            // 
            // panelEx1
            // 
            this.panelEx1.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelEx1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.panelEx1.Controls.Add(this.lbDangNhap);
            this.panelEx1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelEx1.Location = new System.Drawing.Point(5, 666);
            this.panelEx1.Name = "panelEx1";
            this.panelEx1.Size = new System.Drawing.Size(1338, 22);
            this.panelEx1.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelEx1.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelEx1.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.panelEx1.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelEx1.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelEx1.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelEx1.Style.GradientAngle = 90;
            this.panelEx1.TabIndex = 5;
            // 
            // lbDangNhap
            // 
            // 
            // 
            // 
            this.lbDangNhap.BackgroundStyle.Class = "";
            this.lbDangNhap.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lbDangNhap.Location = new System.Drawing.Point(24, -2);
            this.lbDangNhap.Name = "lbDangNhap";
            this.lbDangNhap.Size = new System.Drawing.Size(1257, 23);
            this.lbDangNhap.TabIndex = 0;
            this.lbDangNhap.Text = "Đã đăng nhập:";
            // 
            // pnThaoTac
            // 
            this.pnThaoTac.CanvasColor = System.Drawing.SystemColors.Control;
            this.pnThaoTac.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.pnThaoTac.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnThaoTac.Location = new System.Drawing.Point(5, 211);
            this.pnThaoTac.Name = "pnThaoTac";
            this.pnThaoTac.Size = new System.Drawing.Size(1338, 20);
            this.pnThaoTac.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.pnThaoTac.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.pnThaoTac.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.pnThaoTac.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.pnThaoTac.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.pnThaoTac.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.pnThaoTac.Style.GradientAngle = 90;
            this.pnThaoTac.TabIndex = 7;
            this.pnThaoTac.Text = "panelEx2";
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1348, 690);
            this.Controls.Add(this.pnThaoTac);
            this.Controls.Add(this.panelEx1);
            this.Controls.Add(this.picBackground);
            this.Controls.Add(this.frm_ribbon);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.Name = "frmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Quản Lý Quán Cà Phê - 1985";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmMain_FormClosing);
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.pnMenu.ResumeLayout(false);
            this.frm_ribbon.ResumeLayout(false);
            this.frm_ribbon.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBackground)).EndInit();
            this.panelEx1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.StyleManager styleManager1;
        private DevComponents.DotNetBar.RibbonTabItem ribTenNguoiDung;
        private DevComponents.DotNetBar.Office2007StartButton office2007StartButton1;
        private DevComponents.DotNetBar.QatCustomizeItem qatCustomizeItem1;
        private DevComponents.DotNetBar.RibbonControl frm_ribbon;
        private DevComponents.DotNetBar.ItemContainer itemMenu;
        private DevComponents.DotNetBar.ButtonItem btnDoiMatKhau;
        private DevComponents.DotNetBar.ButtonItem btnThongKe;
        private DevComponents.DotNetBar.ButtonItem btnDoiAnhNen;
        private DevComponents.DotNetBar.ButtonItem btnThoat;
        private System.Windows.Forms.PictureBox picBackground;
        private DevComponents.DotNetBar.PanelEx panelEx1;
        private DevComponents.DotNetBar.LabelX lbDangNhap;
        private DevComponents.DotNetBar.RibbonPanel pnMenu;
        private DevComponents.DotNetBar.RibbonBar rib_QuanLy;
        private DevComponents.DotNetBar.ButtonItem btnQuanLyBan;
        private DevComponents.DotNetBar.ButtonItem btnQuanLyMonAn;
        private DevComponents.DotNetBar.RibbonBar rib_BanHang;
        private DevComponents.DotNetBar.ButtonItem btnQuanLyQuan;
        private DevComponents.DotNetBar.ButtonItem btnDatabase;
        private DevComponents.DotNetBar.ButtonItem btnBackup;
        private DevComponents.DotNetBar.ButtonItem btnRetore;
        private DevComponents.DotNetBar.ButtonItem btnTrangIn;
        private DevComponents.DotNetBar.CheckBoxItem ckCo;
        private DevComponents.DotNetBar.CheckBoxItem ckKhong;
        private DevComponents.DotNetBar.PanelEx pnThaoTac;
        private DevComponents.DotNetBar.ButtonItem btnLichSu;
        private DevComponents.DotNetBar.ButtonItem btnQuanLyTaiKhoan;
        private DevComponents.DotNetBar.ButtonItem btnHelp;
        
    }
}