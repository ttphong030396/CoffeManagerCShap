﻿using DevComponents.DotNetBar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GUI
{
    public class Button_LoaiMon : ButtonX
    {
        public int IDLoaiMon { get; set; }
        public string Mode { get; set; }
        public byte ViTri { get; set; }
    }
}
