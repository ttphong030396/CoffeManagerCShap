﻿using DevComponents.DotNetBar;
using DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BUS;
namespace GUI
{
    public partial class frmMain : Office2007RibbonForm
    {
        #region Properties
        List<ButtonItem> lstMenu = new List<ButtonItem>();
        frmQuanLyQuan frmQuan;
        frmQuanLyBan frmBan;
        frmQuanLyMonAn frmMonAn;
        frmThongKe frmThongKe;
        frmQuanLyTaiKhoan frmTaiKhoan;
        frmXemLichSu frmLichSu;
        private static DevComponents.DotNetBar.Controls.ListViewEx lstLickSu = new DevComponents.DotNetBar.Controls.ListViewEx();
        public static DevComponents.DotNetBar.Controls.ListViewEx LstLickSu
        {
            get { if (lstLickSu == null)lstLickSu = new DevComponents.DotNetBar.Controls.ListViewEx(); return frmMain.lstLickSu; }
            set { if (lstLickSu == null)lstLickSu = new DevComponents.DotNetBar.Controls.ListViewEx(); frmMain.lstLickSu = value; }
        }

        #endregion
        #region Method
        public frmMain()
        {
            InitializeComponent();
            pnMenu.Controls.Add(lstLickSu);
            TaoListView();
            PhanQuyenVaHienThiNguoiDung();
            picBackground.SizeMode = PictureBoxSizeMode.StretchImage;
            LstLickSu.Columns.Add("Người dùng", 103);
            LstLickSu.Columns.Add("Thao tác", 412);
            LstLickSu.Columns.Add("Thời Gian", 135);
            LoadLichSu();
        }
        public static void LoadLichSu()
        {
            DataTable dt = BUS_Database.Instance.LayLichSu(frmDangNhap.Taikhoan.TaiKhoan);
            LstLickSu.Items.Clear();
            if(dt!=null)
            {
                foreach (DataRow item in dt.Rows)
                {
                    ListViewItem lst = new ListViewItem(item[1].ToString());
                    string sub = item[2].ToString();
                    lst.SubItems.Add(item[2].ToString());
                    DateTime time = DateTime.Parse(item[3].ToString());
                    string times = time.Hour.ToString() + ":" + time.Minute.ToString() + "  " + time.Day.ToString() + "-" + time.Month.ToString() + "-" + time.Year.ToString();
                    lst.SubItems.Add(times);
                    LstLickSu.Items.Add(lst);
                }
                LstLickSu.Items[0].Selected = true;
            }
        }
        void TaoListView()
        {
            LstLickSu.Border.Class = "ListViewBorder";
            LstLickSu.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            LstLickSu.Dock = System.Windows.Forms.DockStyle.Right;
            LstLickSu.Location = new System.Drawing.Point(685, 0);
            LstLickSu.MultiSelect = false;
            LstLickSu.FullRowSelect = true;
            LstLickSu.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            LstLickSu.Name = "lstLickSu";
            LstLickSu.Size = new System.Drawing.Size(650, 205);
            LstLickSu.TabIndex = 2;
            LstLickSu.SelectedIndexChanged += LstLickSu_SelectedIndexChanged;
            LstLickSu.UseCompatibleStateImageBehavior = false;
            LstLickSu.View = System.Windows.Forms.View.Details;
        }
        void LstLickSu_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ListViewItem lst = new ListViewItem();
                lst = LstLickSu.SelectedItems[0];
                pnThaoTac.Text = lst.SubItems[1].Text;
            }
            catch (Exception)
            {
                
            }
        }
        private void PhanQuyenVaHienThiNguoiDung()
        {
            ribTenNguoiDung.Text = frmDangNhap.Taikhoan.TenHienThi;
            lbDangNhap.Text = "Đăng nhập bởi " + frmDangNhap.Taikhoan.TaiKhoan + " Vào lúc: " + DateTime.Now.ToString();
            if (frmDangNhap.Taikhoan.Loai == 1)//admin
            {
                btnQuanLyTaiKhoan.Visible = true;
                btnThongKe.Visible = true;
                rib_QuanLy.Visible = true;
                btnLichSu.Visible = true;
                btnDatabase.Visible = true;
                btnLichSu.Visible = true;
            }
            else//staff
            {
                btnQuanLyTaiKhoan.Visible = false;
                btnThongKe.Visible = false;
                rib_QuanLy.Visible = false;
                btnLichSu.Visible = false;
                btnDatabase.Visible = false;
                btnLichSu.Visible = false;
            }
        }
        private void CauHinh()
        {
            try
            {
                btnQuanLyQuan.Image = Properties.Resources.frmMain_btnQuanLyQuan;
                btnQuanLyQuan.Text = "";
                lstMenu.Add(btnQuanLyQuan);
                btnQuanLyBan.Image = Properties.Resources.frmMain_btnQuanLyBan;
                btnQuanLyBan.Text = "";
                lstMenu.Add(btnQuanLyBan);
                btnQuanLyMonAn.Image = Properties.Resources.frmMain_btnQuanLyMonAn;
                btnQuanLyMonAn.Text = "";
                lstMenu.Add(btnQuanLyMonAn);
                if (Properties.Settings.Default.isXemTruocIn)
                    ckCo.Checked = true;
                else
                    ckKhong.Checked = true;

                try
                {
                    picBackground.Image = Image.FromFile(Properties.Settings.Default.pathBackground);
                }
                catch
                {
                    if(MessageBox.Show("Chưa có ảnh nền. Bạn có muốn chọn ảnh nền cho ứng dụng??","Thông báo",MessageBoxButtons.YesNo,MessageBoxIcon.Question)==DialogResult.Yes)
                    LoadBackground();
                }
            }
            catch (Exception)
            {
            }
        }
        private void LoadBackground()
        {
            try
            {
                OpenFileDialog open = new OpenFileDialog();
                open.Title = "Chọn ảnh";
                open.Filter = "JPeg Image|*.jpg|PNG|*.png|All file|*.*";
                if (open.ShowDialog() == DialogResult.OK)
                {
                    string directoryPath = "Background";
                    if (!System.IO.Directory.Exists(directoryPath))
                        System.IO.Directory.CreateDirectory(directoryPath);
                    string path = directoryPath + @"/" + System.IO.Path.GetFileName(open.FileName);
                    if (!System.IO.File.Exists(path))
                    {
                        System.IO.File.Copy(open.FileName, path);
                    }
                    picBackground.Image = Image.FromFile(path);
                    Properties.Settings.Default.pathBackground = path;
                    Properties.Settings.Default.Save();
                }
            }
            catch (Exception)
            {

            }
        }
        #endregion
        #region Event
        private void btnQuanLyQuan_Click(object sender, EventArgs e)
        {

            ButtonItem btn = new ButtonItem();
            btn = sender as ButtonItem;
            if (!btn.Checked)
            {
                try
                {
                    foreach (Form frm in MdiChildren)
                        frm.Close();
                    foreach (ButtonItem btn1 in lstMenu)
                        btn1.Checked = false;
                    btn.Checked = true;
                    frmQuan = new frmQuanLyQuan();
                    frmQuan.MdiParent = this;
                    frmQuan.Dock = DockStyle.Fill;
                    picBackground.Hide();
                    frmQuan.Show();
                }
                catch (Exception)
                {
                }
                
            }

        }
        private void btnQuanLyBan_Click(object sender, EventArgs e)
        {
            try
            {
                ButtonItem btn = new ButtonItem();
                btn = sender as ButtonItem;
                if (!btn.Checked)
                {
                    foreach (Form frm in MdiChildren)
                        frm.Close();
                    foreach (ButtonItem btn1 in lstMenu)
                        btn1.Checked = false;
                    btn.Checked = true;
                    frmBan = new frmQuanLyBan();
                    frmBan.MdiParent = this;
                    picBackground.Hide();
                    frmBan.Dock = DockStyle.Fill;
                    frmBan.Show();
                }
            }
            catch (Exception)
            {
            }
        }
        private void btnQuanLyMonAn_Click(object sender, EventArgs e)
        {
            try
            {
                ButtonItem btn = new ButtonItem();
                btn = sender as ButtonItem;
                if (!btn.Checked)
                {
                    foreach (Form frm in MdiChildren)
                        frm.Close();
                    foreach (ButtonItem btn1 in lstMenu)
                        btn1.Checked = false;
                    btn.Checked = true;
                    frmMonAn = new frmQuanLyMonAn();
                    frmMonAn.MdiParent = this;
                    picBackground.Hide();
                    frmMonAn.Dock = DockStyle.Fill;
                    frmMonAn.Show();
                }
            }
            catch (Exception)
            {
            }
        }
        private void frmMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (MessageBox.Show("Bạn chắc chắc muốn kết thúc chương trình", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                e.Cancel = true;
        }
        private void btnThoat_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void btnThongKe_Click(object sender, EventArgs e)
        {
            ButtonItem btn = new ButtonItem();
            btn = sender as ButtonItem;
            if (!btn.Checked)
            {
                foreach (Form frm in MdiChildren)
                    frm.Close();
                foreach (ButtonItem btn1 in lstMenu)
                    btn1.Checked = false;
                picBackground.Hide();
                frmThongKe = new frmThongKe();
                frmThongKe.MdiParent = this;
                frmThongKe.Dock = DockStyle.Fill;
                frmThongKe.Show();
            }
        }
        private void btnQuanLyTaiKhoan_Click(object sender, EventArgs e)
        {
                frmTaiKhoan = new frmQuanLyTaiKhoan();
                frmTaiKhoan.ShowDialog();
        }
        private void btnDoiMatKhau_Click(object sender, EventArgs e)
        {
            frmDoiMatKhau frmDoiMK = new frmDoiMatKhau();
            frmDoiMK.ShowDialog();
        }
        private void btnDoiAnhNen_Click(object sender, EventArgs e)
        {
            LoadBackground();
        }
        private void frmMain_Load(object sender, EventArgs e)
        {
            CauHinh();
        }
        private void btnBackup_Click(object sender, EventArgs e)
        {
            SaveFileDialog save = new SaveFileDialog();
            save.Filter = "File .bak|*.bak";
            save.FileName = "DB_1985_" + DateTime.Now.Day.ToString() + "_" + DateTime.Now.Month.ToString() + "_" + DateTime.Now.Year.ToString();
            if(save.ShowDialog()==DialogResult.OK)
            {
                if (BUS_Database.Instance.SaoLuu(save.FileName))
                    MessageBox.Show("Sao lưu thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                else
                    MessageBox.Show("Không thể sao lưu file ở ổ C.Vui lòng chọn nơi lưu khác","Lỗi sao lưu",MessageBoxButtons.OK,MessageBoxIcon.Warning);
            }
        }
        private void btnRetore_Click(object sender, EventArgs e)
        {
            OpenFileDialog open = new OpenFileDialog();
            open.Filter = "File .bak|*.bak";
            if (open.ShowDialog() == DialogResult.OK)
            {
                if (BUS_Database.Instance.PhucHoi(open.FileName))
                {
                    MessageBox.Show("Phục hồi thành công Vui khởi động lại ứng dụng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                }
                else
                    MessageBox.Show("Không thể phục hồi", "Lỗi phục hồi", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        

        private void ckCo_CheckedChanged(object sender, CheckBoxChangeEventArgs e)
        {
            Properties.Settings.Default.isXemTruocIn = ckCo.Checked;
            Properties.Settings.Default.Save();
            ckKhong.Checked = !ckCo.Checked;
        }
        private void ckKhong_CheckedChanged(object sender, CheckBoxChangeEventArgs e)
        {
            ckCo.Checked = !ckKhong.Checked;
        }
        private void btnLichSu_Click(object sender, EventArgs e)
        {
            frmLichSu = new frmXemLichSu();
            frmLichSu.ShowDialog();
        }

        private void btnHelp_Click(object sender, EventArgs e)
        {
            Help.ShowHelp(this, @"help.chm");

        }
        #endregion
    }
}