﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BUS;
using DTO;
namespace GUI
{
    public partial class frmDangNhap : DevComponents.DotNetBar.Office2007RibbonForm
    {
        ErrorProvider err = new ErrorProvider();
        static DTO_TaiKhoan taikhoan;

        public static DTO_TaiKhoan Taikhoan
        {
            get { if (taikhoan == null) taikhoan = new DTO_TaiKhoan(); return frmDangNhap.taikhoan; }
            set { if (taikhoan == null) taikhoan = new DTO_TaiKhoan(); frmDangNhap.taikhoan = value; }
        }
        public frmDangNhap()
        {
            InitializeComponent();
        }

        private void btnThoat_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
        private void btnDangNhap_Click(object sender, EventArgs e)
        {
            if(txtTaiKhoan.Text.Trim()=="")
            {
                err.SetError(txtTaiKhoan, "CHƯA NHẬP TÀI KHOẢN");
                txtTaiKhoan.Focus();
                return;
            }
            if(txtMatKhau.Text.Trim()=="")
            {
                err.SetError(txtMatKhau, "CHƯA NHẬP MẬT KHẨU");
                txtMatKhau.Focus();
                return;
            }
            if(BUS_TaiKhoan.Instance.Get(txtTaiKhoan.Text.Trim(),txtMatKhau.Text.Trim())==null)
            {
                MessageBox.Show("Tài khoản hoặc mật khẩu không đúng. Vui lòng nhập lại!", "Lỗi đăng nhập", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtTaiKhoan.Clear();
                txtMatKhau.Clear();
                txtTaiKhoan.Focus();
            }
            else
            {
                Taikhoan = BUS_TaiKhoan.Instance.Get(txtTaiKhoan.Text.Trim(), txtMatKhau.Text.Trim());
                BUS_Database.Instance.GhiLichSu(Taikhoan.TaiKhoan, "Đăng nhập", DateTime.Now);
                frmMain.LoadLichSu();
                frmMain frm = new frmMain();
                this.Hide();
                txtTaiKhoan.Clear();
                txtMatKhau.Clear();
                frmWaiting frmw = new frmWaiting(true);
                frmw.ShowDialog();
                frm.ShowDialog();
                this.Show();
                txtTaiKhoan.Focus();
            }
            
        }
        private void txtTaiKhoan_TextChanged(object sender, EventArgs e)
        {
            err.SetError(txtTaiKhoan, "");
        }
        private void txtMatKhau_TextChanged(object sender, EventArgs e)
        {
            err.SetError(txtMatKhau, "");
        }
    }
}
