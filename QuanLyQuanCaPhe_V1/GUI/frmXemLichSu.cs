﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DTO;
using BUS;
namespace GUI
{
    public partial class frmXemLichSu : DevComponents.DotNetBar.Office2007RibbonForm
    {
        public frmXemLichSu()
        {
            InitializeComponent();
            CauHinh();
        }
        private void CauHinh()
        {
            cbbLoaiTK.Enabled = false;
            ckLoc.Checked = false;
            cbbLoaiTK.DataSource = BUS_TaiKhoan.Instance.GetAll();
            cbbLoaiTK.DisplayMember = "TenHienThi";
            cbbLoaiTK.ValueMember = "TaiKhoan";
            
            LoadDGV(ckLoc.Checked);
        }
        private void ckLoc_CheckedChanged(object sender, EventArgs e)
        {
            cbbLoaiTK.Enabled = ckLoc.Checked;
            if (ckLoc.Checked)
            {
                cbbLoaiTK.Enabled = true;
                LoadDGV(ckLoc.Checked);
            }
            else
            {
                cbbLoaiTK.Enabled = false;
                LoadDGV(ckLoc.Checked);
            }
        }
        private void LoadDGV(bool Nofill)
        {
            try
            {
                switch (Nofill)
            {
                case false: { 
                    dgvLichSu.DataSource = BUS_Database.Instance.LayTatCaLichSu();
                    dgvLichSu.Columns[0].Visible = false;
                    dgvLichSu.Columns[1].HeaderText = "Tên tài khoản";
                    dgvLichSu.Columns[2].HeaderText = "Thao tác";
                    dgvLichSu.Columns[3].HeaderText = "Thời gian";
                    dgvLichSu.Columns[1].Width = 140;
                    dgvLichSu.Columns[2].Width = 640;
                    dgvLichSu.Columns[3].Width = 160;
                    break;
                }
                case true: {
                    dgvLichSu.DataSource = BUS_Database.Instance.LayTatCaLichSu(cbbLoaiTK.SelectedValue.ToString());
                    dgvLichSu.Columns[0].Visible = false;
                    dgvLichSu.Columns[1].HeaderText = "Tên tài khoản";
                    dgvLichSu.Columns[2].HeaderText = "Thao tác";
                    dgvLichSu.Columns[3].HeaderText = "Thời gian";
                    dgvLichSu.Columns[1].Width = 140;
                    dgvLichSu.Columns[2].Width = 640;
                    dgvLichSu.Columns[3].Width = 160;
                    break;
                }
            }
            }
            catch (Exception)
            {
            }
        }

        private void cbbLoaiTK_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadDGV(ckLoc.Checked);
        }
        private void btnThoat_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dgvLichSu_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                DataGridViewRow row = new DataGridViewRow();
                row = dgvLichSu.CurrentRow;
                txtThaoTac.Text = row.Cells[2].Value.ToString();
            }
            catch (Exception)
            {
                
            }
        }

    }
}
