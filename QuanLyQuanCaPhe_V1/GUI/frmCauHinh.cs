﻿using BUS;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Sql;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
namespace GUI
{
    public partial class frmCauHinh : Form
    {
        ErrorProvider er = new ErrorProvider();
        public frmCauHinh()
        {
            Thread t = new Thread(new ThreadStart(FlashCreen));
            t.Start();
            InitializeComponent();
            Setting();
            t.Abort();
        }
        void FlashCreen()
        {
            Application.Run(new frmWaiting());
        }
        private void Setting()
        {
            try
            {
                cbbMayChu.DataSource = GetNameServer();
                cbbMayChu.DisplayMember = "ServerName";
                balloonTip1.SetBalloonCaption(cbbMayChu, "Tên máy chủ chứa cơ sở dữ liệu");
                balloonTip1.SetBalloonCaption(txtTaiKhoan, "Tài khoản đăng nhập cơ sở dữ liệu");
                balloonTip1.SetBalloonCaption(txtMatKhau, "Mật khẩu đăng nhập cơ sở dữ liệu");
            }
            catch (Exception)
            {
            }
        }
        private DataTable GetNameServer()
        {
            try
            {
                SqlDataSourceEnumerator ins = SqlDataSourceEnumerator.Instance;
                DataTable dt = ins.GetDataSources();
                return dt;
            }
            catch (Exception)
            {
                return null;
            }
        }
        private void btnThoat_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
        private void btnXacNhan_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtTaiKhoan.Text == "")
                {
                    er.SetError(txtTaiKhoan, "Bạn chưa nhập tài khoản");
                    txtTaiKhoan.Focus();
                    return;
                }
                if (txtMatKhau.Text == "")
                {
                    er.SetError(txtMatKhau, "Bạn chưa nhập mật khẩu");
                    txtMatKhau.Focus();
                    return;
                }
                string cn = @"Server=" + cbbMayChu.Text + @";Database=QuanLyQuanCaPhe_1985;User Id=" + txtTaiKhoan.Text + @";
Password=" + txtMatKhau.Text + @";";
                if (Connection_BUS.Instance.TestConnection(cn))
                {
                    MessageBox.Show("Cấu hình thành công. Vui lòng khỏi động lại ứng dụng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                    GUI.Properties.Settings.Default.StrConnection = cn;
                    GUI.Properties.Settings.Default.Save();
                    Application.Exit();
                }
                else
                {
                    MessageBox.Show("Cấu hình thất bại. Vui lòng kiểm tra lại cơ sở dữ liệu", "Lỗi !!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtMatKhau.Clear();
                    txtTaiKhoan.Clear();
                    txtTaiKhoan.Focus();
                }
            }
            catch (Exception)
            {

            }
        }
        private void txtTaiKhoan_TextChanged(object sender, EventArgs e)
        {
            er.SetError(txtTaiKhoan, "");
        }
        private void txtMatKhau_TextChanged(object sender, EventArgs e)
        {
            er.SetError(txtMatKhau, "");
        }
    }
}
