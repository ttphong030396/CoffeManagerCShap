﻿namespace GUI
{
    partial class frmThongKe
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource2 = new Microsoft.Reporting.WinForms.ReportDataSource();
            this.DTO_ReportHoaDonBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.DTO_ReportHoaDonChiTietBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.panelEx1 = new DevComponents.DotNetBar.PanelEx();
            this.panelEx2 = new DevComponents.DotNetBar.PanelEx();
            this.btnTuyChon = new DevComponents.DotNetBar.ButtonX();
            this.btnThangNay = new DevComponents.DotNetBar.ButtonX();
            this.btnHomNay = new DevComponents.DotNetBar.ButtonX();
            this.panelEx3 = new DevComponents.DotNetBar.PanelEx();
            this.rpHoaDon = new Microsoft.Reporting.WinForms.ReportViewer();
            this.pnChon = new DevComponents.DotNetBar.PanelEx();
            this.lbTuyChon = new DevComponents.DotNetBar.LabelX();
            this.dtpTuyChon = new DevComponents.Editors.DateTimeAdv.DateTimeInput();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.dtpNgayDau = new DevComponents.Editors.DateTimeAdv.DateTimeInput();
            ((System.ComponentModel.ISupportInitialize)(this.DTO_ReportHoaDonBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DTO_ReportHoaDonChiTietBindingSource)).BeginInit();
            this.panelEx2.SuspendLayout();
            this.panelEx3.SuspendLayout();
            this.pnChon.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtpTuyChon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgayDau)).BeginInit();
            this.SuspendLayout();
            // 
            // DTO_ReportHoaDonBindingSource
            // 
            this.DTO_ReportHoaDonBindingSource.DataSource = typeof(DTO.DTO_ReportHoaDon);
            // 
            // DTO_ReportHoaDonChiTietBindingSource
            // 
            this.DTO_ReportHoaDonChiTietBindingSource.DataSource = typeof(DTO.DTO_ReportHoaDonChiTiet);
            // 
            // panelEx1
            // 
            this.panelEx1.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelEx1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.panelEx1.Location = new System.Drawing.Point(-15, -15);
            this.panelEx1.Name = "panelEx1";
            this.panelEx1.Size = new System.Drawing.Size(200, 100);
            this.panelEx1.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelEx1.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelEx1.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.panelEx1.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelEx1.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelEx1.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelEx1.Style.GradientAngle = 90;
            this.panelEx1.TabIndex = 0;
            this.panelEx1.Text = "panelEx1";
            // 
            // panelEx2
            // 
            this.panelEx2.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelEx2.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.panelEx2.Controls.Add(this.btnTuyChon);
            this.panelEx2.Controls.Add(this.btnThangNay);
            this.panelEx2.Controls.Add(this.btnHomNay);
            this.panelEx2.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelEx2.Location = new System.Drawing.Point(0, 0);
            this.panelEx2.Name = "panelEx2";
            this.panelEx2.Size = new System.Drawing.Size(271, 531);
            this.panelEx2.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelEx2.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelEx2.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.panelEx2.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelEx2.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelEx2.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelEx2.Style.GradientAngle = 90;
            this.panelEx2.TabIndex = 1;
            // 
            // btnTuyChon
            // 
            this.btnTuyChon.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnTuyChon.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnTuyChon.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnTuyChon.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTuyChon.Location = new System.Drawing.Point(0, 184);
            this.btnTuyChon.Name = "btnTuyChon";
            this.btnTuyChon.Size = new System.Drawing.Size(271, 92);
            this.btnTuyChon.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnTuyChon.TabIndex = 2;
            this.btnTuyChon.Text = "TÙY CHỌN";
            this.btnTuyChon.Click += new System.EventHandler(this.btnTuyChon_Click);
            // 
            // btnThangNay
            // 
            this.btnThangNay.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnThangNay.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnThangNay.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnThangNay.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnThangNay.Location = new System.Drawing.Point(0, 92);
            this.btnThangNay.Name = "btnThangNay";
            this.btnThangNay.Size = new System.Drawing.Size(271, 92);
            this.btnThangNay.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnThangNay.TabIndex = 1;
            this.btnThangNay.Text = "THÁNG NÀY";
            this.btnThangNay.Click += new System.EventHandler(this.btnThangNay_Click);
            // 
            // btnHomNay
            // 
            this.btnHomNay.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnHomNay.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnHomNay.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnHomNay.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHomNay.Location = new System.Drawing.Point(0, 0);
            this.btnHomNay.Name = "btnHomNay";
            this.btnHomNay.Size = new System.Drawing.Size(271, 92);
            this.btnHomNay.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnHomNay.TabIndex = 0;
            this.btnHomNay.Text = "HÔM NAY";
            this.btnHomNay.Click += new System.EventHandler(this.btnHomNay_Click);
            // 
            // panelEx3
            // 
            this.panelEx3.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelEx3.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.panelEx3.Controls.Add(this.rpHoaDon);
            this.panelEx3.Controls.Add(this.pnChon);
            this.panelEx3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelEx3.Location = new System.Drawing.Point(271, 0);
            this.panelEx3.Name = "panelEx3";
            this.panelEx3.Size = new System.Drawing.Size(1099, 531);
            this.panelEx3.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelEx3.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelEx3.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.panelEx3.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelEx3.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelEx3.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelEx3.Style.GradientAngle = 90;
            this.panelEx3.TabIndex = 2;
            this.panelEx3.Text = "panelEx3";
            // 
            // rpHoaDon
            // 
            this.rpHoaDon.Dock = System.Windows.Forms.DockStyle.Fill;
            reportDataSource1.Name = "DataSet1";
            reportDataSource1.Value = this.DTO_ReportHoaDonBindingSource;
            reportDataSource2.Name = "DataSet2";
            reportDataSource2.Value = this.DTO_ReportHoaDonChiTietBindingSource;
            this.rpHoaDon.LocalReport.DataSources.Add(reportDataSource1);
            this.rpHoaDon.LocalReport.DataSources.Add(reportDataSource2);
            this.rpHoaDon.LocalReport.ReportEmbeddedResource = "GUI.Report1.rdlc";
            this.rpHoaDon.Location = new System.Drawing.Point(0, 50);
            this.rpHoaDon.Name = "rpHoaDon";
            this.rpHoaDon.ShowPrintButton = false;
            this.rpHoaDon.Size = new System.Drawing.Size(1099, 481);
            this.rpHoaDon.TabIndex = 2;
            // 
            // pnChon
            // 
            this.pnChon.CanvasColor = System.Drawing.SystemColors.Control;
            this.pnChon.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.pnChon.Controls.Add(this.lbTuyChon);
            this.pnChon.Controls.Add(this.dtpTuyChon);
            this.pnChon.Controls.Add(this.labelX1);
            this.pnChon.Controls.Add(this.dtpNgayDau);
            this.pnChon.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnChon.Location = new System.Drawing.Point(0, 0);
            this.pnChon.Name = "pnChon";
            this.pnChon.Size = new System.Drawing.Size(1099, 50);
            this.pnChon.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.pnChon.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.pnChon.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.pnChon.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.pnChon.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.pnChon.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.pnChon.Style.GradientAngle = 90;
            this.pnChon.TabIndex = 1;
            // 
            // lbTuyChon
            // 
            // 
            // 
            // 
            this.lbTuyChon.BackgroundStyle.Class = "";
            this.lbTuyChon.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lbTuyChon.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTuyChon.Location = new System.Drawing.Point(276, 6);
            this.lbTuyChon.Name = "lbTuyChon";
            this.lbTuyChon.Size = new System.Drawing.Size(58, 36);
            this.lbTuyChon.TabIndex = 3;
            this.lbTuyChon.Text = "ĐẾN";
            // 
            // dtpTuyChon
            // 
            // 
            // 
            // 
            this.dtpTuyChon.BackgroundStyle.Class = "DateTimeInputBackground";
            this.dtpTuyChon.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.dtpTuyChon.ButtonDropDown.Shortcut = DevComponents.DotNetBar.eShortcut.AltDown;
            this.dtpTuyChon.ButtonDropDown.Visible = true;
            this.dtpTuyChon.CustomFormat = "dd/MM/yyyy";
            this.dtpTuyChon.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpTuyChon.Format = DevComponents.Editors.eDateTimePickerFormat.Custom;
            this.dtpTuyChon.IsPopupCalendarOpen = false;
            this.dtpTuyChon.Location = new System.Drawing.Point(340, 6);
            this.dtpTuyChon.MaxDate = new System.DateTime(2100, 12, 31, 0, 0, 0, 0);
            this.dtpTuyChon.MinDate = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            // 
            // 
            // 
            this.dtpTuyChon.MonthCalendar.AnnuallyMarkedDates = new System.DateTime[0];
            // 
            // 
            // 
            this.dtpTuyChon.MonthCalendar.BackgroundStyle.BackColor = System.Drawing.SystemColors.Window;
            this.dtpTuyChon.MonthCalendar.BackgroundStyle.Class = "";
            this.dtpTuyChon.MonthCalendar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.dtpTuyChon.MonthCalendar.ClearButtonVisible = true;
            // 
            // 
            // 
            this.dtpTuyChon.MonthCalendar.CommandsBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2;
            this.dtpTuyChon.MonthCalendar.CommandsBackgroundStyle.BackColorGradientAngle = 90;
            this.dtpTuyChon.MonthCalendar.CommandsBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.dtpTuyChon.MonthCalendar.CommandsBackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.dtpTuyChon.MonthCalendar.CommandsBackgroundStyle.BorderTopColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder;
            this.dtpTuyChon.MonthCalendar.CommandsBackgroundStyle.BorderTopWidth = 1;
            this.dtpTuyChon.MonthCalendar.CommandsBackgroundStyle.Class = "";
            this.dtpTuyChon.MonthCalendar.CommandsBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.dtpTuyChon.MonthCalendar.DisplayMonth = new System.DateTime(2017, 4, 1, 0, 0, 0, 0);
            this.dtpTuyChon.MonthCalendar.MarkedDates = new System.DateTime[0];
            this.dtpTuyChon.MonthCalendar.MonthlyMarkedDates = new System.DateTime[0];
            // 
            // 
            // 
            this.dtpTuyChon.MonthCalendar.NavigationBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.dtpTuyChon.MonthCalendar.NavigationBackgroundStyle.BackColorGradientAngle = 90;
            this.dtpTuyChon.MonthCalendar.NavigationBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.dtpTuyChon.MonthCalendar.NavigationBackgroundStyle.Class = "";
            this.dtpTuyChon.MonthCalendar.NavigationBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.dtpTuyChon.MonthCalendar.TodayButtonVisible = true;
            this.dtpTuyChon.MonthCalendar.WeeklyMarkedDays = new System.DayOfWeek[0];
            this.dtpTuyChon.Name = "dtpTuyChon";
            this.dtpTuyChon.Size = new System.Drawing.Size(174, 38);
            this.dtpTuyChon.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.dtpTuyChon.TabIndex = 2;
            this.dtpTuyChon.Value = new System.DateTime(2017, 1, 1, 0, 0, 0, 0);
            this.dtpTuyChon.ValueChanged += new System.EventHandler(this.dtpTuyChon_ValueChanged);
            // 
            // labelX1
            // 
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.Class = "";
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelX1.Location = new System.Drawing.Point(6, 8);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(84, 36);
            this.labelX1.TabIndex = 1;
            this.labelX1.Text = "NGÀY";
            // 
            // dtpNgayDau
            // 
            // 
            // 
            // 
            this.dtpNgayDau.BackgroundStyle.Class = "DateTimeInputBackground";
            this.dtpNgayDau.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.dtpNgayDau.ButtonDropDown.Shortcut = DevComponents.DotNetBar.eShortcut.AltDown;
            this.dtpNgayDau.ButtonDropDown.Visible = true;
            this.dtpNgayDau.CustomFormat = "dd/MM/yyyy";
            this.dtpNgayDau.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpNgayDau.Format = DevComponents.Editors.eDateTimePickerFormat.Custom;
            this.dtpNgayDau.IsPopupCalendarOpen = false;
            this.dtpNgayDau.Location = new System.Drawing.Point(96, 6);
            this.dtpNgayDau.MaxDate = new System.DateTime(2100, 12, 31, 0, 0, 0, 0);
            this.dtpNgayDau.MinDate = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            // 
            // 
            // 
            this.dtpNgayDau.MonthCalendar.AnnuallyMarkedDates = new System.DateTime[0];
            // 
            // 
            // 
            this.dtpNgayDau.MonthCalendar.BackgroundStyle.BackColor = System.Drawing.SystemColors.Window;
            this.dtpNgayDau.MonthCalendar.BackgroundStyle.Class = "";
            this.dtpNgayDau.MonthCalendar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.dtpNgayDau.MonthCalendar.ClearButtonVisible = true;
            // 
            // 
            // 
            this.dtpNgayDau.MonthCalendar.CommandsBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground2;
            this.dtpNgayDau.MonthCalendar.CommandsBackgroundStyle.BackColorGradientAngle = 90;
            this.dtpNgayDau.MonthCalendar.CommandsBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground;
            this.dtpNgayDau.MonthCalendar.CommandsBackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.dtpNgayDau.MonthCalendar.CommandsBackgroundStyle.BorderTopColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarDockedBorder;
            this.dtpNgayDau.MonthCalendar.CommandsBackgroundStyle.BorderTopWidth = 1;
            this.dtpNgayDau.MonthCalendar.CommandsBackgroundStyle.Class = "";
            this.dtpNgayDau.MonthCalendar.CommandsBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.dtpNgayDau.MonthCalendar.DisplayMonth = new System.DateTime(2017, 4, 1, 0, 0, 0, 0);
            this.dtpNgayDau.MonthCalendar.MarkedDates = new System.DateTime[0];
            this.dtpNgayDau.MonthCalendar.MonthlyMarkedDates = new System.DateTime[0];
            // 
            // 
            // 
            this.dtpNgayDau.MonthCalendar.NavigationBackgroundStyle.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.dtpNgayDau.MonthCalendar.NavigationBackgroundStyle.BackColorGradientAngle = 90;
            this.dtpNgayDau.MonthCalendar.NavigationBackgroundStyle.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.dtpNgayDau.MonthCalendar.NavigationBackgroundStyle.Class = "";
            this.dtpNgayDau.MonthCalendar.NavigationBackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.dtpNgayDau.MonthCalendar.TodayButtonVisible = true;
            this.dtpNgayDau.MonthCalendar.WeeklyMarkedDays = new System.DayOfWeek[0];
            this.dtpNgayDau.Name = "dtpNgayDau";
            this.dtpNgayDau.Size = new System.Drawing.Size(174, 38);
            this.dtpNgayDau.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.dtpNgayDau.TabIndex = 0;
            this.dtpNgayDau.Value = new System.DateTime(2017, 1, 1, 0, 0, 0, 0);
            this.dtpNgayDau.ValueChanged += new System.EventHandler(this.dtpNgayDau_ValueChanged);
            // 
            // frmThongKe
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1370, 531);
            this.Controls.Add(this.panelEx3);
            this.Controls.Add(this.panelEx2);
            this.Controls.Add(this.panelEx1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmThongKe";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.frmThongKe_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DTO_ReportHoaDonBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DTO_ReportHoaDonChiTietBindingSource)).EndInit();
            this.panelEx2.ResumeLayout(false);
            this.panelEx3.ResumeLayout(false);
            this.pnChon.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtpTuyChon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpNgayDau)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private DevComponents.DotNetBar.PanelEx panelEx1;
        private DevComponents.DotNetBar.PanelEx panelEx2;
        private DevComponents.DotNetBar.ButtonX btnTuyChon;
        private DevComponents.DotNetBar.ButtonX btnThangNay;
        private DevComponents.DotNetBar.ButtonX btnHomNay;
        private DevComponents.DotNetBar.PanelEx panelEx3;
        private DevComponents.DotNetBar.PanelEx pnChon;
        private DevComponents.Editors.DateTimeAdv.DateTimeInput dtpNgayDau;
        private DevComponents.DotNetBar.LabelX labelX1;
        private Microsoft.Reporting.WinForms.ReportViewer rpHoaDon;
        private System.Windows.Forms.BindingSource DTO_ReportHoaDonBindingSource;
        private System.Windows.Forms.BindingSource DTO_ReportHoaDonChiTietBindingSource;
        private DevComponents.DotNetBar.LabelX lbTuyChon;
        private DevComponents.Editors.DateTimeAdv.DateTimeInput dtpTuyChon;
    }
}

