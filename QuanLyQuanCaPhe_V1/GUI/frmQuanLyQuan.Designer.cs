﻿namespace GUI
{
    partial class frmQuanLyQuan
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.buttonItem1 = new DevComponents.DotNetBar.ButtonItem();
            this.btnChuyenBan = new DevComponents.DotNetBar.ButtonX();
            this.pnButton_LoaiMon = new System.Windows.Forms.FlowLayoutPanel();
            this.dgvMonAn = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.btnGiamGia = new DevComponents.DotNetBar.ButtonX();
            this.btnCheDo = new DevComponents.DotNetBar.ButtonX();
            this.panelEx1 = new DevComponents.DotNetBar.PanelEx();
            this.panelEx5 = new DevComponents.DotNetBar.PanelEx();
            this.panelEx7 = new DevComponents.DotNetBar.PanelEx();
            this.panelEx4 = new DevComponents.DotNetBar.PanelEx();
            this.panelEx8 = new DevComponents.DotNetBar.PanelEx();
            this.panelEx3 = new DevComponents.DotNetBar.PanelEx();
            this.pnChucNang = new DevComponents.DotNetBar.PanelEx();
            this.btnInHoaDon = new DevComponents.DotNetBar.ButtonX();
            this.lbThanhTien = new DevComponents.DotNetBar.LabelX();
            this.btnThanhToan = new DevComponents.DotNetBar.ButtonX();
            this.lstDanhSachHoaDonChiTiet = new DevComponents.DotNetBar.Controls.ListViewEx();
            this.pnBackground = new DevComponents.DotNetBar.PanelEx();
            this.panelEx2 = new DevComponents.DotNetBar.PanelEx();
            this.PanelX = new DevComponents.DotNetBar.PanelEx();
            this.panelEx6 = new DevComponents.DotNetBar.PanelEx();
            this.pnThongTin = new DevComponents.DotNetBar.PanelEx();
            this.pnHienThiBan = new DevComponents.DotNetBar.PanelEx();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMonAn)).BeginInit();
            this.panelEx1.SuspendLayout();
            this.panelEx5.SuspendLayout();
            this.panelEx7.SuspendLayout();
            this.panelEx4.SuspendLayout();
            this.panelEx8.SuspendLayout();
            this.panelEx3.SuspendLayout();
            this.pnChucNang.SuspendLayout();
            this.pnBackground.SuspendLayout();
            this.panelEx2.SuspendLayout();
            this.PanelX.SuspendLayout();
            this.panelEx6.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonItem1
            // 
            this.buttonItem1.Name = "buttonItem1";
            this.buttonItem1.Text = "buttonItem1";
            // 
            // btnChuyenBan
            // 
            this.btnChuyenBan.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnChuyenBan.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnChuyenBan.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnChuyenBan.Location = new System.Drawing.Point(6, 7);
            this.btnChuyenBan.Name = "btnChuyenBan";
            this.btnChuyenBan.Size = new System.Drawing.Size(150, 72);
            this.btnChuyenBan.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnChuyenBan.TabIndex = 2;
            this.btnChuyenBan.Text = "Chuyển Bàn";
            this.btnChuyenBan.Click += new System.EventHandler(this.btnChuyenBan_Click);
            // 
            // pnButton_LoaiMon
            // 
            this.pnButton_LoaiMon.AutoScroll = true;
            this.pnButton_LoaiMon.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnButton_LoaiMon.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pnButton_LoaiMon.Location = new System.Drawing.Point(0, 0);
            this.pnButton_LoaiMon.Name = "pnButton_LoaiMon";
            this.pnButton_LoaiMon.Size = new System.Drawing.Size(578, 116);
            this.pnButton_LoaiMon.TabIndex = 3;
            // 
            // dgvMonAn
            // 
            this.dgvMonAn.AllowUserToOrderColumns = true;
            this.dgvMonAn.AllowUserToResizeRows = false;
            this.dgvMonAn.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvMonAn.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleVertical;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvMonAn.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvMonAn.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvMonAn.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvMonAn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvMonAn.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(170)))), ((int)(((byte)(170)))));
            this.dgvMonAn.Location = new System.Drawing.Point(0, 0);
            this.dgvMonAn.MultiSelect = false;
            this.dgvMonAn.Name = "dgvMonAn";
            this.dgvMonAn.ReadOnly = true;
            this.dgvMonAn.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvMonAn.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvMonAn.Size = new System.Drawing.Size(354, 334);
            this.dgvMonAn.TabIndex = 2;
            this.dgvMonAn.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvMonAn_CellClick);
            // 
            // btnGiamGia
            // 
            this.btnGiamGia.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnGiamGia.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnGiamGia.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGiamGia.Location = new System.Drawing.Point(162, 44);
            this.btnGiamGia.Name = "btnGiamGia";
            this.btnGiamGia.Size = new System.Drawing.Size(201, 35);
            this.btnGiamGia.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnGiamGia.TabIndex = 1;
            this.btnGiamGia.Text = "Giảm Giá";
            this.btnGiamGia.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btnGiamGia_MouseDown);
            // 
            // btnCheDo
            // 
            this.btnCheDo.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnCheDo.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnCheDo.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCheDo.Location = new System.Drawing.Point(162, 7);
            this.btnCheDo.Name = "btnCheDo";
            this.btnCheDo.Size = new System.Drawing.Size(201, 35);
            this.btnCheDo.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnCheDo.TabIndex = 0;
            this.btnCheDo.Text = "Chế Độ NGÀY";
            this.btnCheDo.Click += new System.EventHandler(this.btnCheDo_Click);
            // 
            // panelEx1
            // 
            this.panelEx1.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelEx1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.panelEx1.Controls.Add(this.panelEx5);
            this.panelEx1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelEx1.Location = new System.Drawing.Point(0, 116);
            this.panelEx1.Name = "panelEx1";
            this.panelEx1.Size = new System.Drawing.Size(727, 416);
            this.panelEx1.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelEx1.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelEx1.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.panelEx1.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelEx1.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelEx1.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelEx1.Style.GradientAngle = 90;
            this.panelEx1.TabIndex = 1;
            // 
            // panelEx5
            // 
            this.panelEx5.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelEx5.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.panelEx5.Controls.Add(this.panelEx7);
            this.panelEx5.Controls.Add(this.panelEx3);
            this.panelEx5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelEx5.Location = new System.Drawing.Point(0, 0);
            this.panelEx5.Name = "panelEx5";
            this.panelEx5.Size = new System.Drawing.Size(727, 416);
            this.panelEx5.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelEx5.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelEx5.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.panelEx5.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelEx5.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelEx5.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelEx5.Style.GradientAngle = 90;
            this.panelEx5.TabIndex = 0;
            this.panelEx5.Text = "panelEx5";
            // 
            // panelEx7
            // 
            this.panelEx7.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelEx7.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.panelEx7.Controls.Add(this.panelEx4);
            this.panelEx7.Controls.Add(this.panelEx8);
            this.panelEx7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelEx7.Location = new System.Drawing.Point(0, 0);
            this.panelEx7.Name = "panelEx7";
            this.panelEx7.Size = new System.Drawing.Size(354, 416);
            this.panelEx7.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelEx7.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelEx7.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.panelEx7.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelEx7.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelEx7.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelEx7.Style.GradientAngle = 90;
            this.panelEx7.TabIndex = 4;
            this.panelEx7.Text = "panelEx7";
            // 
            // panelEx4
            // 
            this.panelEx4.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelEx4.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.panelEx4.Controls.Add(this.dgvMonAn);
            this.panelEx4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelEx4.Location = new System.Drawing.Point(0, 0);
            this.panelEx4.Name = "panelEx4";
            this.panelEx4.Size = new System.Drawing.Size(354, 334);
            this.panelEx4.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelEx4.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelEx4.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.panelEx4.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelEx4.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelEx4.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelEx4.Style.GradientAngle = 90;
            this.panelEx4.TabIndex = 4;
            this.panelEx4.Text = "panelEx4";
            // 
            // panelEx8
            // 
            this.panelEx8.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelEx8.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.panelEx8.Controls.Add(this.btnChuyenBan);
            this.panelEx8.Controls.Add(this.btnCheDo);
            this.panelEx8.Controls.Add(this.btnGiamGia);
            this.panelEx8.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelEx8.Location = new System.Drawing.Point(0, 334);
            this.panelEx8.Name = "panelEx8";
            this.panelEx8.Size = new System.Drawing.Size(354, 82);
            this.panelEx8.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelEx8.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelEx8.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.panelEx8.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelEx8.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelEx8.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelEx8.Style.GradientAngle = 90;
            this.panelEx8.TabIndex = 3;
            // 
            // panelEx3
            // 
            this.panelEx3.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelEx3.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.panelEx3.Controls.Add(this.pnChucNang);
            this.panelEx3.Controls.Add(this.lstDanhSachHoaDonChiTiet);
            this.panelEx3.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelEx3.Location = new System.Drawing.Point(354, 0);
            this.panelEx3.Name = "panelEx3";
            this.panelEx3.Size = new System.Drawing.Size(373, 416);
            this.panelEx3.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelEx3.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelEx3.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.panelEx3.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelEx3.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelEx3.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelEx3.Style.GradientAngle = 90;
            this.panelEx3.TabIndex = 3;
            this.panelEx3.Text = "panelEx3";
            // 
            // pnChucNang
            // 
            this.pnChucNang.CanvasColor = System.Drawing.SystemColors.Control;
            this.pnChucNang.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.pnChucNang.Controls.Add(this.btnInHoaDon);
            this.pnChucNang.Controls.Add(this.lbThanhTien);
            this.pnChucNang.Controls.Add(this.btnThanhToan);
            this.pnChucNang.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnChucNang.Location = new System.Drawing.Point(0, 334);
            this.pnChucNang.Name = "pnChucNang";
            this.pnChucNang.Size = new System.Drawing.Size(373, 82);
            this.pnChucNang.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.pnChucNang.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.pnChucNang.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.pnChucNang.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.pnChucNang.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.pnChucNang.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.pnChucNang.Style.GradientAngle = 90;
            this.pnChucNang.TabIndex = 1;
            // 
            // btnInHoaDon
            // 
            this.btnInHoaDon.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnInHoaDon.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnInHoaDon.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnInHoaDon.Location = new System.Drawing.Point(181, 7);
            this.btnInHoaDon.Name = "btnInHoaDon";
            this.btnInHoaDon.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.F4);
            this.btnInHoaDon.Size = new System.Drawing.Size(185, 35);
            this.btnInHoaDon.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnInHoaDon.TabIndex = 5;
            this.btnInHoaDon.Text = "In Hóa Đơn (F4)";
            this.btnInHoaDon.Click += new System.EventHandler(this.btnInHoaDon_Click);
            // 
            // lbThanhTien
            // 
            // 
            // 
            // 
            this.lbThanhTien.BackgroundStyle.Class = "";
            this.lbThanhTien.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lbThanhTien.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.lbThanhTien.Location = new System.Drawing.Point(7, 44);
            this.lbThanhTien.Name = "lbThanhTien";
            this.lbThanhTien.Size = new System.Drawing.Size(366, 36);
            this.lbThanhTien.TabIndex = 4;
            this.lbThanhTien.Text = "Tổng Cộng: 1,000,000 VNĐ";
            // 
            // btnThanhToan
            // 
            this.btnThanhToan.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnThanhToan.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.btnThanhToan.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.btnThanhToan.Location = new System.Drawing.Point(3, 7);
            this.btnThanhToan.Name = "btnThanhToan";
            this.btnThanhToan.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.F5);
            this.btnThanhToan.Size = new System.Drawing.Size(172, 35);
            this.btnThanhToan.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.btnThanhToan.TabIndex = 1;
            this.btnThanhToan.Text = "Thanh Toán (F5)";
            this.btnThanhToan.Click += new System.EventHandler(this.btnThanhToan_Click);
            // 
            // lstDanhSachHoaDonChiTiet
            // 
            // 
            // 
            // 
            this.lstDanhSachHoaDonChiTiet.Border.Class = "ListViewBorder";
            this.lstDanhSachHoaDonChiTiet.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lstDanhSachHoaDonChiTiet.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstDanhSachHoaDonChiTiet.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lstDanhSachHoaDonChiTiet.FullRowSelect = true;
            this.lstDanhSachHoaDonChiTiet.Location = new System.Drawing.Point(0, 0);
            this.lstDanhSachHoaDonChiTiet.MultiSelect = false;
            this.lstDanhSachHoaDonChiTiet.Name = "lstDanhSachHoaDonChiTiet";
            this.lstDanhSachHoaDonChiTiet.Size = new System.Drawing.Size(373, 416);
            this.lstDanhSachHoaDonChiTiet.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.lstDanhSachHoaDonChiTiet.TabIndex = 2;
            this.lstDanhSachHoaDonChiTiet.UseCompatibleStateImageBehavior = false;
            this.lstDanhSachHoaDonChiTiet.View = System.Windows.Forms.View.Details;
            this.lstDanhSachHoaDonChiTiet.MouseClick += new System.Windows.Forms.MouseEventHandler(this.lstDanhSachHoaDonChiTiet_MouseClick);
            // 
            // pnBackground
            // 
            this.pnBackground.CanvasColor = System.Drawing.SystemColors.Control;
            this.pnBackground.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.pnBackground.Controls.Add(this.panelEx2);
            this.pnBackground.Controls.Add(this.pnHienThiBan);
            this.pnBackground.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnBackground.Location = new System.Drawing.Point(0, 0);
            this.pnBackground.Name = "pnBackground";
            this.pnBackground.Size = new System.Drawing.Size(1354, 532);
            this.pnBackground.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.pnBackground.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.pnBackground.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.pnBackground.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.pnBackground.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.pnBackground.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.pnBackground.Style.GradientAngle = 90;
            this.pnBackground.TabIndex = 1;
            // 
            // panelEx2
            // 
            this.panelEx2.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelEx2.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.panelEx2.Controls.Add(this.panelEx1);
            this.panelEx2.Controls.Add(this.PanelX);
            this.panelEx2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelEx2.Location = new System.Drawing.Point(627, 0);
            this.panelEx2.Name = "panelEx2";
            this.panelEx2.Size = new System.Drawing.Size(727, 532);
            this.panelEx2.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelEx2.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelEx2.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.panelEx2.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelEx2.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelEx2.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelEx2.Style.GradientAngle = 90;
            this.panelEx2.TabIndex = 2;
            // 
            // PanelX
            // 
            this.PanelX.CanvasColor = System.Drawing.SystemColors.Control;
            this.PanelX.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.PanelX.Controls.Add(this.panelEx6);
            this.PanelX.Controls.Add(this.pnThongTin);
            this.PanelX.Dock = System.Windows.Forms.DockStyle.Top;
            this.PanelX.Location = new System.Drawing.Point(0, 0);
            this.PanelX.Name = "PanelX";
            this.PanelX.Size = new System.Drawing.Size(727, 116);
            this.PanelX.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.PanelX.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.PanelX.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.PanelX.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.PanelX.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.PanelX.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.PanelX.Style.GradientAngle = 90;
            this.PanelX.TabIndex = 0;
            // 
            // panelEx6
            // 
            this.panelEx6.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelEx6.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.panelEx6.Controls.Add(this.pnButton_LoaiMon);
            this.panelEx6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelEx6.Location = new System.Drawing.Point(0, 0);
            this.panelEx6.Name = "panelEx6";
            this.panelEx6.Size = new System.Drawing.Size(578, 116);
            this.panelEx6.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelEx6.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelEx6.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.panelEx6.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelEx6.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelEx6.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelEx6.Style.GradientAngle = 90;
            this.panelEx6.TabIndex = 5;
            this.panelEx6.Text = "panelEx6";
            // 
            // pnThongTin
            // 
            this.pnThongTin.CanvasColor = System.Drawing.SystemColors.Control;
            this.pnThongTin.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.pnThongTin.Dock = System.Windows.Forms.DockStyle.Right;
            this.pnThongTin.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.pnThongTin.Location = new System.Drawing.Point(578, 0);
            this.pnThongTin.Name = "pnThongTin";
            this.pnThongTin.Size = new System.Drawing.Size(149, 116);
            this.pnThongTin.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.pnThongTin.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.pnThongTin.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.pnThongTin.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.pnThongTin.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.pnThongTin.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.pnThongTin.Style.GradientAngle = 90;
            this.pnThongTin.TabIndex = 4;
            this.pnThongTin.Text = "Bàn: ABCXYZ";
            // 
            // pnHienThiBan
            // 
            this.pnHienThiBan.AutoScroll = true;
            this.pnHienThiBan.CanvasColor = System.Drawing.SystemColors.Control;
            this.pnHienThiBan.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.pnHienThiBan.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnHienThiBan.Location = new System.Drawing.Point(0, 0);
            this.pnHienThiBan.Name = "pnHienThiBan";
            this.pnHienThiBan.Size = new System.Drawing.Size(627, 532);
            this.pnHienThiBan.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.pnHienThiBan.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.pnHienThiBan.Style.BackColor2.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.pnHienThiBan.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.pnHienThiBan.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.pnHienThiBan.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.pnHienThiBan.Style.GradientAngle = 90;
            this.pnHienThiBan.TabIndex = 0;
            // 
            // frmQuanLyQuan
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1354, 532);
            this.Controls.Add(this.pnBackground);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmQuanLyQuan";
            this.Text = "frmQuanLyQuan";
            ((System.ComponentModel.ISupportInitialize)(this.dgvMonAn)).EndInit();
            this.panelEx1.ResumeLayout(false);
            this.panelEx5.ResumeLayout(false);
            this.panelEx7.ResumeLayout(false);
            this.panelEx4.ResumeLayout(false);
            this.panelEx8.ResumeLayout(false);
            this.panelEx3.ResumeLayout(false);
            this.pnChucNang.ResumeLayout(false);
            this.pnBackground.ResumeLayout(false);
            this.panelEx2.ResumeLayout(false);
            this.PanelX.ResumeLayout(false);
            this.panelEx6.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.ButtonItem buttonItem1;
        private DevComponents.DotNetBar.ButtonX btnChuyenBan;
        private System.Windows.Forms.FlowLayoutPanel pnButton_LoaiMon;
        private DevComponents.DotNetBar.Controls.DataGridViewX dgvMonAn;
        private DevComponents.DotNetBar.ButtonX btnGiamGia;
        private DevComponents.DotNetBar.ButtonX btnCheDo;
        private DevComponents.DotNetBar.PanelEx panelEx1;
        private DevComponents.DotNetBar.PanelEx pnBackground;
        private DevComponents.DotNetBar.PanelEx panelEx2;
        private DevComponents.DotNetBar.Controls.ListViewEx lstDanhSachHoaDonChiTiet;
        private DevComponents.DotNetBar.PanelEx pnChucNang;
        private DevComponents.DotNetBar.LabelX lbThanhTien;
        private DevComponents.DotNetBar.ButtonX btnThanhToan;
        private DevComponents.DotNetBar.PanelEx PanelX;
        private DevComponents.DotNetBar.PanelEx pnHienThiBan;
        private DevComponents.DotNetBar.ButtonX btnInHoaDon;
        private DevComponents.DotNetBar.PanelEx panelEx3;
        private DevComponents.DotNetBar.PanelEx pnThongTin;
        private DevComponents.DotNetBar.PanelEx panelEx6;
        private DevComponents.DotNetBar.PanelEx panelEx5;
        private DevComponents.DotNetBar.PanelEx panelEx7;
        private DevComponents.DotNetBar.PanelEx panelEx8;
        private DevComponents.DotNetBar.PanelEx panelEx4;
    }
}