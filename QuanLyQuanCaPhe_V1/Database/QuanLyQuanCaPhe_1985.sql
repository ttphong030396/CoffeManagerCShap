CREATE DATABASE QuanLyQuanCaPhe_1985
GO
USE QuanLyQuanCaPhe_1985
GO
--ALTER DATABASE QuanLyQuanCaPhe_1985 SET MULTI_USER
--GO
--ALTER DATABASE QuanLyQuanCaPhe_1985 SET SINGLE_USER WITH ROLLBACK IMMEDIATE
--GO
CREATE TABLE DanhMuc
(
	ID INT IDENTITY PRIMARY KEY,
	Ten NVARCHAR(100) NOT NULL --DEFAULT N'Đang cập nhật'
)
GO
CREATE TABLE Mon
(
	ID INT IDENTITY PRIMARY KEY,
	Ten NVARCHAR(100) NOT NULL, --DEFAULT N'Đang cập nhật',
	DanhMuc INT NOT NULL,
	Mode NVARCHAR(30) NOT NULL DEFAULT N'Ngày',
	GiaNgay FLOAT NOT NULL DEFAULT 0,
	GiaDem FLOAT NOT NULL DEFAULT 0
	FOREIGN KEY(DanhMuc) REFERENCES dbo.DanhMuc(ID)
)
GO
CREATE TABLE KhuVuc
(
	ID INT IDENTITY PRIMARY KEY,
	Ten NVARCHAR(100) NOT NULL-- DEFAULT N'Đang cập nhật'
)
GO
CREATE TABLE Ban
(
	ID INT IDENTITY PRIMARY KEY,
	Ten NVARCHAR(100) NOT NULL,--DEFAULT N'Đang cập nhật',
	TinhTrang NVARCHAR(100) NOT NULL DEFAULT N'Trống',		--trong|co nguoi
	Khu INT NOT NULL,
	FOREIGN KEY(Khu) REFERENCES dbo.KhuVuc(ID) 
)
GO
CREATE TABLE LoaiTK
(
	ID INT IDENTITY PRIMARY KEY,
	Ten NVARCHAR(100) NOT NULL DEFAULT N'Đang cập nhật'
)
GO
CREATE TABLE TaiKhoan
(
	TenTK NVARCHAR(100) PRIMARY KEY,
	MatKhau NVARCHAR(100) NOT NULL DEFAULT N'0',
	TenHienThi NVARCHAR(100) NOT NULL DEFAULT N'Đang cập nhật',
	IDLoai INT NOT NULL
	FOREIGN KEY(IDLoai) REFERENCES dbo.LoaiTK(ID)
)
GO
CREATE TABLE HoaDon
( 
	ID INT IDENTITY PRIMARY KEY,
	NgayVao DATETIME NOT NULL DEFAULT GETDATE(),
	Ban INT NOT NULL,
	TinhTrang INT NOT NULL DEFAULT 0, -- 0: Chua thanh toan| 1: Da thanh toan,
	ThanhTien FLOAT NOT NULL DEFAULT 0,
	TienGiam FLOAT NOT NULL DEFAULT 0,
	GiamGia INT NOT NULL DEFAULT 0
	FOREIGN KEY(Ban) REFERENCES dbo.Ban(ID)
)
GO
CREATE TABLE HoaDonChiTiet
(
	ID INT IDENTITY PRIMARY KEY,
	HoaDon INT NOT NULL,
	Mon INT NOT NULL,
	SoLuong INT NOT NULL DEFAULT 0,
	TongTien FLOAT NOT NUll DEFAULT 0,
	FOREIGN KEY(HoaDon) REFERENCES dbo.HoaDon(ID),
	FOREIGN KEY(Mon) REFERENCES dbo.Mon(ID)
)
GO
CREATE TABLE LichSu
(
	ID INT IDENTITY PRIMARY KEY,
	NguoiDung NVARCHAR(100),
	HanhVi NVARCHAR(300),
	ThoiGian DATETIME,
	FOREIGN KEY(NguoiDung) REFERENCES dbo.TaiKhoan(TenTK)
)
GO
-- Them Loai Mon
INSERT INTO dbo.DanhMuc(Ten)VALUES(N'Soda')
GO
INSERT INTO dbo.DanhMuc(Ten)VALUES(N'Cafe & Trà & Yaourt')
GO
INSERT INTO dbo.DanhMuc(Ten)VALUES(N'Sinh tố')
GO
INSERT INTO dbo.DanhMuc(Ten)VALUES(N'Nước ép')
GO
INSERT INTO dbo.DanhMuc(Ten)VALUES(N'Thức ăn nhẹ')
GO
INSERT INTO dbo.DanhMuc(Ten)VALUES(N'Nước ngọt')
GO
INSERT INTO dbo.DanhMuc(Ten)VALUES(N'Trà sữa')
GO
INSERT INTO dbo.DanhMuc(Ten)VALUES(N'Thức uống khác')
GO
-- Them loai Tai khoan
INSERT INTO dbo.LoaiTK(Ten)VALUES(N'Admin')
GO
INSERT INTO dbo.LoaiTK(Ten)VALUES(N'Nhân viên')
GO
-- Them tai khoan
INSERT INTO dbo.TaiKhoan(TenTK,MatKhau,TenHienThi,IDLoai)VALUES(N'admin',N'123',N'Admin',1)
GO
INSERT INTO dbo.TaiKhoan(TenTK,MatKhau,TenHienThi,IDLoai)VALUES(N'staff',N'123',N'Staff',2)
GO
-- Them KhuVuc
INSERT INTO dbo.KhuVuc(Ten)VALUES(N'Cổng')
GO
INSERT INTO dbo.KhuVuc(Ten)VALUES(N'Sân')
GO
INSERT INTO dbo.KhuVuc(Ten)VALUES(N'Hồ')
GO
INSERT INTO dbo.KhuVuc(Ten)VALUES(N'Lầu')
GO
INSERT INTO dbo.KhuVuc(Ten)VALUES(N'Nhà')
GO
INSERT INTO dbo.KhuVuc(Ten)VALUES(N'Gỗ')
GO
INSERT INTO dbo.KhuVuc(Ten)VALUES(N'Cửa Sổ')
GO
INSERT INTO dbo.KhuVuc(Ten)VALUES(N'Hành lang')
GO
INSERT INTO dbo.KhuVuc(Ten)VALUES(N'Quầy bar')
GO
-- Thêm bàn
INSERT INTO dbo.Ban(Ten,Khu)VALUES(N'C1',1)
GO
INSERT INTO dbo.Ban(Ten,Khu)VALUES(N'C2',1)
GO
INSERT INTO dbo.Ban(Ten,Khu)VALUES(N'C3',1)
GO
INSERT INTO dbo.Ban(Ten,Khu)VALUES(N'C4',1)
GO
INSERT INTO dbo.Ban(Ten,Khu)VALUES(N'C5',1)
GO
INSERT INTO dbo.Ban(Ten,Khu)VALUES(N'C6',1)
GO
INSERT INTO dbo.Ban(Ten,Khu)VALUES(N'C7',1)
GO
INSERT INTO dbo.Ban(Ten,Khu)VALUES(N'C8',1)
GO
INSERT INTO dbo.Ban(Ten,Khu)VALUES(N'S1',2)
GO
INSERT INTO dbo.Ban(Ten,Khu)VALUES(N'S2',2)
GO
INSERT INTO dbo.Ban(Ten,Khu)VALUES(N'S3',2)
GO
INSERT INTO dbo.Ban(Ten,Khu)VALUES(N'S4',2)
GO
INSERT INTO dbo.Ban(Ten,Khu)VALUES(N'S5',2)
GO
INSERT INTO dbo.Ban(Ten,Khu)VALUES(N'S6',2)
GO
INSERT INTO dbo.Ban(Ten,Khu)VALUES(N'S7',2)
GO
INSERT INTO dbo.Ban(Ten,Khu)VALUES(N'H1',3)
GO
INSERT INTO dbo.Ban(Ten,Khu)VALUES(N'H2',3)
GO
INSERT INTO dbo.Ban(Ten,Khu)VALUES(N'H3',3)
GO
INSERT INTO dbo.Ban(Ten,Khu)VALUES(N'L1',4)
GO
INSERT INTO dbo.Ban(Ten,Khu)VALUES(N'L2',4)
GO
INSERT INTO dbo.Ban(Ten,Khu)VALUES(N'L3',4)
GO
INSERT INTO dbo.Ban(Ten,Khu)VALUES(N'L4',4)
GO
INSERT INTO dbo.Ban(Ten,Khu)VALUES(N'L5',4)
GO
INSERT INTO dbo.Ban(Ten,Khu)VALUES(N'L6',4)
GO
INSERT INTO dbo.Ban(Ten,Khu)VALUES(N'L7',4)
GO
INSERT INTO dbo.Ban(Ten,Khu)VALUES(N'L8',4)
GO
INSERT INTO dbo.Ban(Ten,Khu)VALUES(N'N1',5)
GO
INSERT INTO dbo.Ban(Ten,Khu)VALUES(N'N2',5)
GO
INSERT INTO dbo.Ban(Ten,Khu)VALUES(N'N3',5)
GO
INSERT INTO dbo.Ban(Ten,Khu)VALUES(N'N4',5)
GO
INSERT INTO dbo.Ban(Ten,Khu)VALUES(N'N5',5)
GO
INSERT INTO dbo.Ban(Ten,Khu)VALUES(N'G1',6)
GO
INSERT INTO dbo.Ban(Ten,Khu)VALUES(N'G2',6)
GO
INSERT INTO dbo.Ban(Ten,Khu)VALUES(N'G3',6)
GO
INSERT INTO dbo.Ban(Ten,Khu)VALUES(N'G4',6)
GO
INSERT INTO dbo.Ban(Ten,Khu)VALUES(N'G5',6)
GO
INSERT INTO dbo.Ban(Ten,Khu)VALUES(N'CS1',7)
GO
INSERT INTO dbo.Ban(Ten,Khu)VALUES(N'CS2',7)
GO
INSERT INTO dbo.Ban(Ten,Khu)VALUES(N'HL1',8)
GO
INSERT INTO dbo.Ban(Ten,Khu)VALUES(N'HL2',8)
GO
INSERT INTO dbo.Ban(Ten,Khu)VALUES(N'QB1',9)
GO
INSERT INTO dbo.Ban(Ten,Khu)VALUES(N'QB2',9)
GO
-- Them Mon(loại Mon: 1-soda|2-cafe&tra&yaourt|3-sinhto|4-nuocep|5-thucannhe|6-nuocngot|7-trasua|8-thucuongkhac)
-- Soda
INSERT INTO dbo.Mon(Ten,DanhMuc,Mode,GiaNgay,GiaDem)VALUES(N'Soda Blue Curacao',1,N'Đêm',25000,50000)
GO
INSERT INTO dbo.Mon(Ten,DanhMuc,Mode,GiaNgay,GiaDem)VALUES(N'Soda Black Berry',1,N'Đêm',25000,50000)
GO
INSERT INTO dbo.Mon(Ten,DanhMuc,Mode,GiaNgay,GiaDem)VALUES(N'Soda Blue Berry',1,N'Đêm',25000,50000)
GO
INSERT INTO dbo.Mon(Ten,DanhMuc,Mode,GiaNgay,GiaDem)VALUES(N'Soda Blue Raspberry',1,N'Đêm',25000,50000)
GO
INSERT INTO dbo.Mon(Ten,DanhMuc,Mode,GiaNgay,GiaDem)VALUES(N'Soda Black Currant',1,N'Đêm',25000,50000)
GO
INSERT INTO dbo.Mon(Ten,DanhMuc,Mode,GiaNgay,GiaDem)VALUES(N'Soda Cherry',1,N'Đêm',25000,50000)
GO
INSERT INTO dbo.Mon(Ten,DanhMuc,Mode,GiaNgay,GiaDem)VALUES(N'Soda Hazelnut',1,N'Đêm',25000,50000)
GO
INSERT INTO dbo.Mon(Ten,DanhMuc,Mode,GiaNgay,GiaDem)VALUES(N'Soda Mojito',1,N'Đêm',25000,50000)
GO
INSERT INTO dbo.Mon(Ten,DanhMuc,Mode,GiaNgay,GiaDem)VALUES(N'Soda Grenadine',1,N'Đêm',25000,50000)
GO
INSERT INTO dbo.Mon(Ten,DanhMuc,Mode,GiaNgay,GiaDem)VALUES(N'Soda Sữa Hột Gà',1,N'Đêm',28000,55000)
GO
INSERT INTO dbo.Mon(Ten,DanhMuc,Mode,GiaNgay,GiaDem)VALUES(N'Soda Siro Hương',1,N'Đêm',20000,45000)
GO
-- Cafe & Tea & Yaourt
INSERT INTO dbo.Mon(Ten,DanhMuc,Mode,GiaNgay,GiaDem)VALUES(N'Cafe Đá',2,N'Đêm',15000,40000)
GO
INSERT INTO dbo.Mon(Ten,DanhMuc,Mode,GiaNgay,GiaDem)VALUES(N'Cafe Đá 1985',2,N'Đêm',20000,45000)
GO

INSERT INTO dbo.Mon
        ( Ten, DanhMuc, Mode, GiaNgay, GiaDem )
VALUES  ( N'Cafe Sữa', -- Ten - nvarchar(100)
          2,
		  N'Đêm', -- DanhMuc - int
          20000  -- DonGia - float
		  ,45000
          )
GO

INSERT INTO dbo.Mon
        ( Ten, DanhMuc, Mode, GiaNgay, GiaDem )
VALUES  ( N'Cafe Sữa 1985', -- Ten - nvarchar(100)
          2,
		  N'Đêm', -- DanhMuc - int
          23000  -- DonGia - float
		  ,48000
          )
GO

INSERT INTO dbo.Mon
        ( Ten, DanhMuc, Mode, GiaNgay, GiaDem )
VALUES  ( N'Cafe Sữa Đá Xoay', -- Ten - nvarchar(100)
          2,
		  N'Đêm', -- DanhMuc - int
          25000  -- DonGia - float
		  ,50000
          )
GO

INSERT INTO dbo.Mon
        ( Ten, DanhMuc, Mode, GiaNgay, GiaDem )
VALUES  ( N'Bạc xỉu', -- Ten - nvarchar(100)
          2,
		  N'Đêm', -- DanhMuc - int
          25000  -- DonGia - float
		  ,50000
          )
GO

INSERT INTO dbo.Mon
        ( Ten, DanhMuc, Mode, GiaNgay, GiaDem )
VALUES  ( N'Cacao Sữa', -- Ten - nvarchar(100)
          2,
		  N'Đêm',
          25000  -- DonGia - float
		  ,50000
          )
GO

INSERT INTO dbo.Mon
        ( Ten, DanhMuc, Mode, GiaNgay, GiaDem )
VALUES  ( N'Sữa Tươi', -- Ten - nvarchar(100)
          2,
		  N'Đêm',
          15000  -- DonGia - float
		  ,40000
          )
GO

INSERT INTO dbo.Mon
        ( Ten, DanhMuc, Mode, GiaNgay, GiaDem )
VALUES  ( N'Trà Lipton Đá', -- Ten - nvarchar(100)
          2,
		  N'Đêm',
          15000  -- DonGia - float
		  ,40000
          )
GO

INSERT INTO dbo.Mon
        ( Ten, DanhMuc, Mode, GiaNgay, GiaDem )
VALUES  ( N'Trà Gừng', -- Ten - nvarchar(100)
          2,
		  N'Đêm',
          15000  -- DonGia - float
		  ,40000
          )
GO

INSERT INTO dbo.Mon
        ( Ten, DanhMuc, Mode, GiaNgay, GiaDem )
VALUES  ( N'Trà Đào', -- Ten - nvarchar(100)
          2,
		  N'Đêm',
          20000  -- DonGia - float
		  ,40000
          )
GO

INSERT INTO dbo.Mon
        ( Ten, DanhMuc, Mode, GiaNgay, GiaDem )
VALUES  ( N'Trà Lipton Chanh', -- Ten - nvarchar(100)
          2,
		  N'Đêm',
          20000  -- DonGia - float
		  ,45000
          )
GO

INSERT INTO dbo.Mon
        ( Ten, DanhMuc, Mode, GiaNgay, GiaDem )
VALUES  ( N'Trà Lipton Xí Muội', -- Ten - nvarchar(100)
          2,
		  N'Đêm',
          20000  -- DonGia - float
		  ,45000
          )
GO

INSERT INTO dbo.Mon
        ( Ten, DanhMuc, Mode, GiaNgay, GiaDem )
VALUES  ( N'Trà Lipton Sữa', -- Ten - nvarchar(100)
          2,
		  N'Đêm',
          20000  -- DonGia - float
		  ,45000
          )
GO

INSERT INTO dbo.Mon
        ( Ten, DanhMuc, Mode, GiaNgay, GiaDem )
VALUES  ( N'Yaourt Đá', -- Ten - nvarchar(100)
          2,
		  N'Đêm',
          20000  -- DonGia - float
		  ,40000
          )
GO

INSERT INTO dbo.Mon
        ( Ten, DanhMuc, Mode, GiaNgay, GiaDem )
VALUES  ( N'Yaourt Cafe', -- Ten - nvarchar(100)
          2,
		  N'Đêm',
          20000  -- DonGia - float
		  ,45000
          )
GO

INSERT INTO dbo.Mon
        ( Ten, DanhMuc, Mode, GiaNgay, GiaDem )
VALUES  ( N'Yaourt Nha Đam', -- Ten - nvarchar(100)
          2,
		  N'Đêm',
          20000  -- DonGia - float
		  ,45000
          )
GO

INSERT INTO dbo.Mon
        ( Ten, DanhMuc, Mode, GiaNgay, GiaDem )
VALUES  ( N'Yaourt Siro', -- Ten - nvarchar(100)
          2,
		  N'Đêm',
          20000  -- DonGia - float
		  ,45000
          )
GO

-- them Sinh tố

INSERT INTO dbo.Mon
        ( Ten, DanhMuc, GiaNgay )
VALUES  ( N'Sinh tố Dâu Mãng Cầu', -- Ten - nvarchar(100)
          3, -- DanhMuc - int
          35000  -- DonGia - float
          )
GO

INSERT INTO dbo.Mon
        ( Ten, DanhMuc, GiaNgay )
VALUES  ( N'Sinh tố Dâu', -- Ten - nvarchar(100)
          3, -- DanhMuc - int
          30000  -- DonGia - float
          )
GO

INSERT INTO dbo.Mon
        ( Ten, DanhMuc, GiaNgay )
VALUES  ( N'Sinh tố Bơ', -- Ten - nvarchar(100)
          3, -- DanhMuc - int
          30000  -- DonGia - float
          )
GO

INSERT INTO dbo.Mon
        ( Ten, DanhMuc, GiaNgay )
VALUES  ( N'Sinh tố Việt Quất', -- Ten - nvarchar(100)
          3, -- DanhMuc - int
          30000  -- DonGia - float
          )
GO

INSERT INTO dbo.Mon
        ( Ten, DanhMuc, GiaNgay )
VALUES  ( N'Sinh tố Phúc Bồn Tử', -- Ten - nvarchar(100)
          3, -- DanhMuc - int
          30000  -- DonGia - float
          )
GO

INSERT INTO dbo.Mon
        ( Ten, DanhMuc, GiaNgay )
VALUES  ( N'Sinh tố Mãng Cầu', -- Ten - nvarchar(100)
          3, -- DanhMuc - int
          30000  -- DonGia - float
          )
GO

INSERT INTO dbo.Mon
        ( Ten, DanhMuc, GiaNgay )
VALUES  ( N'Sinh tố Cam', -- Ten - nvarchar(100)
          3, -- DanhMuc - int
          30000  -- DonGia - float
          )
GO

INSERT INTO dbo.Mon
        ( Ten, DanhMuc, GiaNgay )
VALUES  ( N'Sinh tố Sapo Cafe', -- Ten - nvarchar(100)
          3, -- DanhMuc - int
          30000  -- DonGia - float
          )
GO

INSERT INTO dbo.Mon
        ( Ten, DanhMuc, GiaNgay )
VALUES  ( N'Sinh tố Cà cải', -- Ten - nvarchar(100)
          3, -- DanhMuc - int
          25000  -- DonGia - float
          )
GO

INSERT INTO dbo.Mon
        ( Ten, DanhMuc, GiaNgay )
VALUES  ( N'Sinh tố Cà Chua', -- Ten - nvarchar(100)
          3, -- DanhMuc - int
          20000  -- DonGia - float
          )
GO

INSERT INTO dbo.Mon
        ( Ten, DanhMuc, GiaNgay )
VALUES  ( N'Sinh tố Cà Rốt', -- Ten - nvarchar(100)
          3, -- DanhMuc - int
          20000  -- DonGia - float
          )
GO

-- Thêm Nước Ép

INSERT INTO dbo.Mon
        ( Ten, DanhMuc, GiaNgay )
VALUES  ( N'Nước ép Bưởi', -- Ten - nvarchar(100)
          4, -- DanhMuc - int
          28000  -- DonGia - float
          )
GO

INSERT INTO dbo.Mon
        ( Ten, DanhMuc, GiaNgay )
VALUES  ( N'Nước ép Chanh Dây', -- Ten - nvarchar(100)
          4, -- DanhMuc - int
          28000  -- DonGia - float
          )
GO

INSERT INTO dbo.Mon
        ( Ten, DanhMuc, GiaNgay )
VALUES  ( N'Nước ép Cam', -- Ten - nvarchar(100)
          4, -- DanhMuc - int
          28000  -- DonGia - float
          )
GO

INSERT INTO dbo.Mon
        ( Ten, DanhMuc, GiaNgay )
VALUES  ( N'Nước ép Ổi', -- Ten - nvarchar(100)
          4, -- DanhMuc - int
          20000  -- DonGia - float
          )
GO

INSERT INTO dbo.Mon
        ( Ten, DanhMuc, GiaNgay )
VALUES  ( N'Nước ép Thơm', -- Ten - nvarchar(100)
          4, -- DanhMuc - int
          20000  -- DonGia - float
          )
GO

INSERT INTO dbo.Mon
        ( Ten, DanhMuc, GiaNgay )
VALUES  ( N'Nước ép Dưa Hấu', -- Ten - nvarchar(100)
          4, -- DanhMuc - int
          20000  -- DonGia - float
          )
GO

INSERT INTO dbo.Mon
        ( Ten, DanhMuc, GiaNgay )
VALUES  ( N'Nước ép Quýt', -- Ten - nvarchar(100)
          4, -- DanhMuc - int
          20000  -- DonGia - float
          )
GO

INSERT INTO dbo.Mon
        ( Ten, DanhMuc, GiaNgay )
VALUES  ( N'Nước ép Cà Chua', -- Ten - nvarchar(100)
          4, -- DanhMuc - int
          20000  -- DonGia - float
          )
GO

INSERT INTO dbo.Mon
        ( Ten, DanhMuc, GiaNgay )
VALUES  ( N'Nước ép Cà Rốt', -- Ten - nvarchar(100)
          4, -- DanhMuc - int
          20000  -- DonGia - float
          )
GO

INSERT INTO dbo.Mon
        ( Ten, DanhMuc, GiaNgay )
VALUES  ( N'Nước ép 3C', -- Ten - nvarchar(100)
          4, -- DanhMuc - int
          30000  -- DonGia - float
          )
GO

-- Thêm Mon Nước Ngọt

INSERT INTO dbo.Mon
        ( Ten, DanhMuc, GiaNgay )
VALUES  ( N'Khoai Tây chiên', -- Ten - nvarchar(100)
          5, -- DanhMuc - int
          20000  -- DonGia - float
          )
GO

INSERT INTO dbo.Mon(Ten,DanhMuc,GiaNgay)VALUES(N'Sake chiên',5,20000)
GO
INSERT INTO dbo.Mon(Ten,DanhMuc,GiaNgay)VALUES(N'Bò viên chiên',5,20000)
GO
INSERT INTO dbo.Mon(Ten,DanhMuc,GiaNgay)VALUES(N'Bánh mì Ốp La',5,18000)
GO
INSERT INTO dbo.Mon(Ten,DanhMuc,GiaNgay)VALUES(N'Bánh mì Cá Mồi',5,20000)
GO
INSERT INTO dbo.Mon(Ten,DanhMuc,GiaNgay)VALUES(N'Bánh mì Ốp La Cá Mồi',5,25000)
GO
INSERT INTO dbo.Mon(Ten,DanhMuc,GiaNgay)VALUES(N'Cơm Sườn Ốp La',5,25000)
GO
INSERT INTO dbo.Mon(Ten,DanhMuc,GiaNgay)VALUES(N'Bún Thịt Nướng',5,25000)
GO
INSERT INTO dbo.Mon(Ten,DanhMuc,GiaNgay)VALUES(N'Mì gói Trứng',5,20000)
GO
INSERT INTO dbo.Mon(Ten,DanhMuc,GiaNgay)VALUES(N'Mì gói Bò Viên',5,22000)
GO
INSERT INTO dbo.Mon(Ten,DanhMuc,GiaNgay)VALUES(N'Mì gói Bò',5,30000)
GO
INSERT INTO dbo.Mon(Ten,DanhMuc,GiaNgay)VALUES(N'Mì gói xào Bò',5,30000)
GO
INSERT INTO dbo.Mon(Ten,DanhMuc,GiaNgay)VALUES(N'Bò Lúc Lắc Bành mì',5,40000)
GO
INSERT INTO dbo.Mon(Ten,DanhMuc,GiaNgay)VALUES(N'Bò Lúc Lắc Khoai Tây',5,45000)
GO
-- Thêm Nước ngọt
INSERT INTO dbo.Mon(Ten,DanhMuc,Mode,GiaNgay,GiaDem)VALUES(N'Suối Aquafina',6,N'Đêm',15000,40000)
GO
INSERT INTO dbo.Mon(Ten,DanhMuc,Mode,GiaNgay,GiaDem)VALUES(N'7Up',6,N'Đêm',18000,40000)
GO

INSERT INTO dbo.Mon
        ( Ten, DanhMuc, Mode, GiaNgay, GiaDem )
VALUES  ( N'Pepsi', -- Ten - nvarchar(100)
          6,
		  N'Đêm',
          18000  -- DonGia - float
		  ,40000
          )
GO

INSERT INTO dbo.Mon
        ( Ten, DanhMuc, Mode, GiaNgay, GiaDem )
VALUES  ( N'Sting', -- Ten - nvarchar(100)
          6,
		  N'Đêm',
          18000  -- DonGia - float
		  ,40000
          )
GO

INSERT INTO dbo.Mon
        ( Ten, DanhMuc, Mode, GiaNgay, GiaDem )
VALUES  ( N'Trà xanh 0 độ', -- Ten - nvarchar(100)
          6,
		  N'Đêm',
          18000  -- DonGia - float
		  ,40000
          )
GO

INSERT INTO dbo.Mon
        ( Ten, DanhMuc, Mode, GiaNgay, GiaDem )
VALUES  ( N'C2', -- Ten - nvarchar(100)
          6,
		  N'Đêm',
          18000  -- DonGia - float
		  ,40000
          )
GO

INSERT INTO dbo.Mon
        ( Ten, DanhMuc, Mode, GiaNgay, GiaDem )
VALUES  ( N'CC Lemon', -- Ten - nvarchar(100)
          6,
		  N'Đêm',
          18000  -- DonGia - float
		  ,40000
          )
GO

INSERT INTO dbo.Mon
        ( Ten, DanhMuc, Mode, GiaNgay, GiaDem )
VALUES  ( N'Trà Ô Long', -- Ten - nvarchar(100)
          6,
		  N'Đêm',
          18000  -- DonGia - float
		  ,40000
          )
GO

INSERT INTO dbo.Mon
        ( Ten, DanhMuc, Mode, GiaNgay, GiaDem )
VALUES  ( N'Bò Húc Redbull', -- Ten - nvarchar(100)
          6,
		  N'Đêm',
          20000  -- DonGia - float
		  ,40000
          )
GO

INSERT INTO dbo.Mon
        ( Ten, DanhMuc, Mode, GiaNgay, GiaDem )
VALUES  ( N'Heneiken', -- Ten - nvarchar(100)
          6,
		  N'Đêm',
          25000  -- DonGia - float
		  ,50000
          )
GO

--Thêm Trà Sữa 
INSERT INTO dbo.Mon
        ( Ten, DanhMuc, Mode, GiaNgay, GiaDem )
VALUES  ( N'Trà Sữa 1985', -- Ten - nvarchar(100)
          7,
		  N'Đêm',
          28000  -- DonGia - float
		  ,50000
          )
GO

INSERT INTO dbo.Mon
        ( Ten, DanhMuc, Mode, GiaNgay, GiaDem )
VALUES  ( N'Trà Sữa', -- Ten - nvarchar(100)
          7,
		  N'Đêm',
          20000  -- DonGia - float
		  ,40000
          )
GO

INSERT INTO dbo.Mon
        ( Ten, DanhMuc, Mode, GiaNgay, GiaDem )
VALUES  ( N'Trà Sữa Thạch', -- Ten - nvarchar(100)
          7,
		  N'Đêm',
          25000  -- DonGia - float
		  ,45000
          )
GO

INSERT INTO dbo.Mon
        ( Ten, DanhMuc, Mode, GiaNgay, GiaDem )
VALUES  ( N'Trà sữa hạt thủy tinh', -- Ten - nvarchar(100)
          7,
		  N'Đêm',
          25000  -- DonGia - float
		  ,45000
          )
GO

INSERT INTO dbo.Mon
        ( Ten, DanhMuc, Mode, GiaNgay, GiaDem )
VALUES  ( N'Hồng Trà sủi bọt', -- Ten - nvarchar(100)
          7,
		  N'Đêm',
          18000  -- DonGia - float
		  ,40000
          )
GO

INSERT INTO dbo.Mon
        ( Ten, DanhMuc, Mode, GiaNgay, GiaDem )
VALUES  ( N'Hồng Trà thạch', -- Ten - nvarchar(100)
          7,
		  N'Đêm',
          25000  -- DonGia - float
		  ,45000
          )
GO

INSERT INTO dbo.Mon
        ( Ten, DanhMuc, Mode, GiaNgay, GiaDem )
VALUES  ( N'Hồng Trà hạt thủy tinh', -- Ten - nvarchar(100)
          7,
		  N'Đêm',
		  25000,
          45000  -- DonGia - float
          )
GO

INSERT INTO dbo.Mon
        ( Ten, DanhMuc, Mode, GiaNgay, GiaDem)
VALUES  ( N'Món thêm(Thạch/Hạt thủy tinh)', -- Ten - nvarchar(100)
          7,
		  N'Đêm',
          5000,  -- DonGia - float
		  5000
          )


-- Thêm Thức uống khác
INSERT INTO dbo.Mon(Ten,DanhMuc,Mode,GiaNgay,GiaDem)VALUES(N'Lô Hội Mật Ong Chanh',8,N'Đêm',28000,50000)
GO
INSERT INTO dbo.Mon(Ten,DanhMuc,Mode,GiaNgay,GiaDem)VALUES(N'Cà Dâu đá đập',8,N'Đêm',28000 ,50000)
GO
INSERT INTO dbo.Mon(Ten,DanhMuc,Mode,GiaNgay,GiaDem)VALUES(N'Cà Cam đá đập',8,N'Đêm',28000,50000)
GO
INSERT INTO dbo.Mon(Ten,DanhMuc,Mode,GiaNgay,GiaDem)VALUES(N'Nước Sấu ngâm',8,N'Đêm',25000,45000)
GO
INSERT INTO dbo.Mon(Ten,DanhMuc,Mode,GiaNgay,GiaDem)VALUES(N'Nước Mơ ngâm',8,N'Đêm',18000,45000)
GO
INSERT INTO dbo.Mon(Ten,DanhMuc,Mode,GiaNgay,GiaDem)VALUES(N'Đá Chanh',8,N'Đêm',18000,40000)
GO
INSERT INTO dbo.Mon(Ten,DanhMuc,Mode,GiaNgay,GiaDem)VALUES(N'Chanh muối',8,N'Đêm',18000,40000)
GO
INSERT INTO dbo.Mon(Ten,DanhMuc,Mode,GiaNgay,GiaDem)VALUES(N'Dừa tươi',8,N'Đêm',20000,40000)
GO
INSERT INTO dbo.Mon(Ten,DanhMuc,Mode,GiaNgay,GiaDem)VALUES(N'Cam sữa',8,N'Đêm',30000,55000)
GO
INSERT INTO dbo.Mon(Ten,DanhMuc,Mode,GiaNgay,GiaDem)VALUES(N'Cam mật ong',8,N'Đêm',30000,55000)
GO
INSERT INTO dbo.Mon(Ten,DanhMuc,Mode,GiaNgay,GiaDem)VALUES(N'Chanh Dây',8,N'Đêm',25000,50000)
GO
-------------------------------------PROC-----------------------------------
----------------------------------------------------------------------------
CREATE PROC ThemHoaDonChiTiet
 @idbill INT , @Mon INT , @sl INT , @gia FLOAT
AS
BEGIN
	DECLARE @isExistBillChiTiet INT
	DECLARE @slMon INT = 1

	SELECT @isExistBillChiTiet = ID, @slMon = SoLuong
	FROM dbo.HoaDonChiTiet WHERE HoaDon = @idbill AND Mon = @Mon
	IF ( @isExistBillChiTiet > 0)
	BEGIN
		DECLARE @newSL INT = @slMon + @sl
		IF( @newSL > 0)
		BEGIN
			UPDATE dbo.HoaDonChiTiet SET SoLuong = @slMon + @sl WHERE ID = @isExistBillChiTiet
			UPDATE dbo.HoaDonChiTiet SET TongTien = SoLuong * @gia WHERE ID = @isExistBillChiTiet
		END
		ELSE
			DELETE dbo.HoaDonChiTiet WHERE HoaDon = @idBill AND Mon = @Mon
    END
	ELSE
	BEGIN
		INSERT INTO dbo.HoaDonChiTiet(HoaDon,Mon,SoLuong,TongTien)VALUES(@idbill,@Mon,@sl,@sl * @gia)
	END
END
GO
CREATE PROC CapNhatHoaDonChiTiet
 @idbillchitiet INT , @Mon INT , @sl INT , @gia FLOAT
AS
BEGIN
	DECLARE @slMon INT = 1
	SELECT @slMon = SoLuong FROM dbo.HoaDonChiTiet WHERE ID = @idbillchitiet	
	DECLARE @newSL INT = @slMon + @sl
	IF( @newSL > 0)
	BEGIN
			UPDATE dbo.HoaDonChiTiet SET SoLuong = @slMon + @sl WHERE ID = @idbillchitiet
			UPDATE dbo.HoaDonChiTiet SET TongTien = SoLuong * @gia WHERE ID = @idbillchitiet
	END
	ELSE
			DELETE dbo.HoaDonChiTiet WHERE ID = @idbillchitiet
END
GO
CREATE PROC USP_Login
@userName nvarchar(100), @MatKhauWord nvarchar(100)
AS
BEGIN
	SELECT * FROM dbo.TaiKhoan WHERE TenTK = @userName AND MatKhau = @MatKhauWord
END
GO
CREATE PROC LayDanhSachMon_Trang
@page int 
AS
BEGIN 
	DECLARE @pagerow INT = 20
	DECLARE @selectrow INT = @pagerow * @page
	DECLARE @exceptrow INT = (@page -1) *@pagerow
	
	;WITH MonShow AS (SELECT * FROM Mon)
	SELECT TOP (@selectrow)* FROM MonShow
	EXCEPT 
	SELECT TOP (@exceptrow)* FROM MonShow
END
GO
CREATE PROC LayDanhSachMon_Trang_Mode
@page int , @mode nvarchar(10)
AS
BEGIN 
	DECLARE @pagerow INT = 20
	DECLARE @selectrow INT = @pagerow * @page
	DECLARE @exceptrow INT = (@page -1) *@pagerow
	
	;WITH MonShow AS (SELECT * FROM Mon WHERE Mode = @mode)
	SELECT TOP (@selectrow)* FROM MonShow
	EXCEPT 
	SELECT TOP (@exceptrow)* FROM MonShow
END
GO
CREATE PROC LayDanhSachMon_Trang_Mode_Loai
@page int , @mode nvarchar(10), @idloai INT
AS
BEGIN 
	DECLARE @pagerow INT = 20
	DECLARE @selectrow INT = @pagerow * @page
	DECLARE @exceptrow INT = (@page -1) *@pagerow
	
	;WITH MonShow AS (SELECT * FROM Mon WHERE Mode = @mode AND DanhMuc = @idloai )
	SELECT TOP (@selectrow)* FROM MonShow
	EXCEPT 
	SELECT TOP (@exceptrow)* FROM MonShow
END
GO
CREATE PROC LayDanhSach_Trang_Loai
@DanhMuc INT, @page INT
AS
BEGIN
	DECLARE @pagerow INT = 20
	DECLARE @selectrow INT = @pagerow * @page
	DECLARE @exceptrow INT = (@page -1) * @pagerow
	;WITH MonByIDLoai AS (SELECT * FROM Mon where DanhMuc = @DanhMuc)
	SELECT TOP (@selectrow)* FROM MonByIDLoai
	EXCEPT 
	SELECT TOP (@exceptrow)* FROM MonByIDLoai
END
GO
CREATE PROC LayDanhSachBan_Trang
@page int 
AS
BEGIN 
	DECLARE @pagerow INT = 20
	DECLARE @selectrow INT = @pagerow * @page
	DECLARE @exceptrow INT = (@page -1) *@pagerow
	;WITH BanShow AS (SELECT * FROM Ban)
	SELECT TOP (@selectrow)* FROM BanShow
	EXCEPT 
	SELECT TOP (@exceptrow)* FROM BanShow
END
GO
CREATE PROC LayDanhSachBan_Trang_Khu
@Khu INT, @page INT
AS
BEGIN
	DECLARE @pagerow INT = 20
	DECLARE @selectrow INT = @pagerow * @page
	DECLARE @exceptrow INT = (@page -1) * @pagerow
	;WITH BanByKhu AS (SELECT * FROM Ban where Khu = @Khu)
	SELECT TOP (@selectrow)* FROM BanByKhu
	EXCEPT 
	SELECT TOP (@exceptrow)* FROM BanByKhu
END
GO
-- Lấy danh sách món ăn gồm tên món ăn, số lượng, đơn giá, thành tiền, tổng tiền từ Ban cho trước
CREATE PROC LayDanhSachHoaDonChiTiet_Ban
@Ban INT
AS
BEGIN
SELECT F.ID, F.Ten, CT.SoLuong, F.GiaNgay,F.GiaDem , CT.SoLuong * F.GiaNgay AS 'TongNgay',CT.SoLuong * F.GiaDem AS 'TongDem', HD.ID AS HoaDon, HD.GiamGia
FROM Mon F,HoaDon HD,HoaDonChiTiet CT, Ban B
WHERE F.ID = CT.Mon AND CT.HoaDon = HD.ID AND HD.Ban = B.ID AND B.ID = @Ban AND HD.TinhTrang = 0
END
GO

CREATE PROC LayHoaDonTheoNgay
@ngay DATE
AS
BEGIN
	SELECT HD.ID , HD.NgayVao,B.Ten,HD.GiamGia,HD.TienGiam, HD.ThanhTien
	FROM HoaDon HD,Ban B
	WHERE DATEDIFF(DAY,HD.NgayVao,@ngay)= 0 AND HD.TinhTrang = 1 AND HD.Ban = B.ID
END
GO
CREATE PROC LayHoaDonTheoThang
@ngay DATE
AS
BEGIN
	SELECT HD.ID , HD.NgayVao,B.Ten,HD.GiamGia,HD.TienGiam, HD.ThanhTien
	FROM HoaDon HD,Ban B
	WHERE DATEDIFF(MONTH,HD.NgayVao,@ngay)= 0 AND HD.TinhTrang = 1 AND HD.Ban = B.ID
END
GO
CREATE PROC LayHoaDonTheoKhoanThoiGian
@ngay1 DATE, @ngay2 DATE
AS
BEGIN
	SELECT HD.ID , HD.NgayVao,B.Ten,HD.GiamGia,HD.TienGiam, HD.ThanhTien
	FROM HoaDon HD,Ban B
	WHERE HD.NgayVao BETWEEN @ngay1 AND @ngay2 AND HD.TinhTrang = 1 AND HD.Ban = B.ID
END
GO
CREATE PROC HoaDonChiTietTheoNgay
@ngay DATE
AS
BEGIN
	SELECT HD.ID, HD.NgayVao,B.Ten,F.Ten,CT.SoLuong,CT.TongTien
	FROM HoaDon HD, HoaDonChiTiet CT,Ban B,Mon F
	WHERE HD.ID = CT.HoaDon AND HD.Ban = B.ID AND CT.Mon = F.ID AND HD.TinhTrang = 1 AND DATEDIFF(DAY,HD.NgayVao,@ngay)= 0
END
GO
CREATE PROC HoaDonChiTietTheoThang
@ngay DATE
AS
BEGIN
	SELECT HD.ID, HD.NgayVao,B.Ten,F.Ten,CT.SoLuong,CT.TongTien
	FROM HoaDon HD, HoaDonChiTiet CT,Ban B,Mon F
	WHERE HD.ID = CT.HoaDon AND HD.Ban = B.ID AND CT.Mon = F.ID AND HD.TinhTrang = 1 AND DATEDIFF(MONTH,HD.NgayVao,@ngay)= 0
END
GO

CREATE PROC HoaDonChiTietTheoKhoanThoiGian
@ngay1 DATE, @ngay2 DATE
AS
BEGIN
	SELECT HD.ID, HD.NgayVao,B.Ten,F.Ten,CT.SoLuong,CT.TongTien
	FROM HoaDon HD, HoaDonChiTiet CT,Ban B,Mon F
	WHERE HD.ID = CT.HoaDon AND HD.Ban = B.ID AND CT.Mon = F.ID AND HD.TinhTrang = 1 AND HD.NgayVao BETWEEN @ngay1 AND @ngay2
END
GO


CREATE PROC LayLichSu_NguoiDung
@nguoidung NVARCHAR(100)
AS
BEGIN
	DECLARE @loai INT
	SELECT @loai = IDLoai FROM TaiKhoan WHERE TenTK = @nguoidung
	IF(@loai=1)--admin
	BEGIN 
		SELECT TOP(5)* FROM LichSu WHERE DATEDIFF(DAY,ThoiGian,GETDATE())= 0 ORDER BY ThoiGian DESC
	END
	ELSE --nhanvien
	BEGIN 
		SELECT TOP(5)* FROM LichSu WHERE DATEDIFF(DAY,ThoiGian,GETDATE())= 0 AND NguoiDung = @nguoidung ORDER BY ThoiGian DESC
	END
END
GO

--LayLichSu_NguoiDung N'admin'
--SELECT * FROM LichSu
--GO
--SELECT * FROM Mon
--BACKUP DATABASE QuanLyQuanCaPhe_1985 TO DISK= 'D:\\DB_1985_12_4_2017.bak'
--USE master RESTORE DATABASE QuanLyQuanCaPhe_1985 FROM DISK= 'D:/1111.bak' WITH REPLACE
--GO
