﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using DTO;
using DAO;
namespace BUS
{
    public class BUS_Ban
    {
        private static BUS_Ban instance;
        public static BUS_Ban Instance
        {
            get { if (instance == null) instance = new BUS_Ban(); return BUS_Ban.instance; }
            private set { BUS_Ban.instance = value; }
        }
        private BUS_Ban() { }
        #region Method
        public bool Insert(DTO_Ban ban)
        {
            return DAO_Ban.Instance.Insert(ban);
        }
        public bool Update(DTO_Ban ban)
        {
            return DAO_Ban.Instance.Update(ban);
        }
        public bool Delete(int IDBan)
        {
            return DAO_Ban.Instance.Delete(IDBan);
        }
        public List<DTO_Ban> LayDanhSach()
        {
            return DAO_Ban.Instance.LayDanhSach();
        }
        
        public List<DTO_Ban> LayDanhSach_Khu(int IDKhu)
        {
            return DAO_Ban.Instance.LayDanhSach_Khu(IDKhu);
        }

        public List<DTO_Ban> LayDanhSach_Trang(int Trang)
        {
            return DAO_Ban.Instance.LayDanhSach_Trang(Trang);
        }
        public List<DTO_Ban> LayDanhSach_Trang_Khu(int Trang, int IDKhu)
        {
            return DAO_Ban.Instance.LayDanhSach_Trang_Khu(Trang, IDKhu);
        }
        public bool CapNhatTrangThaiBan(int IDBan, string TrangThai)
        {
            return DAO_Ban.Instance.CapNhatTrangThaiBan(IDBan, TrangThai);
        }
        public int GetCount(int IDKhu)
        {
            return DAO_Ban.Instance.GetCount(IDKhu);
        }
        public int GetCount()
        {
            return DAO_Ban.Instance.GetCount();
        }
        public string TrangThai(int IDban)
        {
            return DAO_Ban.Instance.TrangThai(IDban);
        }
        public int CountTen(string TenBan)
        {
            return DAO_Ban.Instance.CountTen(TenBan);
        }
        #endregion
    }
}
