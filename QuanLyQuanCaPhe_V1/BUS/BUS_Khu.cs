﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;
using DAO;
namespace BUS
{
    public class BUS_Khu
    {
        private static BUS_Khu instance;

        public static BUS_Khu Instance
        {
            get { if (instance == null) instance = new BUS_Khu(); return BUS_Khu.instance; }
            private set { BUS_Khu.instance = value; }
        }
        private BUS_Khu() { }

        #region Method
        public bool Insert(DTO_Khu khu)
        {
            return DAO_Khu.Instance.Insert(khu);
        }
        public bool Update(DTO_Khu khu)
        {
            return DAO_Khu.Instance.Update(khu);
        }
        public bool Delete(int IDKhu)
        {
            return DAO_Khu.Instance.Delete(IDKhu);
        }
        public List<DTO_Khu> LayDanhSach()
        {
            return DAO_Khu.Instance.LayDanhSach();
        }
        public int DemTenKhu(string TenKhu)
        {
            return DAO_Khu.Instance.DemTenKhu(TenKhu);
        }
        #endregion 
    }
}
