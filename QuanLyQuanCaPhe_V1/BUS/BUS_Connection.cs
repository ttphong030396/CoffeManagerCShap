﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;
using DAO;
namespace BUS
{
    public class Connection_BUS
    {
        private static Connection_BUS instance;
        private Connection_BUS() { }
        public static Connection_BUS Instance
        {
            get { if (instance == null) instance = new Connection_BUS(); return Connection_BUS.instance; }
            private set { Connection_BUS.instance = value; }
        }
        public bool TestConnection(string connect)
        {
            try
            {
                 DataProvider.Instance.Strconnect = connect;
                 return DataProvider.Instance.TestConnection();
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
