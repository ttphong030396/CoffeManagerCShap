﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAO;
using System.Data;
namespace BUS
{
    public class BUS_Database
    {
        private static BUS_Database instance;

        public static BUS_Database Instance
        {
            get { if (instance == null)instance = new BUS_Database(); return BUS_Database.instance; }
            private set { BUS_Database.instance = value; }
        }
        private BUS_Database() { }
        public bool SaoLuu(string path)
        {
            return DAO_Database.Instance.SaoLuu(path);
        }
        public bool PhucHoi(string path)
        {
            return DAO_Database.Instance.PhucHoi(path);
        }
        public bool GhiLichSu(string NguoiDung,string HanhVi,DateTime ThoiGian)
        {
            return DAO_Database.Instance.GhiLichSu(NguoiDung, HanhVi, ThoiGian);
        }
        public bool GhiLoi(string Loi,DateTime ThoiGian)
        {
            return DAO_Database.Instance.GhiLoi(Loi, ThoiGian);
        }
        public DataTable LayLichSu(string NguoiDung)
        {
            return DAO_Database.Instance.LayLichSu(NguoiDung);
        }
        public DataTable LayTatCaLichSu()
        {
            return DAO_Database.Instance.LayTatCaLichSu();
        }
        public DataTable LayTatCaLichSu(string taikhoan)
        {
            return DAO_Database.Instance.LayTatCaLichSu(taikhoan);
        }
    }
}
