﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;
using DAO;
namespace BUS
{
    public class BUS_TaiKhoan
    {
        private static BUS_TaiKhoan instance;

        public static BUS_TaiKhoan Instance
        {
            get { if (instance == null)instance = new BUS_TaiKhoan(); return BUS_TaiKhoan.instance; }
            private set { BUS_TaiKhoan.instance = value; }
        }
        private BUS_TaiKhoan() { }
        public bool Insert(DTO_TaiKhoan tk)
        {
            return DAO_TaiKhoan.Instance.Insert(tk);
        }
        public bool Update(DTO_TaiKhoan tk)
        {
            return DAO_TaiKhoan.Instance.Update(tk);
        }
        public bool Delete(string user)
        {
            return DAO_TaiKhoan.Instance.Delete(user);
        }
        public DTO_TaiKhoan Get(string user, string pass)
        {
            return DAO_TaiKhoan.Instance.Get(user, pass);
        }
        public List<DTO_TaiKhoan> GetAll()
        {
            return DAO_TaiKhoan.Instance.GetAll();
        }
        
    }
}
