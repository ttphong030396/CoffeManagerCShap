﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;
using DAO;
namespace BUS
{
    public class BUS_HoaDonChiTiet
    {
        private static BUS_HoaDonChiTiet instance;

        public static BUS_HoaDonChiTiet Instance
        {
            get { if (instance == null) instance = new BUS_HoaDonChiTiet(); return BUS_HoaDonChiTiet.instance; }
            private set { BUS_HoaDonChiTiet.instance = value; }
        }
        private BUS_HoaDonChiTiet() { }
        #region Method
        public object LayIDHoaDonChiTiet_IDHoaDon_IDMonAn(int IDHoaDon, int IDMonAn)
        {
            try
            {
                return DAO_HoaDonChiTiet.Instance.LayIDHoaDonChiTiet_IDHoaDon_IDMonAn(IDHoaDon, IDMonAn);
            }
            catch (Exception)
            {
                return null;
            }
        }
        public bool Insert(int IDHoaDon,int IDMonAn, int SoLuong, float DonGia)
        {
            return DAO_HoaDonChiTiet.Instance.Insert(IDHoaDon, IDMonAn, SoLuong, DonGia);
        }
        public bool Update(int IDHoaDon, int IDMonAn, int SoLuong, float DonGia)
        {
            return DAO_HoaDonChiTiet.Instance.Update(IDHoaDon, IDMonAn, SoLuong, DonGia);
        }
        public List<DTO_HoaDon_HoaDonChiTiet_MonAn> LayDanhSach_IDBan(int IDBan)
        {
            try
            {
                return DAO_HoaDonChiTiet.Instance.LayDanhSach_IDBan(IDBan);
            }
            catch (Exception)
            {
                return null;
            }
        }
        public bool Delete(int IDHoaDonCHiTiet)
        {
            return DAO_HoaDonChiTiet.Instance.Delete(IDHoaDonCHiTiet);
        }
        public int DemSoDong(int IDHoaDon)
        {
            return DAO_HoaDonChiTiet.Instance.DemSoDong(IDHoaDon);
        }
        #endregion
    }
}
