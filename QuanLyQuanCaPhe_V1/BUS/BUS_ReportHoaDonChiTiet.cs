﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;
using DAO;
namespace BUS
{
    public class BUS_ReportHoaDonChiTiet
    {
        private static BUS_ReportHoaDonChiTiet instance;

        public static BUS_ReportHoaDonChiTiet Instance
        {
            get { if (instance == null) instance = new BUS_ReportHoaDonChiTiet(); return BUS_ReportHoaDonChiTiet.instance; }
            private set { BUS_ReportHoaDonChiTiet.instance = value; }
        }
        private BUS_ReportHoaDonChiTiet() { }

        public List<DTO_ReportHoaDonChiTiet> Report(string ngay)
        {
            return DAO_ReportHoaDonChiTiet.Instance.Report(ngay);
        }
        public List<DTO_ReportHoaDonChiTiet> ReportMonth(string ngay)
        {
            return DAO_ReportHoaDonChiTiet.Instance.ReportMonth(ngay);
        }
        public List<DTO_ReportHoaDonChiTiet> ReportCustom(string ngaybatdau, string ngayketthuc)
        {
            return DAO_ReportHoaDonChiTiet.Instance.ReportCustom(ngaybatdau, ngayketthuc);
        }
    }
}
