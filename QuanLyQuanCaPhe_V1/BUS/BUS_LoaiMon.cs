﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;
using DAO;
namespace BUS
{
    public class BUS_LoaiMon
    {
        private static BUS_LoaiMon instance;

        public static BUS_LoaiMon Instance
        {
            get { if (instance == null) instance = new BUS_LoaiMon(); return BUS_LoaiMon.instance; }
            private set { BUS_LoaiMon.instance = value; }
        }
        private BUS_LoaiMon() { }

        #region Method
        public bool Insert(DTO_LoaiMon loai)
        {
            return DAO_LoaiMon.Instance.Insert(loai);
        }
        public bool Update(DTO_LoaiMon loai)
        {
            return DAO_LoaiMon.Instance.Update(loai);
        }
        public bool Delete(int ID)
        {
            return DAO_LoaiMon.Instance.Delete(ID);
        }
        public List<DTO_LoaiMon> LayDanhSach()
        {
            try
            {
                return DAO_LoaiMon.Instance.LayDanhSach();
            }
            catch (Exception)
            {
                return null;
            }
        }
        public string LayTen_ID(int ID)
        {
            return DAO_LoaiMon.Instance.LayTen_ID(ID);
        }


        #endregion
    }
}
