﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;
using DAO;
namespace BUS
{
    public class BUS_MonAn
    {
        private static BUS_MonAn instance;

        public static BUS_MonAn Instance
        {
            get { if (instance == null)instance = new BUS_MonAn(); return BUS_MonAn.instance; }
            private set { BUS_MonAn.instance = value; }
        }
        private BUS_MonAn() { }
        #region Method
        public bool Insert(DTO_MonAn mon)
        {
            return DAO_MonAn.Instance.Insert(mon);
        }
        public bool Delete(int IDMon)
        {
            return DAO_MonAn.Instance.Delete(IDMon);
        }
        public bool Update(DTO_MonAn mon)
        {
            return DAO_MonAn.Instance.Update(mon);
        }
        public List<DTO_MonAn> LayDanhSach_Mode(string Mode)
        {
            try
            {
                return DAO_MonAn.Instance.LayDanhSach_Mode(Mode);
            }
            catch (Exception)
            {
                return null;
            }
        }
        public List<DTO_MonAn> LayDanhSach_Mode_IDLoai(string Mode, int IDLoai)
        {
            try
            {
                return DAO_MonAn.Instance.LayDanhSach_Mode_IDLoai(Mode, IDLoai);
            }
            catch (Exception)
            {
                return null;
            }
        }
        public List<DTO_MonAn> LayDanhSach_IDLoai(int IDLoai)
        {
            try
            {
                return DAO_MonAn.Instance.LayDanhSach_IDLoai(IDLoai);
            }
            catch (Exception)
            {
                return null;
            }
        }
        //Phaan trang
        public List<DTO_MonAn> LayDanhSach_Trang(int Trang)
        {
            return DAO_MonAn.Instance.LayDanhSach_Trang(Trang);
        }
        public List<DTO_MonAn> LayDanhSach_Trang_IDLoai(int Trang, int IDLoai)
        {
            return DAO_MonAn.Instance.LayDanhSach_Trang_IDLoai(Trang, IDLoai);
        }
        public List<DTO_MonAn> LayDanhSach_Trang_Mode(int Trang, string Mode)
        {
            return DAO_MonAn.Instance.LayDanhSach_Trang_Mode(Trang, Mode);
        }
        public List<DTO_MonAn> LayDanhSach_Trang_Mode_IDLoai(int Trang, string Mode, int IDLoai)
        {
            return DAO_MonAn.Instance.LayDanhSach_Trang_Mode_IDLoai(Trang, Mode, IDLoai);
        }
        public int GetCount(int IDLoai)
        {
            return DAO_MonAn.Instance.GetCount(IDLoai);
        }
        public int GetCount()
        {
            return DAO_MonAn.Instance.GetCount();
        }
        public string LayTen_ID(int ID)
        {
            return DAO_MonAn.Instance.LayTen_ID(ID);
        }
        #endregion
    }
}
