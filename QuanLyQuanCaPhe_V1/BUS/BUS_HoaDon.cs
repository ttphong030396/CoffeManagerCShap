﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;
using DAO;
namespace BUS
{
    public class BUS_HoaDon
    {
        private static BUS_HoaDon instance;
        public static BUS_HoaDon Instance
        {
            get { if (instance == null)instance = new BUS_HoaDon(); return BUS_HoaDon.instance; }
            private set { BUS_HoaDon.instance = value; }
        }
        private BUS_HoaDon() { }
        public object LayIDHoaDonTheoIDBan(int IDBan)
        {
            try
            {
                return DAO_HoaDon.Instance.LayIDHoaDon_IDBan(IDBan);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public DTO_HoaDon LayHoaDon_IDBan(int IDBan)
        {
            try
            {
                return DAO_HoaDon.Instance.LayHoaDon_IDBan(IDBan);
            }
            catch(Exception)
            {
                throw;
            }
        }
        public bool Insert(int IDBan)
        {
            return DAO_HoaDon.Instance.Insert(IDBan);
        }
        public object LayIDHoaDonChuaThanhToan_IDBan(int IDBan)
        {
            return DAO_HoaDon.Instance.LayIDHoaDonChuaThanhToan_IDBan(IDBan);
        }
        public object TongTien_IDHD(int IDHoaDon)
        {
            try
            {
                return DAO_HoaDon.Instance.TongTien_IDHD(IDHoaDon);
            }
            catch (Exception)
            {
                return null;
            }
        }
        public bool CapNhatGiamGia(int IDHoaDon, int PhanTramGiamGia)
        {
            return DAO_HoaDon.Instance.CapNhatGiamGia(IDHoaDon, PhanTramGiamGia);
        }
        public bool ChuyenBan(int HoaDon, int IDBanChuyenDen)
        {
            return DAO_HoaDon.Instance.ChuyenBan(HoaDon, IDBanChuyenDen);
        }
        public bool Delete(int IDHoaDon)
        {
            return DAO_HoaDon.Instance.Delete(IDHoaDon);
        }
        public bool ThanhToan(int IDHoaDon, float TongTien, int PhanTramGiamGia, float TienGiam)
        {
            return DAO_HoaDon.Instance.ThanhToan(IDHoaDon, TongTien, PhanTramGiamGia, TienGiam);
        }
    }
}
