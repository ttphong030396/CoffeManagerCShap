﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAO;
using DTO;
namespace BUS
{
    public class BUS_LoaiTaiKhoan
    {
        private static BUS_LoaiTaiKhoan instance;

        public static BUS_LoaiTaiKhoan Instance
        {
            get { if (instance == null)instance = new BUS_LoaiTaiKhoan(); return BUS_LoaiTaiKhoan.instance; }
            private set { BUS_LoaiTaiKhoan.instance = value; }
        }
        public BUS_LoaiTaiKhoan() { }


        public List<DTO_LoaiTaiKhoan> Get()
        {
            return DAO_LoaiTaiKhoan.Instance.Get();
        }
        public string LayTen_ID(int ID)
        {
            return DAO_LoaiTaiKhoan.Instance.LayTen_ID(ID);
        }
    }
}
