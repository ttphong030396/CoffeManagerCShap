﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAO;
using DTO;
namespace BUS
{
    public class BUS_ReportHoaDon
    {
        private static BUS_ReportHoaDon instance;

        public static BUS_ReportHoaDon Instance
        {
            get { if (instance == null)instance = new BUS_ReportHoaDon(); return BUS_ReportHoaDon.instance; }
            set { BUS_ReportHoaDon.instance = value; }
        }
        private BUS_ReportHoaDon() { }
        public List<DTO_ReportHoaDon> Report(string ngay)
        {
            return DAO_ReportHoaDon.Instance.Report(ngay);
        }
        public List<DTO_ReportHoaDon> ReportMonth(string ngay)
        {
            return DAO_ReportHoaDon.Instance.ReportMonth(ngay);
        }
        public List<DTO_ReportHoaDon> ReportCustom(string ngaybatdau, string ngayketthuc)
        {
            return DAO_ReportHoaDon.Instance.ReportCustom(ngaybatdau, ngayketthuc);
        }

    }
}
