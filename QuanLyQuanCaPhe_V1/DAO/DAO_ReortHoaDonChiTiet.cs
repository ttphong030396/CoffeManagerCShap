﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;
using System.Data;
namespace DAO
{
    public class DAO_ReportHoaDonChiTiet
    {
        private static DAO_ReportHoaDonChiTiet instance;

        public static DAO_ReportHoaDonChiTiet Instance
        {
            get { if (instance == null) instance = new DAO_ReportHoaDonChiTiet(); return DAO_ReportHoaDonChiTiet.instance; }
            private set { DAO_ReportHoaDonChiTiet.instance = value; }
        }
        private DAO_ReportHoaDonChiTiet() { }

        public List<DTO_ReportHoaDonChiTiet> Report(string ngay)
        {
            try
            {
                DataTable dt = DataProvider.Instance.ExecuteQuery("HoaDonChiTietTheoNgay @ngay ", new object[] { ngay });
                List<DTO_ReportHoaDonChiTiet> lst = new List<DTO_ReportHoaDonChiTiet>();
                foreach (DataRow item in dt.Rows)
                {
                    DTO_ReportHoaDonChiTiet value = new DTO_ReportHoaDonChiTiet(item);
                    lst.Add(value);
                }
                return lst;
            }
            catch (Exception)
            {
                return null;
            }
        }
        public List<DTO_ReportHoaDonChiTiet> ReportMonth(string ngay)
        {
            try
            {
                DataTable dt = DataProvider.Instance.ExecuteQuery("HoaDonChiTietTheoThang @ngay ", new object[] { ngay });
                List<DTO_ReportHoaDonChiTiet> lst = new List<DTO_ReportHoaDonChiTiet>();
                foreach (DataRow item in dt.Rows)
                {
                    DTO_ReportHoaDonChiTiet value = new DTO_ReportHoaDonChiTiet(item);
                    lst.Add(value);
                }
                return lst;
            }
            catch (Exception)
            {
                return null;
            }
        }
        public List<DTO_ReportHoaDonChiTiet> ReportCustom(string ngaybatdau,string ngayketthuc)
        {
            try
            {
                DataTable dt = DataProvider.Instance.ExecuteQuery("HoaDonChiTietTheoKhoanThoiGian @ngay , @ngay2 ", new object[] { ngaybatdau, ngayketthuc });
                List<DTO_ReportHoaDonChiTiet> lst = new List<DTO_ReportHoaDonChiTiet>();
                foreach (DataRow item in dt.Rows)
                {
                    DTO_ReportHoaDonChiTiet value = new DTO_ReportHoaDonChiTiet(item);
                    lst.Add(value);
                }
                return lst;
            }
            catch (Exception)
            {
                return null;
            }
        }

    }
}
