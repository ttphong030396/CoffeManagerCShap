﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;
using System.Data;
namespace DAO
{
    public class DAO_LoaiMon
    {
        private static DAO_LoaiMon instance;

        public static DAO_LoaiMon Instance
        {
            get { if (instance == null) instance = new DAO_LoaiMon(); return DAO_LoaiMon.instance; }
            private set { DAO_LoaiMon.instance = value; }
        }
        private DAO_LoaiMon() { }

        #region Method
        public bool Insert(DTO_LoaiMon loai)
        {
            try
            {
                DataProvider.Instance.ExecuteNonQuery(@"INSERT INTO DanhMuc (Ten) VALUES( @ten )", new object[] { loai.Ten });
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public bool Update(DTO_LoaiMon loai)
        {
            try
            {
                DataProvider.Instance.ExecuteNonQuery(@"UPDATE DanhMuc SET Ten = @ten WHERE ID = @id ", new object[] { loai.Ten, loai.ID });
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public bool Delete(int ID)
        {
            try
            {
                DataProvider.Instance.ExecuteNonQuery(@"DELETE DanhMuc WHERE ID = @id ", new object[] { ID });
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public List<DTO_LoaiMon> LayDanhSach()
        {
            try
            {
                List<DTO_LoaiMon> lst = new List<DTO_LoaiMon>();
                DataTable dt = DataProvider.Instance.ExecuteQuery(@"SELECT * FROM DanhMuc");
                foreach (DataRow item in dt.Rows)
                {
                    DTO_LoaiMon value = new DTO_LoaiMon(item);
                    lst.Add(value);
                }
                return lst;
            }
            catch (Exception)
            {

                throw;
            }
        }
        public string LayTen_ID(int ID)
        {
            try
            {
                return DataProvider.Instance.ExecuteScalar(@"SELECT Ten FROM DanhMuc WHERE ID = @id", new object[] { ID }).ToString();
            }
            catch (Exception)
            {
                return "";
            }
        }

        #endregion
    }
}
