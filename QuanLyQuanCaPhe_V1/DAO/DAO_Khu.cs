﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;
using System.Data;
namespace DAO
{
    public class DAO_Khu
    {
        private static DAO_Khu instance;

        public static DAO_Khu Instance
        {
            get { if (instance == null) instance = new DAO_Khu(); return DAO_Khu.instance; }
            private set { DAO_Khu.instance = value; }
        }
        private DAO_Khu() { }
        #region Method
        public bool Insert(DTO_Khu khu)
        {
            try
            {
                DataProvider.Instance.ExecuteNonQuery(@"INSERT INTO KhuVuc VALUES( @tenkhu )", new object[] { khu.Ten });
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public bool Update(DTO_Khu khu)
        {
            try
            {
                DataProvider.Instance.ExecuteNonQuery(@"UPDATE KhuVuc SET Ten = @ten WHERE ID = @id ", new object[] { khu.Ten, khu.ID });
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public bool Delete(int IDKhu)
        {
            try
            {
                DataProvider.Instance.ExecuteNonQuery(@"DELETE KhuVuc WHERE ID = @id ", new object[] { IDKhu });
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public int DemTenKhu(string TenKhu)
        {
            try
            {
                return (int)DataProvider.Instance.ExecuteScalar(@"SELECT COUNT(Ten) FROM KhuVuc WHERE Ten = @ten ", new object[] { TenKhu });
            }
            catch (Exception)
            {
                return -1;
            }
        }

        
        public List<DTO_Khu> LayDanhSach()
        {
            try
            {
                List<DTO_Khu> lst = new List<DTO_Khu>();
                DataTable dt = DataProvider.Instance.ExecuteQuery(@"SELECT * FROM KhuVuc");
                foreach (DataRow item in dt.Rows)
                {
                    DTO_Khu tmp = new DTO_Khu(item);
                    lst.Add(tmp);
                }
                return lst;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion
    }
}
