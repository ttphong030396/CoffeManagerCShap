﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;
using System.Data;
namespace DAO
{
    public class DAO_TaiKhoan
    {
        private static DAO_TaiKhoan instance;

        public static DAO_TaiKhoan Instance
        {
            get { if (instance == null) instance = new DAO_TaiKhoan(); return DAO_TaiKhoan.instance; }
            private set { DAO_TaiKhoan.instance = value; }
        }
        private DAO_TaiKhoan() { }
        #region Method
        public bool Insert(DTO_TaiKhoan tk)
        {
            try
            {
                DataProvider.Instance.ExecuteNonQuery(@"INSERT INTO TaiKhoan VALUES( @user , @pass , @tenhienthi , @idloai )", new object[] { tk.TaiKhoan, tk.MatKhau, tk.TenHienThi, tk.Loai });
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public bool Update(DTO_TaiKhoan tk)
        {
            try
            {
                DataProvider.Instance.ExecuteNonQuery(@"UPDATE TaiKhoan SET MatKhau = @pass , TenHienThi = @ten , IDLoai = @id WHERE TenTK = @user ", new object[] { tk.MatKhau, tk.TenHienThi, tk.Loai, tk.TaiKhoan });
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public bool Delete(string user)
        {
            try
            {
                DataProvider.Instance.ExecuteNonQuery(@"DELETE TaiKhoan WHERE TenTK = @user", new object[] { user });
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public DTO_TaiKhoan Get(string user,string pass)
        {
            try
            {
                DTO_TaiKhoan tk;
                DataTable dt = DataProvider.Instance.ExecuteQuery(@"SELECT * FROM TaiKhoan WHERE TenTK = @user AND MatKhau = @pass", new object[] { user, pass });
                tk = new DTO_TaiKhoan(dt.Rows[0]);
                return tk;
            }
            catch (Exception)
            {
                return null;
            }
        }
        public List<DTO_TaiKhoan> GetAll()
        {
            try
            {
                List<DTO_TaiKhoan> lst = new List<DTO_TaiKhoan>();
                DataTable dt = DataProvider.Instance.ExecuteQuery(@"SELECT * FROM TaiKhoan");
                foreach(DataRow item in dt.Rows)
                {
                    DTO_TaiKhoan value = new DTO_TaiKhoan(item);
                    lst.Add(value);
                }
                return lst;
            }
            catch (Exception)
            {
                return null;
            }
        }
        
        #endregion
    }
}
