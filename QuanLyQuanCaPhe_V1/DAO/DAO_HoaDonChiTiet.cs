﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;
using System.Data;
namespace DAO
{
    public class DAO_HoaDonChiTiet
    {
        private static DAO_HoaDonChiTiet instance;

        public static DAO_HoaDonChiTiet Instance
        {
            get { if (instance == null)instance = new DAO_HoaDonChiTiet(); return DAO_HoaDonChiTiet.instance; }
            private set { DAO_HoaDonChiTiet.instance = value; }
        }
        private DAO_HoaDonChiTiet() { }
        #region Method
        public object LayIDHoaDonChiTiet_IDHoaDon_IDMonAn(int IDHoaDon, int IDMonAn)
        {
            try
            {
                return DataProvider.Instance.ExecuteScalar(@"SELECT ID FROM HoaDonChiTiet WHERE Mon = @idMon AND HoaDon = @id", new object[] { IDMonAn, IDHoaDon });
            }
            catch (Exception)
            {
                return null;
            }
        }
        public bool Insert(int IDHoaDon, int IDMonAn, int SoLuong, float DonGia)
        {
            try
            {
                DataProvider.Instance.ExecuteNonQuery(@"ThemHoaDonChiTiet @idhd , @idmonan , @sl , @dg ", new object[] { IDHoaDon, IDMonAn, SoLuong, DonGia });
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Update(int IDHoaDon, int IDMonAn, int SoLuong, float DonGia)
        {
            try
            {
                DataProvider.Instance.ExecuteNonQuery(@"CapNhatHoaDonChiTiet @idhd , @idmonan , @sl , @dg ", new object[] { IDHoaDon, IDMonAn, SoLuong, DonGia });
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public List<DTO_HoaDon_HoaDonChiTiet_MonAn> LayDanhSach_IDBan(int IDBan)
        {
            try
            {
                DataTable dt = DataProvider.Instance.ExecuteQuery(@"LayDanhSachHoaDonChiTiet_Ban @idban ", new object[] { IDBan });
                List<DTO_HoaDon_HoaDonChiTiet_MonAn> lst = new List<DTO_HoaDon_HoaDonChiTiet_MonAn>();
                foreach (DataRow item in dt.Rows)
                {
                    DTO_HoaDon_HoaDonChiTiet_MonAn value = new DTO_HoaDon_HoaDonChiTiet_MonAn(item);
                    lst.Add(value);
                }
                return lst;
            }
            catch (Exception)
            {
                return null;
            }
        }
        public bool Delete(int IDHoaDonCHiTiet)
        {
            try
            {
                DataProvider.Instance.ExecuteNonQuery(@"DELETE HoaDonChiTiet WHERE ID = @id ", new object[] { IDHoaDonCHiTiet });
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public int DemSoDong(int IDHoaDon)
        {
            try
            {
                return int.Parse(DataProvider.Instance.ExecuteScalar(@"SELECT COUNT(ID) FROM HoaDonChiTiet WHERE HoaDon = @id ", new object[] { IDHoaDon }).ToString());
            }
            catch (Exception)
            {
                return 0;
            }
        }
        #endregion


    }
}
