﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;
using System.Data;
namespace DAO
{
    public class DAO_LoaiTaiKhoan
    {
        private static DAO_LoaiTaiKhoan instance;

        public static DAO_LoaiTaiKhoan Instance
        {
            get { if (instance == null)instance = new DAO_LoaiTaiKhoan(); return DAO_LoaiTaiKhoan.instance; }
            private set { DAO_LoaiTaiKhoan.instance = value; }
        }
        private DAO_LoaiTaiKhoan() { }

        public List<DTO_LoaiTaiKhoan> Get()
        {
            try
            {
                DataTable dt = DataProvider.Instance.ExecuteQuery(@"SELECT * FROM LoaiTK");
                List<DTO_LoaiTaiKhoan> lst = new List<DTO_LoaiTaiKhoan>();
                foreach(DataRow item in dt.Rows)
                {
                    DTO_LoaiTaiKhoan value = new DTO_LoaiTaiKhoan(item);
                    lst.Add(value);
                }
                return lst;
            }
            catch (Exception)
            {
                return null;
            }
        }
        public string LayTen_ID(int ID)
        {
            try
            {
                return DataProvider.Instance.ExecuteScalar(@"SELECT Ten FROM LoaiTK WHERE ID= @id", new object[] { ID }).ToString();
            }
            catch (Exception)
            {
                return "";
            }
        }

    }
}
