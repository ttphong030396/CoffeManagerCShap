﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO
{
    public class DAO_Database
    {
        private static DAO_Database instance;

        public static DAO_Database Instance
        {
            get { if (instance == null) instance = new DAO_Database(); return DAO_Database.instance; }
            private set { DAO_Database.instance = value; }
        }
        private DAO_Database() { }
        public bool SaoLuu(string path)
        {
            try
            {
                DataProvider.Instance.ExecuteNonQuery(@"BACKUP DATABASE QuanLyQuanCaPhe_1985 TO DISK= @path", new object[] { path });
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public bool PhucHoi(string path)
        {
            try
            {
                DataProvider.Instance.ExecuteNonQuery(@"USE master RESTORE DATABASE QuanLyQuanCaPhe_1985 FROM DISK= @path WITH REPLACE", new object[] { path });
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public bool GhiLichSu(string NguoiDung,string HanhVi,DateTime ThoiGian)
        {
            try
            {
                DataProvider.Instance.ExecuteNonQuery(@"INSERT INTO LichSu VALUES( @tennguoidung , @HanhVi , @ThoiGian )", new object[] { NguoiDung, HanhVi, ThoiGian });
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public bool GhiLoi(string Loi,DateTime ThoiGian)
        {
            try
            {
                DataProvider.Instance.ExecuteNonQuery(@"INSERT INTO Loi VALUES(  @Loi , @ThoiGian )", new object[] { Loi, ThoiGian });
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public DataTable LayLichSu(string NguoiDung)
        {
            try
            {
                return DataProvider.Instance.ExecuteQuery(@"LayLichSu_NguoiDung @nguoidung", new object[] { NguoiDung });
            }
            catch (Exception)
            {
                return null;
            }
        }
        public DataTable LayTatCaLichSu()
        {
            try
            {
                return DataProvider.Instance.ExecuteQuery(@"SELECT * FROM LichSu ORDER BY ThoiGian DESC");
            }
            catch (Exception)
            {
                return null;
            }
        }
        public DataTable LayTatCaLichSu(string taikhoan)
        {
            try
            {
                return DataProvider.Instance.ExecuteQuery(@"SELECT * FROM LichSu WHERE NguoiDung= @nguoidung ORDER BY ThoiGian DESC ", new object[] { taikhoan });
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}