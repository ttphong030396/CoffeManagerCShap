﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;
using System.Data;
namespace DAO
{
    public class DAO_Ban
    {
        private static DAO_Ban instance;

        public static DAO_Ban Instance
        {
            get { if (instance == null) instance = new DAO_Ban(); return DAO_Ban.instance; }
            private set { DAO_Ban.instance = value; }
        }
        private DAO_Ban() { }
        #region Method
        public bool Insert(DTO_Ban ban)
        {
            try
            {
                DataProvider.Instance.ExecuteNonQuery(@"INSERT INTO Ban(Ten,Khu)VALUES( @ten , @idkhu )", new object[] { ban.Ten, ban.IDKhu });
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public bool Update(DTO_Ban ban)
        {
            try
            {
                DataProvider.Instance.ExecuteNonQuery(@"UPDATE Ban SET Ten= @ten ,Khu= @khu WHERE ID= @id ", new object[] { ban.Ten, ban.IDKhu, ban.ID });
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public bool Delete(int IDBan)
        {
            try
            {
                DataProvider.Instance.ExecuteNonQuery(@"DELETE Ban WHERE ID = @id ",new object[]{IDBan});
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        
        public List<DTO_Ban> LayDanhSach()
        {
            try
            {
                List<DTO_Ban> lst = new List<DTO_Ban>();
                DataTable dt = DataProvider.Instance.ExecuteQuery("SELECT * FROM Ban");
                foreach (DataRow item in dt.Rows)
                {
                    DTO_Ban tmp = new DTO_Ban(item);
                    lst.Add(tmp);
                }
                return lst;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public List<DTO_Ban> LayDanhSach_Khu(int IDKhu)
        {
            try
            {
                List<DTO_Ban> lst = new List<DTO_Ban>();
                DataTable dt = DataProvider.Instance.ExecuteQuery("SELECT * FROM Ban WHERE Khu = @id ", new object[] { IDKhu });
                foreach (DataRow item in dt.Rows)
                {
                    DTO_Ban tmp = new DTO_Ban(item);
                    lst.Add(tmp);
                }
                return lst;
            }
            catch (Exception)
            {
                
                throw;
            }
        }
        public List<DTO_Ban> LayDanhSach_Trang(int Trang)
        {
            try
            {

                List<DTO_Ban> lst = new List<DTO_Ban>();
                DataTable dt = DataProvider.Instance.ExecuteQuery(@"LayDanhSachBan_Trang @Trang", new object[] { Trang });
                foreach (DataRow item in dt.Rows)
                {
                    DTO_Ban value = new DTO_Ban(item);
                    lst.Add(value);
                }
                return lst;

            }
            catch (Exception)
            {
                return null;
            }
        }
        public List<DTO_Ban> LayDanhSach_Trang_Khu(int Trang, int IDKhu)
        {
            try
            {

                List<DTO_Ban> lst = new List<DTO_Ban>();
                DataTable dt = DataProvider.Instance.ExecuteQuery(@"LayDanhSachBan_Trang_Khu @idkhu , @Trang", new object[] { IDKhu, Trang });
                foreach (DataRow item in dt.Rows)
                {
                    DTO_Ban value = new DTO_Ban(item);
                    lst.Add(value);
                }
                return lst;

            }
            catch (Exception)
            {
                return null;
            }
        }
        public bool CapNhatTrangThaiBan(int IDBan, string TrangThai)
        {
            try
            {
                DataProvider.Instance.ExecuteNonQuery(@"UPDATE Ban SET TinhTrang = @tt WHERE ID = @id ", new object[] { TrangThai, IDBan });
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public int GetCount(int IDKhu)
        {
            try
            {
                return (int)DataProvider.Instance.ExecuteScalar("SELECT COUNT(*) FROM Ban WHERE Khu = @id ", new object[] { IDKhu });
            }
            catch (Exception)
            {
                return -1;
            }
        }
        public int GetCount()
        {
            try
            {
                return (int)DataProvider.Instance.ExecuteScalar("SELECT COUNT(*) FROM Ban");
            }
            catch (Exception)
            {
                return -1;
            }

        }
        public string TrangThai(int IDban)
        {
            try
            {
                return (string)DataProvider.Instance.ExecuteScalar(@"SELECT TinhTrang FROM Ban WHERE ID = @id ", new object[] { IDban });
            }
            catch (Exception)
            {
                return null;
            }
        }
        public int CountTen(string TenBan)
        {
            try
            {
                return (int)DataProvider.Instance.ExecuteScalar(@"SELECT COUNT(Ten) FROM Ban WHERE Ten = @ten ", new object[] { TenBan });
            }
            catch (Exception)
            {
                return -1;
            }
        }
        #endregion

    }
}
