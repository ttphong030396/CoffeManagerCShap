﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;
using System.Data;
namespace DAO
{
    public class DAO_HoaDon
    {
        private static DAO_HoaDon instance;

        public static DAO_HoaDon Instance
        {
            get { if (instance == null) instance = new DAO_HoaDon(); return DAO_HoaDon.instance; }
            private set { DAO_HoaDon.instance = value; }
        }
        private DAO_HoaDon() { }
        public object LayIDHoaDon_IDBan(int IDBan)
        {
            try
            {
                return DataProvider.Instance.ExecuteScalar(@"SELECT ID FROM HoaDon WHERE Ban = @idban AND TinhTrang = 0 ", new object[] { IDBan });
            }
            catch (Exception)
            {
                throw;
            }
        }
        public DTO_HoaDon LayHoaDon_IDBan(int IDBan)
        {
            try
            {
                DataTable dt = DataProvider.Instance.ExecuteQuery(@"SELECT * FROM HoaDon WHERE Ban = @idban AND TinhTrang = 0 ", new object[] { IDBan });
                DTO_HoaDon hd = new DTO_HoaDon(dt.Rows[0]);
                return hd;
            }
            catch (Exception)
            {
                return null;
            }
        }
        public bool Insert(int IDBan)
        {
            try
            {
                DataProvider.Instance.ExecuteNonQuery(@"INSERT INTO HoaDon(Ban) VALUES( @id )", new object[] { IDBan });
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public object LayIDHoaDonChuaThanhToan_IDBan(int IDBan)
        {
            try
            {
                return DataProvider.Instance.ExecuteScalar(@"SELECT ID FROM HoaDon WHERE Ban = @id AND TinhTrang = 0", new object[] { IDBan });
            }
            catch (Exception)
            {
                return null;
            }
        }
        public object TongTien_IDHD(int IDHoaDon)
        {
            try
            {
                return DataProvider.Instance.ExecuteScalar(@"SELECT SUM(TongTien) FROM HoaDonChiTiet WHERE HoaDon = @id ", new object[] { IDHoaDon });
            }
            catch (Exception)
            {
                return null;
            }
        }
        public bool CapNhatGiamGia(int IDHoaDon,int PhanTramGiamGia)
        {
            try
            {
                DataProvider.Instance.ExecuteNonQuery(@"UPDATE HoaDon Set GiamGia = @giamgia WHERE ID = @id ", new object[] { PhanTramGiamGia, IDHoaDon });
                return true;
            }
            catch (Exception)
            {
                return false;
                throw;
            }
        }
        public bool ChuyenBan(int HoaDon, int IDBanChuyenDen)
        {
            try
            {
                DataProvider.Instance.ExecuteNonQuery(@"UPDATE HoaDon SET Ban = @idban WHERE ID = @id", new object[] { IDBanChuyenDen, HoaDon });
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public bool Delete(int IDHoaDon)
        {
            try
            {
                DataProvider.Instance.ExecuteNonQuery(@"DELETE HoaDon WHERE ID = @id", new object[] { IDHoaDon });
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public bool ThanhToan(int IDHoaDon,float TongTien,int PhanTramGiamGia,float TienGiam)
        {
            try
            {
                DataProvider.Instance.ExecuteNonQuery("UPDATE HoaDon SET ThanhTien = @tt , TienGiam = @tiengiam , GiamGia = @giamgia , TinhTrang = 1 where ID = @id ", new object[] {TongTien,TienGiam,PhanTramGiamGia,IDHoaDon });
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

    }
}
