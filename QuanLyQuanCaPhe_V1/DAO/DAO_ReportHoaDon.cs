﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;
using System.Data;
namespace DAO
{
    public class DAO_ReportHoaDon
    {
        private static DAO_ReportHoaDon instance;

        public static DAO_ReportHoaDon Instance
        {
            get { if (instance == null)instance = new DAO_ReportHoaDon(); return DAO_ReportHoaDon.instance; }
            set { DAO_ReportHoaDon.instance = value; }
        }
        private DAO_ReportHoaDon() { }

        public List<DTO_ReportHoaDon> Report(string ngay)
        {
            try
            {
                DataTable dt = DataProvider.Instance.ExecuteQuery("LayHoaDonTheoNgay @ngay ", new object[] { ngay });
                List<DTO_ReportHoaDon> lst = new List<DTO_ReportHoaDon>();
                foreach (DataRow item in dt.Rows)
                {
                    DTO_ReportHoaDon value = new DTO_ReportHoaDon(item);
                    lst.Add(value);
                }
                return lst;
            }
            catch (Exception)
            {
                return null;
            }
        }
        public List<DTO_ReportHoaDon> ReportMonth(string ngay)
        {
            try
            {
                DataTable dt = DataProvider.Instance.ExecuteQuery("LayHoaDonTheoThang @ngay ", new object[] { ngay });
                List<DTO_ReportHoaDon> lst = new List<DTO_ReportHoaDon>();
                foreach (DataRow item in dt.Rows)
                {
                    DTO_ReportHoaDon value = new DTO_ReportHoaDon(item);
                    lst.Add(value);
                }
                return lst;
            }
            catch (Exception)
            {
                return null;
            }
        }
        public List<DTO_ReportHoaDon> ReportCustom(string ngaybatdau,string ngayketthuc)
        {
            try
            {
                DataTable dt = DataProvider.Instance.ExecuteQuery("LayHoaDonTheoKhoanThoiGian @ngay , @ngay2 ", new object[] { ngaybatdau, ngayketthuc });
                List<DTO_ReportHoaDon> lst = new List<DTO_ReportHoaDon>();
                foreach (DataRow item in dt.Rows)
                {
                    DTO_ReportHoaDon value = new DTO_ReportHoaDon(item);
                    lst.Add(value);
                }
                return lst;
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
