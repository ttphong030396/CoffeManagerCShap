﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;
using System.Data;
namespace DAO
{
    public class DAO_MonAn
    {
        private static DAO_MonAn instance;

        public static DAO_MonAn Instance
        {
            get { if (instance == null)instance = new DAO_MonAn(); return DAO_MonAn.instance; }
            private set { DAO_MonAn.instance = value; }
        }
        private DAO_MonAn() { }
        #region Method
        public bool Insert(DTO_MonAn mon)
        {
            try
            {
                DataProvider.Instance.ExecuteNonQuery(@"INSERT INTO Mon (Ten, DanhMuc, Mode, GiaNgay, GiaDem) VALUES ( @Ten , @idloai , @mode , @giangay , @giadem )", new object[] { mon.Ten, mon.IDLoai, mon.Mode, mon.GiaNgay, mon.GiaDem });
                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }
        public bool Delete(int IDMon)
        {
            try
            {
                DataProvider.Instance.ExecuteNonQuery(@"DELETE Mon WHERE ID = @id ", new object[] { IDMon });
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public bool Update(DTO_MonAn mon)
        {
            try
            {
                DataProvider.Instance.ExecuteNonQuery(@"UPDATE Mon SET Ten = @ten ,DanhMuc = @idloai , Mode = @mode ,GiaNgay = @giangay ,GiaDem = @giadem WHERE ID = @id", new object[] { mon.Ten, mon.IDLoai, mon.Mode, mon.GiaNgay, mon.GiaDem, mon.ID });
                return true;
            }
            catch (Exception)
            {
                return false;
            }



        }


        public List<DTO_MonAn> LayDanhSach_Mode(string Mode)
        {
            try
            {
                List<DTO_MonAn> lst = new List<DTO_MonAn>();
                DataTable dt = DataProvider.Instance.ExecuteQuery(@"SELECT * FROM Mon WHERE Mode = @mode ", new object[] { Mode });
                foreach (DataRow item in dt.Rows)
                {
                    DTO_MonAn value = new DTO_MonAn(item);
                    lst.Add(value);
                }
                return lst;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public List<DTO_MonAn> LayDanhSach_Mode_IDLoai(string Mode, int IDLoai)
        {
            try
            {
                List<DTO_MonAn> lst = new List<DTO_MonAn>();
                DataTable dt = DataProvider.Instance.ExecuteQuery(@"SELECT * FROM Mon WHERE Mode = @mode AND DanhMuc = @idloai ", new object[] { Mode, IDLoai });
                foreach (DataRow item in dt.Rows)
                {
                    DTO_MonAn value = new DTO_MonAn(item);
                    lst.Add(value);
                }
                return lst;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public List<DTO_MonAn> LayDanhSach_IDLoai(int IDLoai)
        {
            try
            {
                List<DTO_MonAn> lst = new List<DTO_MonAn>();
                DataTable dt = DataProvider.Instance.ExecuteQuery(@"SELECT * FROM Mon WHERE DanhMuc = @idloai ", new object[] { IDLoai });
                foreach (DataRow item in dt.Rows)
                {
                    DTO_MonAn value = new DTO_MonAn(item);
                    lst.Add(value);
                }
                return lst;
            }
            catch (Exception)
            {
                return null;
            }
        }
        //Phân trang
        public List<DTO_MonAn> LayDanhSach_Trang(int Trang)
        {
            try
            {
                DataTable dt = DataProvider.Instance.ExecuteQuery(@"LayDanhSachMon_Trang @page ", new object[] { Trang });
                List<DTO_MonAn> lst = new List<DTO_MonAn>();
                foreach (DataRow item in dt.Rows)
                {
                    DTO_MonAn value = new DTO_MonAn(item);
                    lst.Add(value);
                }
                return lst;
            }
            catch (Exception)
            {
                return null;
            }
        }
        public List<DTO_MonAn> LayDanhSach_Trang_Mode(int Trang, string Mode)
        {
            try
            {
                DataTable dt = DataProvider.Instance.ExecuteQuery(@"LayDanhSachMon_Trang_Mode @page , @mode ", new object[] { Trang, Mode });
                List<DTO_MonAn> lst = new List<DTO_MonAn>();
                foreach (DataRow item in dt.Rows)
                {
                    DTO_MonAn value = new DTO_MonAn(item);
                    lst.Add(value);
                }
                return lst;
            }
            catch (Exception)
            {
                return null;
            }
        }
        public List<DTO_MonAn> LayDanhSach_Trang_Mode_IDLoai(int Trang, string Mode, int IDLoai)
        {
            try
            {
                DataTable dt = DataProvider.Instance.ExecuteQuery(@"LayDanhSachMon_Trang_Mode_Loai @page , @mode ,@idloai ", new object[] { Trang, Mode, IDLoai });
                List<DTO_MonAn> lst = new List<DTO_MonAn>();
                foreach (DataRow item in dt.Rows)
                {
                    DTO_MonAn value = new DTO_MonAn(item);
                    lst.Add(value);
                }
                return lst;
            }
            catch (Exception)
            {
                return null;
            }
        }
        public List<DTO_MonAn> LayDanhSach_Trang_IDLoai(int Trang, int IDLoai)
        {
            try
            {
                DataTable dt = DataProvider.Instance.ExecuteQuery(@"LayDanhSach_Trang_Loai @idloai , @page", new object[] { IDLoai, Trang });
                List<DTO_MonAn> lst = new List<DTO_MonAn>();
                foreach (DataRow item in dt.Rows)
                {
                    DTO_MonAn value = new DTO_MonAn(item);
                    lst.Add(value);
                }
                return lst;
            }
            catch (Exception)
            {
                return null;
            }
        }
        //

        public int GetCount(int IDLoai)
        {
            try
            {
                return (int)DataProvider.Instance.ExecuteScalar("SELECT COUNT(*) FROM Mon WHERE DanhMuc = @id ", new object[] { IDLoai });
            }
            catch (Exception)
            {
                return -1;
            }
        }
        public int GetCount()
        {
            try
            {
                return (int)DataProvider.Instance.ExecuteScalar("SELECT COUNT(*) FROM Mon");
            }
            catch (Exception)
            {
                return -1;
            }
        }
        public string LayTen_ID(int ID)
        {
            try
            {
                return DataProvider.Instance.ExecuteScalar(@"SELECT Ten FROM Mon WHERE ID= @id", new object[] { ID }).ToString();
            }
            catch (Exception)
            {
                return "";
            }
        }
        #endregion
    }
}
