﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;
using System.Data;
namespace DAO
{
    public class DataProvider
    {
        #region Properties
        private static DataProvider instance;
        private string strconnect = "";
        #endregion
        #region Method
        public static DataProvider Instance
        {
            get { if (instance == null) instance = new DataProvider(); return DataProvider.instance; }
            private set { DataProvider.instance = value; }
        }
        private DataProvider() { }
        public string Strconnect
        {
            get { return strconnect; }
            set { strconnect = value; }
        }
        public DataTable ExecuteQuery(string query, object[] param = null)
        {
            DataTable data = new DataTable();

            using (SqlConnection connection = new SqlConnection(strconnect))
            {
                connection.Open();

                SqlCommand cmd = new SqlCommand(query, connection);

                if (param != null)
                {
                    string[] listPara = query.Split(' ');
                    int i = 0;
                    foreach (string item in listPara)
                    {
                        if (item.Contains('@'))
                        {
                             cmd.Parameters.AddWithValue(item, param[i]);
                            i++;
                        }
                    }
                }
                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                adapter.Fill(data);
                connection.Close();
            }
            return data;
        }
        public int ExecuteNonQuery(string query, object[] param = null)
        {
            int data = 0;

            using (SqlConnection connection = new SqlConnection(strconnect))
            {
                connection.Open();

                SqlCommand cmd = new SqlCommand(query, connection);

                if (param != null)
                {
                    string[] listpara = query.Split(' ');
                    int i = 0;
                    foreach (string item in listpara)
                    {
                        if (item.Contains('@'))
                        {
                            cmd.Parameters.AddWithValue(item, param[i]);
                            i++;
                        }
                    }
                }
                data = cmd.ExecuteNonQuery();

                connection.Close();
            }
            return data;
        }
        public object ExecuteScalar(string query, object[] param = null)
        {
            object data = 0;

            using (SqlConnection connection = new SqlConnection(strconnect))
            {
                connection.Open();

                SqlCommand cmd = new SqlCommand(query, connection);

                if (param != null)
                {
                    string[] listpara = query.Split(' ');
                    int i = 0;
                    foreach (string item in listpara)
                    {
                        if (item.Contains('@'))
                        {
                            cmd.Parameters.AddWithValue(item, param[i]);
                            i++;
                        }
                    }
                }
                data = cmd.ExecuteScalar();

                connection.Close();
            }
            return data;
        }
        public bool TestConnection()
        {
            try
            {
                SqlConnection cnn = new SqlConnection(Strconnect);
                cnn.Open();
                cnn.Close();
                return true;
            }
            catch
            {
                return false;
            }

        }
        #endregion
    }
}
