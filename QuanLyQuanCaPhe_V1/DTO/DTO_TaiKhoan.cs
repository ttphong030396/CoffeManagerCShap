﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class DTO_TaiKhoan
    {
        public string TaiKhoan { get; set; }
        public string MatKhau { get; set; }
        public string TenHienThi { get; set; }
        public int Loai { get; set; }
        public DTO_TaiKhoan() { }
        public DTO_TaiKhoan(DataRow d)
        {
            this.TaiKhoan = d.ItemArray[0].ToString();
            this.MatKhau = d.ItemArray[1].ToString();
            this.TenHienThi = d.ItemArray[2].ToString();
            this.Loai = int.Parse(d.ItemArray[3].ToString());
        }
    }
}
