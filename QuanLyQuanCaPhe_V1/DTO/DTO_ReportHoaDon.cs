﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class DTO_ReportHoaDon
    {
        //HD.ID , HD.NgayVao,B.Ten,HD.GiamGia,HD.TienGiam, HD.ThanhTien
        public int IDHoaDon { get; set; }
        public DateTime NgayVao { get; set; }
        public string TenBan { get; set; }
        public int GiamGia { get; set; }
        public float TienGiam { get; set; }
        public float ThanhTien { get; set; }

        public DTO_ReportHoaDon(DataRow d)
        {
            this.IDHoaDon = int.Parse(d.ItemArray[0].ToString().Trim());
            this.NgayVao = DateTime.Parse(d.ItemArray[1].ToString().Trim());
            this.TenBan = d.ItemArray[2].ToString().Trim();
            this.GiamGia = int.Parse(d.ItemArray[3].ToString().Trim());
            this.TienGiam = float.Parse(d.ItemArray[4].ToString().Trim());
            this.ThanhTien = float.Parse(d.ItemArray[5].ToString().Trim());
        }




    }
}
