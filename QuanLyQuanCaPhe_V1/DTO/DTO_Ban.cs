﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class DTO_Ban
    {
        public int ID { get; set; }
        public string Ten { get; set;}
        public string TinhTrang { get; set; }
        public int IDKhu { get; set; }
        public DTO_Ban()
        {

        }
        public DTO_Ban(DataRow d)
        { 
            this.ID = int.Parse(d.ItemArray[0].ToString());
            this.Ten = d.ItemArray[1].ToString();
            this.TinhTrang = d.ItemArray[2].ToString();
            this.IDKhu = int.Parse(d.ItemArray[3].ToString());
        }
        
    }
}
