﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class DTO_LoaiMon
    {
        public int ID { get; set; }
        public string Ten { get; set; }
        public DTO_LoaiMon() { }
        public DTO_LoaiMon(DataRow d)
        {
            this.ID = int.Parse(d.ItemArray[0].ToString());
            this.Ten = d.ItemArray[1].ToString();
        }
    }
}
