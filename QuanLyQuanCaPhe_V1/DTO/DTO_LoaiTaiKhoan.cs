﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
namespace DTO
{
    public class DTO_LoaiTaiKhoan
    {
        public int ID { get; set; }
        public string Ten { get; set; }

        public DTO_LoaiTaiKhoan(DataRow d)
        {
            this.ID = int.Parse(d.ItemArray[0].ToString().Trim());
            this.Ten = d.ItemArray[1].ToString().Trim();
        }
    }
}
