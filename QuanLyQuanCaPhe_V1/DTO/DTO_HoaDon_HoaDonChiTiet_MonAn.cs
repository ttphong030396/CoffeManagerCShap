﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
namespace DTO
{
    public class DTO_HoaDon_HoaDonChiTiet_MonAn
    {
        public int IDMonAn { get; set; }
        public string TenMonAn { get; set; }
        public int SoLuongMonAn { get; set; }
        public float GiaNgay { get; set; }
        public float GiaDem { get; set; }
        public float TongNgay { get; set; }
        public float TongDem { get; set; }
        public int IDHoaDon { get; set; }
        public DTO_HoaDon_HoaDonChiTiet_MonAn(DataRow d)
        {
            this.IDMonAn = int.Parse(d.ItemArray[0].ToString());
            this.TenMonAn = d.ItemArray[1].ToString();
            this.SoLuongMonAn = int.Parse(d.ItemArray[2].ToString());
            this.GiaNgay = (float)Convert.ToDouble(d.ItemArray[3].ToString());
            this.GiaDem = (float)Convert.ToDouble(d.ItemArray[4].ToString());
            this.TongNgay = (float)Convert.ToDouble(d.ItemArray[5].ToString());
            this.TongDem = (float)Convert.ToDouble(d.ItemArray[6].ToString());
            this.IDHoaDon = int.Parse(d.ItemArray[7].ToString());
        }

    }
}
