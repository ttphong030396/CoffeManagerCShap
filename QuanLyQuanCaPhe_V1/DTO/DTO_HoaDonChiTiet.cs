﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
namespace DTO
{
    public class DTO_HoaDonChiTiet
    {
        public int ID { get; set; }
        public int IDHoaDon { get; set; }
        public int IDMonAn { get; set; }
        public int SoLuong { get; set; }
        public float TongTien { get; set; }
        public DTO_HoaDonChiTiet() { }
        public DTO_HoaDonChiTiet(DataRow d)
        {
            this.ID = int.Parse(d.ItemArray[0].ToString());
            this.IDHoaDon = int.Parse(d.ItemArray[1].ToString());
            this.IDMonAn = int.Parse(d.ItemArray[2].ToString());
            this.SoLuong = int.Parse(d.ItemArray[3].ToString());
            this.TongTien = (float)Convert.ToDouble(d.ItemArray[4].ToString());
        }
    }
}
