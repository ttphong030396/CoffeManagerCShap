﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace DTO
{
    public class DTO_HoaDon
    {
        public int ID { get; set; }
        public DateTime NgayVao { get; set; }
        public int IDBan { get; set; }
        public int TinhTrang { get; set; }
        public float TongTien { get; set; }
        public float TienGiam { get; set; }
        public int GiamGia { get; set; }
        public DTO_HoaDon() { }
        public DTO_HoaDon(DataRow d)
        {
            this.ID = int.Parse(d.ItemArray[0].ToString());
            this.NgayVao = DateTime.Parse(d.ItemArray[1].ToString());
            this.IDBan = int.Parse(d.ItemArray[2].ToString());
            this.TinhTrang = int.Parse(d.ItemArray[3].ToString());
            this.TongTien = (float)Convert.ToDouble(d.ItemArray[4].ToString());
            this.TienGiam = (float)Convert.ToDouble(d.ItemArray[5].ToString());
            this.GiamGia = int.Parse(d.ItemArray[6].ToString());
        }
    }
}
