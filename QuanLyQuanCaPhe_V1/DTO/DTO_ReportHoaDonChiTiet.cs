﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace DTO
{
    public class DTO_ReportHoaDonChiTiet
    {
        public int IDHoaDon { get; set; }
        public DateTime NgayVao { get; set; }
        public string TenBan { get; set; }
        public string TenMon { get; set; }
        public int SoLuong { get; set; }
        public float TongTien { get; set; }


        public DTO_ReportHoaDonChiTiet(DataRow d)
        {
            this.IDHoaDon = int.Parse(d.ItemArray[0].ToString().Trim());
            this.NgayVao = DateTime.Parse(d.ItemArray[1].ToString().Trim());
            this.TenBan = d.ItemArray[2].ToString().Trim();
            this.TenMon = d.ItemArray[3].ToString().Trim();
            this.SoLuong = int.Parse(d.ItemArray[4].ToString().Trim());
            this.TongTien = float.Parse(d.ItemArray[5].ToString().Trim());
        }
    }
}
