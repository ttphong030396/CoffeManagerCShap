﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class DTO_MonAn
    {
        public int ID { get; set; }
        public string Ten { get; set; }
        public int IDLoai { get; set; }
        public string Mode { get; set; }
        public float GiaNgay { get; set; }
        public float GiaDem { get; set; }
        public DTO_MonAn() { }
        public DTO_MonAn(DataRow d)
        {
            this.ID = int.Parse(d.ItemArray[0].ToString());
            this.Ten = d.ItemArray[1].ToString();
            this.IDLoai = int.Parse(d.ItemArray[2].ToString());
            this.Mode = d.ItemArray[3].ToString();
            this.GiaNgay = (float)Convert.ToDouble(d.ItemArray[4].ToString());
            this.GiaDem = (float)Convert.ToDouble(d.ItemArray[5].ToString());
        }
    }
}
